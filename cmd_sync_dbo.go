package orm

import (
	"bytes"
	"fmt"
	"strings"
)

const section_db_before_sync = "START"
const section_db_after_sync = "FINISH"

type dbo struct {
	Data []byte
}

func newDbo(dboFile *string) (this *dbo) {
	//var err error
	this = &dbo{}
	if dboFile != nil {
		//if this.Data, err = ioutils.ReadFile(*dboFile); err != nil {
		//	panic(err)
		//}
		this.Data = []byte(*dboFile)
	}
	return
}

func (t *dbo) find(dbAlias string, objName string) (*string, error) {

	obj_beg := []byte("[" + dbAlias + "." + objName + "]")
	obj_end := []byte("[/" + dbAlias + "." + objName + "]")

	if !bytes.Contains(t.Data, obj_beg) {
		return nil, fmt.Errorf("orm: `dbo.conf` contains no section %s", obj_beg)
	}

	if !bytes.Contains(t.Data, obj_end) {
		//return nil, fmt.Errorf( "orm: `dbo.conf` not contains end tag of section %s", obj_end)
		panic(fmt.Errorf("orm: `dbo.conf` contains no end tag of section %s", obj_end))
	}

	beg := bytes.Index(t.Data, obj_beg)
	end := bytes.Index(t.Data, obj_end)
	if end < beg {
		//return nil, fmt.Errorf( "orm: bad data in `dbo.conf` file, tags must be %s...%s", obj_beg, obj_end)
		panic(fmt.Errorf("orm: bad data in `dbo.conf` file, tags must be %s...%s", obj_beg, obj_end))
	}
	obj_data := string(t.Data[beg+len(obj_beg) : end])
	return &obj_data, nil
}

func GetFunctionHeader(source string) string {

	data := []byte(strings.ToLower(source))
	tag_beg := []byte(strings.ToLower(`FUNCTION`))
	tag_end := []byte(strings.ToLower(`RETURNS`))

	pb := bytes.Index(data, tag_beg)
	pe := bytes.Index(data, tag_end)

	if pb == -1 || pe == -1 {
		panic(fmt.Errorf("orm: bad source of function, tags must be %s...%s", tag_beg, tag_end))
	}
	data = data[pb+len(tag_beg) : pe]
	data = bytes.Trim(data, "\t\r\n ")

	return string(data)
}

func GetFunctionBody(source string) string {
	tag_body := []byte(`$body$`)
	// fix user's Upper/Lower Case
	source = strings.Replace(source, strings.ToUpper(string(tag_body)), string(tag_body), -1)

	beg := bytes.Index([]byte(source), tag_body)
	if beg == -1 {
		return source
	}

	end := bytes.LastIndex([]byte(source), tag_body)
	data := string([]byte(source)[beg+len(tag_body) : end]) // тело обьекта
	return data
}

// Postgres Event function
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// functions for replica trigger
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const pg_fn_tr_replica = `CREATE OR REPLACE FUNCTION public.fn_tr_replica ()
  RETURNS trigger AS
  $body$
DECLARE
  REC RECORD;
  j_old         json;
  j_new         json;    
  field         text;  
  cntchg        integer = 0;  
BEGIN
  RETURN NULL;
  REC = CASE WHEN TG_OP = 'DELETE' THEN OLD ELSE NEW END;
  
  -- calc changes   
  IF TG_OP = 'UPDATE' THEN 
    j_old = to_json(OLD);
    j_new = to_json(NEW);  
    FOR field IN SELECT json_object_keys(j_old) 
    LOOP 
      IF COALESCE(j_old->>field,'') <> COALESCE(j_new->>field,'') THEN
        cntchg = cntchg + 1;
        EXIT;
      END IF;
    END LOOP;
    -- если не было изменений в записи, не регистрируем
    IF cntchg = 0 THEN
      RETURN REC;
    END IF;
  END IF;    
  
  -- вставка в таблицу регистрации изменений для репликации
  BEGIN
    INSERT INTO public.sys_replica(id, table_name, oper, rec_id) 
    VALUES(
      0/*auto*/
    , TG_TABLE_NAME
    , CASE TG_OP WHEN 'INSERT' THEN 0 WHEN 'UPDATE' THEN 1 ELSE 2/*DELETE*/ END
    , REC.id);
    RETURN REC;      
  EXCEPTION
    WHEN unique_violation THEN
    RETURN REC;
  END;
  RETURN REC;
EXCEPTION
  WHEN others THEN
  RAISE;
END;
$body$
LANGUAGE 'plpgsql'`

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// functions for syncronization trigger
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Postgres function for syncronization trigger
//const pg_fn_tr_sync_2_link =
//`CREATE OR REPLACE FUNCTION public.fn_tr_sync_2_link (
//)
//RETURNS trigger AS
//$body$
//DECLARE
//  link_shema text;
//  col  text;
//  cols text[];
//  SQL  text;
//BEGIN
//  ------------------------------------------------------------------------------
//  -- synchronization of the table with a similar one specified in the trigger parameter
//  -- is applied with the trigger AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE ... FOR EACH STATEMENT
//  -- updates changed records + adds new records + deltetes unnecessary records
//  ----------- CHECK TRIGGER ARGUMENT -------------------------------------------
//  if TG_NARGS = 0 then
//    RAISE 'args not found, trigger %s is incorrect', TG_NAME;
//  end if;
//  link_shema = TG_ARGV[0];
//
//  ----------- GET INTERSECTING COLUMNS OF SYNC TABLES --------------------------
//  SELECT array_agg(a)
//    FROM unnest((SELECT array_agg(d.column_name::text)
//                   FROM information_schema.columns d
//                  WHERE d.table_schema = TG_TABLE_SCHEMA
//                    AND d.table_name   = TG_TABLE_NAME
//                    AND d.column_name != 'id')) a
//   WHERE a = any(string_to_array(
//              (SELECT array_to_string(array_agg(d.column_name::text),',')
//                FROM information_schema.columns d
//               WHERE d.table_schema = link_shema
//                 AND d.table_name   = TG_TABLE_NAME
//                 AND d.column_name != 'id'), ','))
//  INTO cols;
//
//  ----------- UPDATE CHANGED RECORDS -------------------------------------------
//  SQL = format('UPDATE %s.%s d SET id = s.id', link_shema, TG_TABLE_NAME);
//
//  FOREACH col IN ARRAY cols LOOP
//    SQL = SQL || format(',%s = s.%s', col, col);
//	END LOOP;
//  SQL = SQL || format(' FROM %s.%s s WHERE d.id = s.id AND(0 = 1 ', TG_TABLE_SCHEMA, TG_TABLE_NAME);
//
//  FOREACH col IN ARRAY cols
//  LOOP
//    SQL = SQL || format(' OR d.%s IS DISTINCT FROM s.%s', col, col);
//	END LOOP;
//  SQL = SQL || ');';
//  EXECUTE SQL;
//
//  ----------- INSERT MISSING RECORDS -------------------------------------------
//  SQL = format('INSERT INTO %s.%s(id,%s)
//                SELECT id,%s
//                  FROM %s.%s s
//                 WHERE NOT EXISTS( SELECT d.id
//                                     FROM %s.%s d
//                                    WHERE d.id = s.id );'
//              , link_shema, TG_TABLE_NAME
//              , array_to_string(cols,',')
//              , array_to_string(cols,',')
//              , TG_TABLE_SCHEMA, TG_TABLE_NAME
//              , link_shema, TG_TABLE_NAME
//              );
//  EXECUTE SQL;
//
//  ----------- DELETE EXTRA RECORDS ---------------------------------------------
//  SQL = format('DELETE
//                  FROM %s.%s d
//                 WHERE NOT EXISTS( SELECT s.id
//                                     FROM %s.%s s
//                                    WHERE s.id = d.id );'
//              , link_shema, TG_TABLE_NAME
//              , TG_TABLE_SCHEMA, TG_TABLE_NAME
//              );
//  EXECUTE SQL;
//
//  RETURN NULL;
//EXCEPTION
//WHEN others THEN
//	RAISE NOTICE '%', SQLERRM;
//  RETURN NULL;
//END;
//$body$
//LANGUAGE 'plpgsql'`
