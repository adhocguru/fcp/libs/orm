package orm

import (
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
)

const maxRelationDepth = 10

// Context all relations of current alias
func (t *syncDb) get_AliasRelationsInfo(al *alias) {

	server := al.Server

	// for all models by current alias read their relations if exists
	for _, mi := range modelCache.allOrdered(al, true) {
		if mi.schema != al.Server.Schema {

			if _, ok := t.relations[server]; !ok {
				t.relations[server] = make(relLocation)
			}

			// determine if relation placed to external server
			location := locLocal

			// find reference db alias of db location
			if _, ok := t.relations[server][location]; !ok {
				t.relations[server][location] = make(relAlias)
			}

			// find reference model of db alias
			if _, ok := t.relations[server][location][mi.al]; !ok {
				t.relations[server][location][mi.al] = make(relSchemas) // [schema]*modelInfo{}
			}

			if _, ok := t.relations[server][location][mi.al][mi.schema]; !ok || !utils.SliceContains(t.relations[server][location][mi.al][mi.schema], mi) {
				t.relations[server][location][mi.al][mi.schema] = append(t.relations[server][location][mi.al][mi.schema], mi)
			}
		}
		getFieldsRelationsInfo(t, al, mi, maxRelationDepth)
	}
	return
}

// Context relation info of model
func getFieldsRelationsInfo(this *syncDb, al *alias, mi *modelInfo, RelationStack int) {
	RelationStack--
	if RelationStack == 0 {
		return
	}

	server := al.Server

	for _, fi := range mi.fields.fieldsDB { // range model fields
		if fi.rel && al.Server.Schema != fi.relModelInfo.schema /*al.Server.Schema*/ {

			// find reference server
			if _, ok := this.relations[server]; !ok {
				this.relations[server] = make(relLocation)
			}

			// determine if relation placed to external server
			location := typeLocation(al.Server.sameAs(fi.relModelInfo.al.Server))

			// find reference db alias of db location
			if _, ok := this.relations[server][location]; !ok {
				this.relations[server][location] = make(relAlias)
			}

			// find reference model of db alias
			if _, ok := this.relations[server][location][fi.relModelInfo.al]; !ok {
				this.relations[server][location][fi.relModelInfo.al] = make(relSchemas) //[]*modelInfo{}
			}

			if _, ok := this.relations[server][location][fi.relModelInfo.al][fi.relModelInfo.schema]; !ok ||
				!utils.SliceContains(this.relations[server][location][fi.relModelInfo.al][fi.relModelInfo.schema], fi.relModelInfo) {

				if location == locRemote && !fi.relModelInfo.al.DbBaser.Support(db_link) {
					panic(fmt.Errorf("model %q has reference to database %q(model:%q), wich not support external link", mi.fullName, fi.relModelInfo.al.Server.Driver, fi.relModelInfo.fullName))
				}

				this.relations[server][location][fi.relModelInfo.al][fi.relModelInfo.schema] = append(this.relations[server][location][fi.relModelInfo.al][fi.relModelInfo.schema], fi.relModelInfo)
			}

			getFieldsRelationsInfo(this, al, fi.relModelInfo, RelationStack)
		}
	}
	return
}

func (t *syncDb) PrintRelations() {
	logger.Debugf("--------------- Relations -----------------")
	for server, relLocations := range t.relations {
		logger.Debugf("[%s] Server: %s %s:%d.%q(%s)", server.Schema, server.Driver, server.Host, server.Port, server.DbName, server.User)
		for tLoc, tAliases := range relLocations {
			logger.Debugf("\tLocation: %v", tLoc)
			for al, tModels := range tAliases {
				logger.Debugf("\t\t->Alias(%s)[%s]: %s-%s:%d-%q.%s[%s]", al.Name, al.Server.Schema, al.Server.Driver, al.Server.Host, al.Server.Port, al.Server.DbName, al.Server.Schema, al.Server.User)
				for schema, models := range tModels {
					logger.Debugf("\t\t\t->Schema[%s]: %s-%s:%d-%q.%s[%s]", schema, al.Server.Driver, al.Server.Host, al.Server.Port, al.Server.DbName, al.Server.Schema, al.Server.User)
					for i, mi := range models {
						logger.Debugf("\t\t\t\tModel %d. %s(%s)", i, mi.name, mi.table)
					}
				}
			}
		}
		logger.Debugf(" ")
	}
	logger.Debugf("-------------------------------------------")
}

//--------------------------------------------------------------
// создаются связи между таблицами баз данных на основе моделей
//--------------------------------------------------------------
//func (this *syncDb) get_DbExternalLinksSQL() {
//	for _, this.al = range this.als {
//		si := this.getShemaInfo(this.al)
//		if si.skip {
//			continue // не обрабатываем версии новее
//		}
//		// получаем алиасы ссылок на внешние данные
//		if remoteAliases, ok := this.relations[this.al.Server][locRemote]; ok {
//			// получаем список реплицируемых моделей для алиаса
//			for al, relModels := range remoteAliases {
//				sqls := []string{}
//				// список таблиц алиаса которые синхронизируются
//				var objects []string
//				var mi *modelInfo
//				for _, mi = range relModels {
//					objects = append(objects, mi.table)
//				}
//
//					this.al.Server.DbName, this.al.Server.Schema,
//					al.Server.DbName, al.Server.Schema, strings.Join(objects, ","))
//
//				// Crete remote server info
//				dbServer := *this.al.Server
//				dbServer.Alias  = fmt.Sprintf("%s_%s", this.al.Server.DbName, al.Server.Schema )
//				dbServer.Schema = al.Server.Schema
//
//				// Register remote server && databaase
//				DBServers.Add(&dbServer)
//				defer DBServers.Delete(&dbServer)
//
//				RegisterDatabase(&dbServer, 1, 1)
//				defer UnRegisterDataBase(dbServer.Alias)
//
//				// Crete db link to remote database
//				dbLink := &DBLink{
//					Alias: 		fmt.Sprintf("link_%s_%s", this.al.Server.DbName, al.Server.Schema),
//					GetPlayerActive:   	al.Server.Alias,
//					Remote:  	dbServer.Alias,
//					Objects:  	strings.Join(objects, ","),
//				}
//
//				// добавляем link remote db --> current db
//				err := RegisterDBLink( dbLink )
//				defer UnRegisterDBLink(dbLink)
//				if err != nil	{
//					panic(err.Errorf())
//				}
//
//				//------------------------------------------------------------------
//				// добавляем триггер синхронизации на удаленнуб таблицу
//				//------------------------------------------------------------------
//				// 1. проверка наличия триггерной функции( общая в паблик схеме )
//				//------------------------------------------------------------------
//				schema_public := mi.al.DbBaser.GetSchemaPublic()	// имя public схемы на сервере
//				fn_name := mi.getSyncroFuncName()					// имя триггерной функции
//
//				// Исходник функции в dbo
//				pr := &procInfo{
//					dbname: mi.al.Server.DbName,
//					schema: schema_public,
//					name: 	fn_name,
//					body: 	this.dbo.GetFunctionBody(pg_fn_tr_sync_2_link),
//					source: pg_fn_tr_sync_2_link}
//
//				// проверяем существует ли функция в базе
//				pit, _ := this.prs[pr.dbname][pr.schema][pr.name]
//
//				// сравниваем описание триггера с триггером в базе
//				if ok, msg := this.sql_func_compare(pr, pit); !ok {
//					// команды обновления триггера
//					sqls = append(sqls, pr.source)
//				}
//
//				// удаляем из списка фнкций обработанный
//				if pit != nil {
//					//delete(this.prs[pi.dbname][pi.schema], pi.name);
//					this.prs[pr.dbname][pr.schema][pr.name].used = true
//				}
//
//				//------------------------------------------------------------------
//				// 2. навешиваем триггер синхронизации на таблицы удаленного сервера
//				//------------------------------------------------------------------
//				// проходимся по списку моделей удаленного алиаса
//				for _, mi := range relModels {
//
//					// описание триггера для таблицы
//					tr := &triggerInfo{
//						dbname	  : mi.al.Server.DbName,
//						schema	  : mi.schema,
//						table	  : mi.table,
//						name   	  : mi.getSyncroTriggerName(dbLink.Alias),
//						fn_name   : fn_name,
//						fn_schema :	schema_public,
//						enabled	  : true,
//						desc	  : fmt.Sprintf("syncronization with %s.%s", dbLink.Alias, mi.table) }
//					tr.source = this.dbTriggerCreate_ASTM_IUDT(tr, fmt.Sprintf("'%s'", dbLink.Alias)) // CREATE TRIGGER( STATEMENT )
//
//					// проверяем существует ли триггер в базе
//					ti, _ := this.trs[mi.al.Server.DbName][mi.schema][tr.table][tr.name]
//
//					// сравниваем описание триггера с триггером в базе
//					if ok, msg := this.dbTriggerCompare(tr, ti); !ok {
//						// команды обновления триггера
//						sqls = append(sqls,
//							this.dbTriggerDrop(tr),				// DROP TRIGGER
//							tr.source,								// CREATE TRIGGER ( ROW )
//							this.dbTriggerComment(tr, tr.desc))	// COMMENT ON TRIGGER
//					}
//
//					// команда обновления заруска триттега для обновления данных на удаленном сервере
//					sqls = append(sqls, fmt.Sprintf("UPDATE %s set id = id WHERE id = %d", mi.table, math.MinInt32))
//
//					// удаляем из списка триггеров обработанный
//					if ti != nil {
//						delete(this.trs[mi.al.Server.DbName][mi.schema][tr.table], tr.name);
//					}
//				}
//				// Update structure
//				ExecSQL(al, &sqls, fmt.Sprintf("updating db objects of %s.%s for remote syncronization", al.Server.DbName, al.Server.Schema))
//			}
//		}
//	}
//	return
//}
