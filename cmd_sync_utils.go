package orm

import (
	"errors"
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"path"
	"runtime"
	"strconv"
	"strings"
)

func recoverPanic(_ interface{}, err *error) {

	logStack := func() (msgs []string) {
		i := 4
		for {
			pc, file, line, ok := runtime.Caller(i)
			if !ok {
				break
			}
			// make file path when was error
			pathElements := strings.Split(file, "/" /*string(os.PathSeparator)*/)
			index := utils.SliceIndex(pathElements, "src")
			if index >= 0 {
				pathElements = pathElements[index+1:]
			}
			file = path.Join(pathElements...)

			msgs = append(msgs, fmt.Sprintf("%s --> %s(%d)", file, path.Base(runtime.FuncForPC(pc).Name()), line))
			i++
			if file == path.Join(pathElements[0], "main.go") {
				break
			}
		}
		return
	}

	if r := recover(); r != nil {
		msg := fmt.Sprintf("PANIC %v", r)
		logger.Errorf("%s", strings.Repeat("-", len(msg)))
		logger.Errorf("%s", msg)
		logger.Errorf("> CALL STACK:")
		for _, msg := range logStack() {
			logger.Errorf("> %s", msg)
		}
		logger.Errorf(strings.Repeat("-", len(msg)))
		if err != nil {
			*err = errors.New(fmt.Sprintf("%v", r))
		}
	}

}

// Show current search path of user
func getDbSchemaSearchPathSQL(al *alias) string {
	thePath, err := al.DbBaser.GetUserSearchPath(al.DB, al.Server.User)
	if err != nil {
		logger.Errorf("%s", err.Error())
	} else {
		return thePath
	}
	return ""
}

// get version from string
func Str2Version(text string) float64 {
	var loc_ver float64
	var err error

	if loc_ver, err = strconv.ParseFloat(text, 64); err != nil {
		return 0
	}
	return loc_ver
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minInt64(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func maxInt64(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

// get database column type string.
func getColumnTyp(al *alias, fi *fieldInfo) (col string) {
	T := al.DbBaser.DbTypes()
	fieldType := fi.fieldType
	fieldSize := fi.size

checkColumn:
	switch fieldType {
	case TypeBooleanField:
		col = T["bool"]
	case TypeCharField, TypeVarCharField:
		if (al.DriverType == DRPostgres || al.DriverType == DRVertica) && fi.toText {
			col = T["string-text"]
		} else {
			col = fmt.Sprintf(T["string"], fieldSize)
		}
	case TypeTextField:
		col = T["string-text"]
	case TypeTimeField:
		col = T["time.Time-clock"]
	case TypeDateField:
		col = T["time.Time-date"]
	case TypeDateTimeField:
		col = T["time.Time"]
	case TypeBitField:
		col = T["int8"]
	case TypeSmallIntegerField:
		col = T["int16"]
	case TypeIntegerField:
		col = T["int32"]
	case TypeBigIntegerField:
		if al.DriverType == DRSqlite {
			fieldType = TypeIntegerField
			goto checkColumn
		}
		col = T["int64"]
	case TypePositiveBitField:
		col = T["uint8"]
	case TypePositiveSmallIntegerField:
		col = T["uint16"]
	case TypePositiveIntegerField:
		col = T["uint32"]
	case TypePositiveBigIntegerField:
		col = T["uint64"]
	case TypeFloatField:
		col = T["float64"]
	case TypeDecimalField:
		s := T["float64-decimal"]
		if !strings.Contains(s, "%d") {
			col = s
		} else {
			col = fmt.Sprintf(s, fi.digits, fi.decimals)
		}
	case TypeJSONField:
		if al.DriverType != DRPostgres {
			fieldType = TypeCharField
			goto checkColumn
		}
		col = T["json"]
	case TypeJsonbField:
		if al.DriverType != DRPostgres {
			fieldType = TypeCharField
			goto checkColumn
		}
		col = T["jsonb"]
	case RelForeignKey, RelOneToOne:
		ffi := fi.relModelInfo.fields.pk
		if ffi.rel {
			ffi = ffi.relModelInfo.fields.pk
		}
		fieldType = ffi.fieldType
		fieldSize = ffi.size
		goto checkColumn
	}

	return
}

// Context string value for the attribute "DEFAULT" for the CREATE, ALTER commands
func getColumnDefault(fi *fieldInfo) string {
	var (
		v, t, d string
	)

	// Skip default attribute if field is in relations
	if fi.rel || fi.reverse {
		return v
	}

	t = " DEFAULT '%s' "
	d = ""

	// These defaults will be useful if there no config value orm:"default" and NOT NULL is on
	switch fi.fieldType {
	// RUM { TypeTimeField can have a default value
	//case TypeTimeField, TypeDateField, TypeDateTimeField, TypeTextField:
	//	return v
	//case TypeDateField, TypeDateTimeField, TypeTextField:
	case TypeDateField, TypeDateTimeField:
		//return v
		t = " DEFAULT %s "
		d = "CURRENT_TIMESTAMP"
	case TypeTimeField:
		//t = " DEFAULT '%s' "
		d = "00:00"
		// RUM }
	case TypeBitField, TypeSmallIntegerField, TypeIntegerField,
		TypeBigIntegerField, TypePositiveBitField, TypePositiveSmallIntegerField,
		TypePositiveIntegerField, TypePositiveBigIntegerField, TypeFloatField,
		TypeDecimalField:
		t = " DEFAULT %s "
		d = "0"
	case TypeBooleanField:
		t = " DEFAULT %s "
		d = "FALSE"
	case TypeJSONField, TypeJsonbField:
		d = "{}"
	}

	if fi.colDefault {
		if !fi.initial.Exist() {
			v = fmt.Sprintf(t, d)
			//			v = fmt.Sprintf(t, "")
		} else {
			v = fmt.Sprintf(t, fi.initial.String())
		}
	} else {
		if !fi.null {
			v = fmt.Sprintf(t, d)
		}
	}

	return v
}
