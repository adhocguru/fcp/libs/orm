package orm

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"sync"
	"time"
)

// the Database driver's Name
const (
	DBMySQL    = "mysql"
	DBSqlite   = "sqlite3"
	DBTiDB     = "tidb"
	DBOracle   = "oracle"
	DBOra      = "ora"
	DBOci8     = "oci8"
	DBPostgres = "postgres"
	DBVertica  = "vertica"
)

// DriverType database driver constant int.
type DriverType int

// Enum the Database driver
const (
	_          DriverType = iota // int enum type
	DRMySQL                      // mysql
	DRSqlite                     // sqlite
	DRTiDB                       // TiDB
	DROracle                     // oracle
	DRPostgres                   // pgsql
	DRVertica                    // Vertica
)

const ODBC = "odbc"

// database driver string.
type driver string

// get type constant int of current driver..
func (d driver) Type() DriverType {
	a, _ := dataBaseCache.get(string(d))
	return a.DriverType
}

// get name of current driver
func (d driver) Name() string {
	return string(d)
}

// check driver iis implemented Driver interface or not.
var _ Driver = new(driver)

var (
	dataBaseCache = &_dbCache{cache: make(map[string]*alias)}
	drivers       = map[string]DriverType{
		DBMySQL:    DRMySQL,
		DBSqlite:   DRSqlite,
		DBTiDB:     DRTiDB,
		DBOracle:   DROracle,
		DBOra:      DROracle, //https://github.com/rana/ora
		DBOci8:     DROracle, // github.com/mattn/go-oci8
		DBPostgres: DRPostgres,
		DBVertica:  DRVertica,
	}
	dbBasers = map[DriverType]dbBaser{
		DRMySQL:    newdbBaseMysql(),
		DRSqlite:   newdbBaseSqlite(),
		DROracle:   newdbBaseOracle(),
		DRPostgres: newdbBasePostgres(),
		DRTiDB:     newdbBaseTidb(),
		DRVertica:  newdbBaseVertica(),
	}

	dbDriver = map[DriverType]string{
		DRVertica: ODBC,
	}
)

// database alias cacher.
type _dbCache struct {
	mux   sync.RWMutex
	cache map[string]*alias
}

// clear database aliases
func (ac *_dbCache) clear() {
	for k := range ac.cache {
		ac.delete(k)
	}
}

// add database alias with original name.
func (ac *_dbCache) delete(name string) {
	ac.mux.Lock()
	defer ac.mux.Unlock()
	if _, ok := ac.cache[name]; ok {
		//al.connected <- false	// close db
		delete(ac.cache, name)
	}
	return
}

// add database alias with original name.
func (ac *_dbCache) add(name string, al *alias) (added bool) {
	ac.mux.Lock()
	defer ac.mux.Unlock()
	if _, ok := ac.cache[name]; !ok {
		ac.cache[name] = al
		added = true
		al.openDB() // starting manager
	}
	return
}

// get database alias if cached.
func (ac *_dbCache) get(name string) (al *alias, ok bool) {
	ac.mux.RLock()
	defer ac.mux.RUnlock()
	al, ok = ac.cache[name]
	return
}

// get default alias.
func (ac *_dbCache) getDefault() (al *alias) {
	al, _ = ac.get(defaultDbAlias)
	return
}

// Clean databse cache. Then you can re-RegisterDataBase
func resetDatabaseCache() {
	dataBaseCache.clear()
}

func RegisterDatabase(dbi *DBServer, params ...int) (*DB, error) {
	if _, err := GetDB(dbi.Alias); err == nil { // Already registered
		return &DB{
			Alias: getDbAlias(dbi.Alias),
			Debug: dbi.Debug,
		}, nil
	}

	al := newAlias()

	al.Server = dbi
	al.Name = dbi.Alias
	al.DriverName = dbi.Driver
	al.TZ = time.UTC // Default TimeZone

	if dr, ok := drivers[dbi.Driver]; ok {
		al.DriverType = dr
		al.DbBaser = dbBasers[dr]
	} else {
		return nil, fmt.Errorf("driver name `%s` have not registered", dbi.Driver)
	}

	for i, v := range params {
		switch i {
		case 0:
			al.MaxIdleConns = v
		case 1:
			al.MaxOpenConns = v
		}
	}

	dataBaseCache.add(dbi.Alias, al)

	return &DB{
		Alias: al,
		Debug: dbi.Debug,
	}, nil

}

// UnRegisterDataBase with alias
func UnRegisterDataBase(aliasName string) {
	dataBaseCache.delete(aliasName)
}

// RegisterDriver Register a database driver use specify driver name, this can be definition the driver is which database type.
func RegisterDriver(driverName string, typ DriverType) error {
	if t, ok := drivers[driverName]; !ok {
		drivers[driverName] = typ
	} else {
		if t != typ {
			return fmt.Errorf("driverName `%s` db driver already registered and is other type", driverName)
		}
	}
	return nil
}

// DB Context *sql.DB from registered database by db alias name.
// Use "default" as alias name if you not set.
func GetDB(aliasNames ...string) (*sql.DB, error) {
	var name string
	if len(aliasNames) > 0 {
		name = aliasNames[0]
	} else {
		name = defaultDbAlias
	}
	al, ok := dataBaseCache.get(name)
	if ok {
		return al.DB, nil
	}
	return nil, fmt.Errorf("DataBase of alias name `%s` not found", name)
}

// RegisterDataBase Setting the database connect params. Use the database driver self dataSource args.
// Not Use in future
//func RegisterDatabase(aliasName, driverName, dataSource string, params ...int) error {
//	var (
//		err error
//		db  *sql.DB
//		al  *alias
//	)
//
//	db, err = sql.Open(driverName, dataSource)
//	if err != nil {
//		err = fmt.Errorf("register db `%s`, %s", aliasName, err.Errorf())
//		goto end
//	}
//
//	al, err = addAliasWthDB(aliasName, driverName, db)
//	if err != nil {
//		goto end
//	}
//
//	al.ConnDriver = driverName
//	al.ConnString = dataSource
//
//	detectTZ(al)
//
//	for i, v := range params {
//		switch i {
//		case 0:
//			SetMaxIdleConns(al.Name, v)
//		case 1:
//			SetMaxOpenConns(al.Name, v)
//		}
//	}
//
//end:
//	if err != nil {
//		if db != nil {
//			db.Close()
//		}
//		DebugLog.Println(err.Errorf())
//	}
//
//	return err
//}

//func RegisterDatabase(dbi *DBServer, params ...int) error {
//	var (
//		err error
//		db  *sql.DB
//		al  *alias
//	)
//
//	if _, err := DB(dbi.Alias); err == nil { // Already registered
//		return nil
//	}
//
//		dbi.Driver, dbi.DbName, dbi.Schema,	dbi.Host, dbi.Port,	dbi.User, dbi.Alias)
//
//	db, err = sql.Open(dbi.GetConnectionDriver(), dbi.GetConnectionString())
//	if err != nil {
//		err = fmt.Errorf("register db `%s`, %s", dbi.Alias, err.Errorf())
//		goto end
//	}
//
//	al, err = addAliasWthDB(dbi.Alias, dbi.Driver, db)
//	al.Server     = dbi
//	al.TZ         = time.UTC	// Default TimeZone
//
//	if err != nil {
//		goto end
//	}
//
//	err = db.Ping()
//	if err != nil {
//		err = fmt.Errorf("register db Ping `%s`, %s", dbi.Alias, err.Errorf())
//		goto end
//	}
//
//	detectTZ(al)
//
//	for i, v := range params {
//		switch i {
//		case 0:
//			SetMaxIdleConns(al.Name, v)
//		case 1:
//			SetMaxOpenConns(al.Name, v)
//		}
//	}
//
//end:
//	if err != nil {
//		if db != nil {
//			db.Close()
//		}
//		//DebugLog.Println(err.Errorf())
//		logs.GetBeeLogger().Errorf(err.Errorf())
//	}
//
//	return err
//}

// SetDataBaseTZ Change the database default used timezone
//func SetDataBaseTZ(aliasName string, tz *time.Location) error {
//	if al, ok := dataBaseCache.get(aliasName); ok {
//		al.TZ = tz
//	} else {
//		return fmt.Errorf("DataBase alias name `%s` not registered", aliasName)
//	}
//	return nil
//}

// SetMaxIdleConns Change the max idle conns for *sql.DB, use specify database alias name
//func SetMaxIdleConns(aliasName string, maxIdleConns int) {
//	al := getDbAlias(aliasName)
//	al.MaxIdleConns = maxIdleConns
//	al.DB.SetMaxIdleConns(maxIdleConns)
//}

// SetMaxOpenConns Change the max open conns for *sql.DB, use specify database alias name
//func SetMaxOpenConns(aliasName string, maxOpenConns int) {
//	al := getDbAlias(aliasName)
//	al.MaxOpenConns = maxOpenConns
//	// for tip go 1.2
//	if fun := reflect.ValueOf(al.DB).MethodByName("SetMaxOpenConns"); fun.IsValid() {
//		fun.Call([]reflect.Value{reflect.ValueOf(maxOpenConns)})
//	}
//}
