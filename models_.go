package orm

import (
	"fmt"
	"reflect"
	"strings"
	"unicode"
)

// FRONT
const (
	frontSTRING    = "STRING"
	frontNUMBER    = "NUMBER"
	frontLINK      = "LINK"
	frontDATE      = "DATE"
	frontTIME      = "TIME"
	frontTIMESTAMP = "TIMESTAMP"
	frontBOOL      = "BOOL"
	frontREAL      = "REAL"
)

// Front Object Model
var FrontObjectsList = make(map[string]FrontObject)

type FrontObject struct {
	Uri       string                     `json:"uri,omitempty"`
	KeyField  string                     `json:"keyField,omitempty"`
	FieldDesc map[string]FrontObjectDesc `json:"fieldDesc,omitempty"`
}

type FrontObjectDesc struct {
	DataType   string `json:"dataType,omitempty"`
	IsNullable bool   `json:"isNullable,omitempty"`
	IsArray    bool   `json:"isArray,omitempty"`
	LinkedObj  string `json:"linkedObj,omitempty"`
}

func ModelExists(ptrStructOrTableName interface{}) bool {
	mi, _ := getModel(ptrStructOrTableName)
	return mi != nil
}

func GetModelName(ptrStructOrTableName interface{}) string {
	mi, err := getModel(ptrStructOrTableName)
	if err != nil || mi == nil {
		return ""
	}
	return mi.name
}

func GetModel(ptrStructOrTableName interface{}) *modelInfo {
	mi, err := getModel(ptrStructOrTableName)
	if err != nil {
		panic(err)
	}
	return mi
}

func getModel(ptrStructOrTableName interface{}) (*modelInfo, error) {
	var name string
	var mi *modelInfo

	if ptrStructOrTableName == nil {
		panic(fmt.Errorf("orm: <GetModel> input parameter can't be NULL"))
	}

	if table, ok := ptrStructOrTableName.(string); ok {
		table = strings.TrimSpace(table)
		if table == "" {
			panic(fmt.Errorf("orm: <GetModel> input parameter `%v` can't be empty", table))
		}

		if unicode.IsLower(rune(table[0])) {
			// input - db table name
			name = snakeString(table)
			mi, _ = modelCache.get(name)
		} else {
			// input - model name
			name = table
			mi, _ = modelCache.getByModelName(name)
		}
	} else {
		name = getFullName(indirectType(reflect.TypeOf(ptrStructOrTableName)))
		mi, _ = modelCache.getByFullName(name)
	}

	if mi == nil {
		return nil, fmt.Errorf("orm: <GetModel> object name: `%s` not exists", name)
	}
	mr := *mi
	return &mr, nil
}

func GetModelField(ptrStructOrTableName interface{}, fieldName string) *fieldInfo {
	mi := GetModel(ptrStructOrTableName)

	tables := newDbTables(mi, mi.al.DbBaser)
	_, _, fi, ok := tables.parseExprs(mi, strings.Split(fieldName, ExprSep))
	if !ok {
		panic(fmt.Errorf("orm: <GetModelFieldInfo> model `%s` no contains field `%s`", GetModelName(ptrStructOrTableName), fieldName))
	}
	return fi
}

func GetModelUniqueIndexesWithField(ptrStructOrTableName interface{}) map[string][]string {
	mi := GetModel(ptrStructOrTableName)
	return mi.uniqFieldIdxs
}

func GetModelUniqueIndexesWithColumn(ptrStructOrTableName interface{}) map[string][]string {
	mi := GetModel(ptrStructOrTableName)
	return mi.uniqColumnIdxs
}

// returns map[aliasName]*[]modelInfo for replicated tables
//func GetReplicatedModels() map[string][]*modelInfo {
//	models := make(map[string][]*modelInfo)
//	for _, mi := range modelCache.all() {
//		if mi.virtual {
//			continue
//		}
//
//		replicated := true
//		if val, ok := getTableExData(mi.addrField)[DB_table_replica]; ok {
//			replicated = val.(bool)
//		}
//		if !replicated {
//			continue
//		}
//
//		if _, ok := models[mi.al.Name]; !ok {
//			models[mi.al.Name] = []*modelInfo{}
//		}
//		models[mi.al.Name] = append(models[mi.al.Name], mi)
//	}
//	return models
//}

func GetModels(includeVirtual ...bool) (models []*modelInfo) {
	incVirtual := append(includeVirtual, false)[0]
	for _, mi := range modelCache.all() {
		if mi.virtual && !incVirtual {
			continue
		}
		models = append(models, mi)
	}
	return models
}
