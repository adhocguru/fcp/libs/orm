package orm

import (
	"gitlab.com/adhocguru/fcp/libs/utils"
	"time"
)

const tagVer = "version"

type typeLocation bool

const (
	locRemote typeLocation = false
	locLocal  typeLocation = true
)

func (t typeLocation) String() string {
	if t == locRemote {
		return "Remote"
	}
	return "Local"
}

type mdType int

const (
	// A default md has mdType 0, which is invalid.
	mdRemote mdType = iota + 1
	mdDelUnused
	mdNoInit
	mdNoTest
)

type relSchemas map[string][]*modelInfo    // [schema]*modelInfo
type relAlias map[*alias]relSchemas        // [*alias][schema]*modelInfo
type relLocation map[typeLocation]relAlias // [typeLocation][*alias][schema]*modelInfo
type relServer map[*DBServer]relLocation   // [*DBServer][typeLocation][*alias][schema]*modelInfo
func (t relServer) findTable(ti *tableInfo, onServer DBServer, onLocations []typeLocation) bool {
	for server, relLocations := range t {
		// на другом сервере не интересуют
		if onServer != server.server() {
			continue
		}
		for tLoc, tAliases := range relLocations {
			// внешние ссылки не интересуют
			if !utils.SliceContains(onLocations, tLoc) {
				continue
			}
			// список моделей по алиасам
			for _, tModels := range tAliases {
				// список моделей в схеме
				for schema, models := range tModels {
					// интересует конкретная схема
					if schema != ti.schema {
						continue
					}
					for _, mi := range models {
						if mi.table == ti.table {
							return true
						}
					}
				}
			}
		}
	}
	return false
}

type userPath struct {
	db []string // path in db
	md []string // path of models
	wk []string // path for syncro
}

// sync database struct interface.
type syncDb struct {
	dbo       *dbo
	al        *alias               // work alias
	usr       map[*alias]*userPath // [*alias][]path
	als       []*alias
	alt       map[*alias][]string                                        // [*alias][]table
	dbs       map[DBServer]map[string]*schemaInfo                        // [DBServer][schema]*schemaInfo
	prs       map[DBServer]map[string]map[string]*pit                    // [DBServer][schema][func]*procInfo
	trs       map[DBServer]map[string]map[string]map[string]*triggerInfo // [DBServer][schema][table][trigger]*triggerInfo
	relations relServer
	// replica(bigdata)
	repl        *alias // replica alias
	repl_prefix string // replica alias schema_prefix
}

func newSyncDb() *syncDb {
	return &syncDb{
		usr:       make(map[*alias]*userPath),
		als:       make([]*alias, 0),
		alt:       make(map[*alias][]string),
		dbs:       make(map[DBServer]map[string]*schemaInfo),
		prs:       make(map[DBServer]map[string]map[string]*pit),
		trs:       make(map[DBServer]map[string]map[string]map[string]*triggerInfo),
		relations: make(relServer),
	}
}

// table saver
type tableSaver struct {
	model     *modelInfo
	sv_alias  *alias
	sv_schema string
}

func newTableSaver(mi *modelInfo) *tableSaver {
	ts := &tableSaver{}
	ts.model = mi
	ts.sv_alias = mi.al
	ts.sv_schema = mi.schema
	return ts
}

func (ts *tableSaver) IsDifferent() bool {
	return ts.model.al != ts.sv_alias || ts.model.schema != ts.sv_schema
}

func (ts *tableSaver) IsDifferentAlias() bool {
	return ts.model.al != ts.sv_alias
}

func (ts *tableSaver) IsDifferentSchema() bool {
	return ts.model.schema != ts.sv_schema
}

func (ts *tableSaver) Restore() {
	if ts.IsDifferent() {
		ts.model.al = ts.sv_alias
		ts.model.schema = ts.sv_schema
	}
}

// field saver
type fieldSaver struct {
	filed   *fieldInfo
	sv_auto bool
}

func newFiledSaver(fi *fieldInfo) *fieldSaver {
	this := &fieldSaver{}
	this.filed = fi
	this.sv_auto = fi.auto
	return this
}

func (t *fieldSaver) IsDifferent() bool {
	return t.filed.auto != t.sv_auto
}

func (t *fieldSaver) Restore() {
	if t.IsDifferent() {
		t.filed.auto = t.sv_auto
	}
}

type elapser struct {
	start time.Time
}

func newElapser() *elapser {
	return &elapser{time.Now()}
}

func (t *elapser) Duration() time.Duration {
	return time.Since(t.start)
}

func (t *elapser) Show(msg string) {
	logger.Debugf("orm: * finished %s elapsed:%v ", msg, time.Since(t.start))
}
