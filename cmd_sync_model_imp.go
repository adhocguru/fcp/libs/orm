package orm

import (
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"strconv"
	"strings"
)

func (t *syncDb) initSyncInfo() {
	//el := newElapser() defer el.Show("initSyncInfo()")

	for _, table := range modelCache.orders {
		al := modelCache.cache[table].al

		if utils.SliceContains(t.als, al) {
			continue
		}
		t.als = append(t.als, al)

		t.al = al

		t.test_User(al)              // test user provilege
		t.get_AliasRelationsInfo(al) // Load relation info of current alias to external models
		sqls := t.getSchemas(al)     // get schemas info db+Models

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}
		// Exec commands
		t.ExecSQL(al, &sqls, fmt.Sprintf("set user parameters for %s:%s.%s", al.Server.Driver, al.Server.DbName, al.Server.Schema))
	}
}

// Получить локальную и серверную версии схемы базы данных
func (t *syncDb) db_GetVersions() (dbo_ver, db_ver float64) {
	// Читаем локальную версию

	ver, err := t.dbo.find(t.al.Name, tagVer)
	if err != nil {
		panic(err)
	}
	dbo_ver = Str2Version(strings.TrimSpace(*ver))

	// Читаем версию БД
	desc, err := t.al.DbBaser.GetShemaDescription(t.al.DB, t.al.Server.Schema)
	if err != nil {
		panic(err)
	}
	db_ver = Str2Version(desc)
	return
}

func (t *syncDb) test_User(al *alias) {

	//--------------------------------
	// Test User...
	//--------------------------------
	// Test User if exists
	if ok, err := al.DbBaser.UserExists(al.DB, al.Server.User); err != nil {
		panic(err)
	} else if !ok {
		panic(fmt.Errorf("orm: %s:%s user:%q is not exists. must create before test.", al.Server.Driver, al.Server.DbName, al.Server.User))
	}
	// Test User - have superuser privilege
	ok, err := al.DbBaser.UserIsSuperuser(al.DB, al.Server.User)
	if err != nil {
		panic(err)
	} else if !ok {
		panic(fmt.Errorf("orm: %s:%s[user: %q] is nave no superuser privilege.", al.Server.Driver, al.Server.DbName, al.Server.User))
	}
	return
}

func (t *syncDb) dbCreateStructureByModels() {
	//el := newElapser()	defer el.Show("dbCreateStructureByModels()")

	for _, al := range t.als {
		logger.Infof("orm: testing structure of %s.%s(%s)...", al.info(), al.Server.Schema, getDbSchemaSearchPathSQL(al))

		t.al = al

		// получить команды модификации структуры БД
		models := modelCache.allOrdered(al, false)
		sqls := t.get_SyncTableStructureSQL(al, models)

		// Check version of database
		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
			continue
		}
		// Exec commands
		t.ExecSQL(al, &sqls, fmt.Sprintf("synchronization tables structure %q", al.Name))
	}
	return
}

// репликация моделей - отдельно
func (t *syncDb) dbCreateReplicaByModels() {

	if t.repl == nil {
		//logger.Warnf("orm: replica server not defined, skipping...")
		return
	}
	//el := newElapser()	defer el.Show("dbCreateReplicaByModels()")
	logger.Infof("orm: testing structure of replica...")

	for _, al := range t.als {
		// данные базы где находится реплика не обрабатываем
		if al == t.repl {
			continue
		}

		logger.Infof("orm: testing structure of replica %s --> %s...", al.info(), t.repl.info())

		t.al = al

		// получить команды модификации структуры БД в реплике
		models := modelCache.allOrdered(al, false)
		sqls := t.get_SyncTableStructureSQL(t.repl, models, mdNoInit)

		// Check version of database
		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
			continue
		}

		// Exec commands
		t.ExecSQL(t.repl, &sqls, fmt.Sprintf("updating replica structure %s --> %s...", al.info(), t.repl.info()))
	}
	return
}

// репликация моделей - отдельно
//func (this *syncDb) db_init_ReplicaModels() {
//
//	if this.repl == nil {
//		return
//	}
//	el := newElapser(); defer el.Show("db_init_ReplicaModels()");
//
//	for _, al := range this.als {
//		// данные базы где находится реплика не обрабатываем
//		if al == this.repl {
//			continue
//		}
//
//		// Check version of database
//		if this.WrongShemaVersion(al, logs.LevelError) {
//			continue
//		}
//
//			al.Server.Driver, al.Server.Alias, this.repl.Server.Driver, this.repl.Server.Alias)
//
//		var repl_cnt, data_cnt int64
//
//		models := make(map[*modelInfo]int64)
//		for _, mi := range modelCache.allOrdered(al,false) {
//			used := true
//			if val, ok := getTableExData(mi.addrField)[DB_table_replica]; ok {
//				used = val.(bool)
//			}
//			if !used {
//				continue
//			}
//
//			// проверяем есть ли запись для таблицы с -1/*ALL*/
//			query := fmt.Sprintf(`SELECT COUNT(*)
//                                          FROM %s m
//                                         WHERE table_name = '%s'
//                                           AND oper = 0/*INSERT*/
//                                           AND rec_id = -1/*ALL*/;`, mi.getReplicaTableName(), mi.table)
//			if  err := al.DB.QueryRow(query).Scan(&data_cnt); err != nil {
//				panic(fmt.Errorf("orm : %v [%s:%s.%s.%s %s]", err, this.repl.Server.Driver, this.repl.Server.DbName, mi.schema, mi.table, query))
//			}
//			if data_cnt > 0 {
//				continue
//			}
//
//			// count records from replica
//			query = fmt.Sprintf(`SELECT count(*) from %s;`, mi.table)
//			if  err := this.repl.DB.QueryRow(query).Scan(&repl_cnt); err != nil {
//				panic(fmt.Errorf("orm : %v [%s:%s.%s.%s %s]", err, this.repl.Server.Driver, this.repl.Server.DbName, mi.schema, mi.table, query))
//			}
//
//			// no record in replica
//			if repl_cnt == 0 {
//				models[mi] = -1 // загрузить все записи
//				continue
//			}
//
//			// count records from data
//            query = fmt.Sprintf(`SELECT COUNT(*)
//                                          FROM %s m
//                                         WHERE NOT EXISTS( SELECT 1
//                                                             FROM %s
//                                                            WHERE table_name = '%s'
//                                                              AND oper = 0/*INSERT*/
//                                                              AND rec_id = m.id);`, mi.table, mi.getReplicaTableName(), mi.table)
//			if  err := al.DB.QueryRow(query).Scan(&data_cnt); err != nil {
//				panic(fmt.Errorf("orm : %v [%s:%s.%s.%s %s]", err, this.repl.Server.Driver, this.repl.Server.DbName, mi.schema, mi.table, query))
//			}
//
//			// записи не равны
//			if repl_cnt < data_cnt {
//				models[mi] = -1 // загрузить все записи // TODO только разницу если меньше половины
//			}
//
//		}
//		sqls := []string{}
//		if len(models) > 0 {
//			for mi, val := range models {
//				sqls = append(sqls, fmt.Sprintf(`('%s', 0, %v)`, mi.table, val))
//			}
//			replicaStoreTable := (&modelInfo{}).getReplicaTableName()
//			sql := fmt.Sprintf(`INSERT INTO %s(table_name, oper, rec_id) VALUES`, replicaStoreTable) + strings.Join(sqls, ",")
//			sqls = []string{sql}
//		}
//
//		// Exec commands
//		this.ExecSQL(al, &sqls, fmt.Sprintf("updating replicated infomation %s:%-10q --> %s:%q...",
//			al.Server.Driver, al.Server.Alias, this.repl.Server.Driver, this.repl.Server.Alias))
//	}
//	return
//}

// returns:
// [fullTableName]sql
// [fullTableName]*dbCommentInfo
// [fullTableName][column]*dbCommentInfo
// [fullTableName][fullIndexName]*indexInfo
func (t *syncDb) get_ModelCreateSQL(al *alias, models []*modelInfo /*, isReplica bool*/) (
	mdTables map[string]string,
	mdTableComments map[string]*dbCommentInfo,
	mdTableColumnComments map[string]map[string]*dbCommentInfo,
	mdTableIndexes map[string]map[string]*indexInfo) {
	Q := al.DbBaser.TableQuote()
	T := al.DbBaser.DbTypes()
	I := al.DbBaser.DbExData()

	mdTables = make(map[string]string)                                 // [fullTableName]sql
	mdTableComments = make(map[string]*dbCommentInfo)                  // [fullTableName]*dbCommentInfo
	mdTableColumnComments = make(map[string]map[string]*dbCommentInfo) // [fullTableName][column]*dbCommentInfo
	mdTableIndexes = make(map[string]map[string]*indexInfo)            // [fullIndexName]*indexInfo

	t_indx_enabled := al.DbBaser.Support(db_index)             // can use indexes for the database
	t_desc_enabled := al.DbBaser.Support(db_table_desc)        // can use table desc for the database
	c_desc_enabled := al.DbBaser.Support(db_table_column_desc) // can use column table desc for the database
	existSQL := al.DbBaser.GetIFNotExistsSQL()                 // IF NOT EXISTS support ddl

	for _, mi := range models {
		if mi.model == nil {
			continue
		}

		// полное имя таблицы модели с учетом схемы
		fullTableName := mi.getFullTableName()

		// таблица имеет ручной скрипт миграции func (T) TableMigration() string
		if getTableIsExtMigration(mi.addrField) {
			cmd := getTableMigration(mi.addrField)
			mdTables[fullTableName] = cmd
			continue
		}

		columns := make([]string, 0, len(mi.fields.fieldsDB)) // []columnName
		columnsComments := make(map[string]*dbCommentInfo)    // [columnName]*indexInfo
		tableIndexes := make(map[string]*indexInfo)           // [fullIndexName]*indexInfo

		//sql := fmt.Sprintf("-- Table Structure of `%s`\n", mi.fullName)
		sql := fmt.Sprintf("CREATE TABLE %s %s%s%s.%s%s%s (\n", existSQL, Q, mi.schema, Q, Q, mi.table, Q)
		for _, fi := range mi.fields.fieldsDB {

			//if isReplica {
			//	fs := newFiledSaver(fi)
			//	defer fs.Restore() // хранитель данных
			//
			//	// в реплике нет автоинкементных полей
			//	fi.auto = false
			//}

			column := fmt.Sprintf("    %s%s%s ", Q, fi.column, Q)
			col := getColumnTyp(al, fi)

			if fi.auto {
				switch al.DriverType {
				case DRSqlite, DRPostgres, DRVertica:
					column += T[tag_auto]
				default:
					column += col + " " + T[tag_auto]
				}
			} else if fi.pk {
				column += col + " " + T[tag_pk]
				idx := fmt.Sprintf("%s_pkey", mi.table)
				mi.uniqFieldIdxs[idx] = []string{fi.name}
				mi.uniqColumnIdxs[idx] = []string{fi.column}
			} else {
				column += col

				if !fi.null {
					column += " " + "NOT NULL"
				}

				//if fi.initial.String() != "" {
				//	column += " DEFAULT " + fi.initial.String()
				//}

				// Append attribute DEFAULT
				column += getColumnDefault(fi)

				if fi.unique && t_indx_enabled {
					//column += " UNIQUE" - will make as constraint
					// idx info
					idx := newIndexInfo().TableBy(mi).ColunmsBy([]*fieldInfo{fi}).Unique(true).Constraint(true).Get()
					tableIndexes[idx.getFullIndexName()] = idx
					mi.uniqFieldIdxs[idx.IndexName] = []string{fi.name}
					mi.uniqColumnIdxs[idx.IndexName] = []string{fi.column}
				}

				if fi.index && t_indx_enabled {
					// index info
					idx := newIndexInfo().TableBy(mi).ColunmsBy([]*fieldInfo{fi}).Get()
					tableIndexes[idx.getFullIndexName()] = idx
				}
			}

			if strings.Contains(column, "%COL%") {
				column = strings.Replace(column, "%COL%", fi.column, -1)
			}

			//if fi.description != "" {
			//	column += " " + fmt.Sprintf("COMMENT '%s'",fi.description)
			//}

			columns = append(columns, column)

			// Комментарий к колонке таблицы
			if c_desc_enabled {
				dbCommentInfo := newDbCommentInfo(db_table_column_desc, mi)
				dbCommentInfo.column = fi.column
				dbCommentInfo.dbComment.Object = fi.json
				dbCommentInfo.dbComment.Desc = new(string)
				*dbCommentInfo.dbComment.Desc = fi.description
				columnsComments[fi.column] = dbCommentInfo
			}
		}

		// -- Comment of tables's column ---
		mdTableColumnComments[fullTableName] = columnsComments

		// UNIQUE constraints(from model): CONSTRAINT <constraint_name> UNIQUE (<column_name>)
		for _, idx := range tableIndexes {
			if idx.IsConstraint {
				column := idx.GetConstraintSQL()
				columns = append(columns, column)
			}
		}

		// UNIQUE constraints(from model.TableUnique()):CONSTRAINT <constraint_name> UNIQUE (<column_name>[,<column_name>][,<column_name>]...)
		allnames := getTableUnique(mi.addrField)
		if !mi.manual && len(mi.uniques) > 0 {
			allnames = append(allnames, mi.uniques)
		}

		for _, names := range allnames {
			cols := make([]string, 0, len(names))
			flds := make([]string, 0, len(names))
			idx_fis := make([]*fieldInfo, 0, len(names)) // list of index's fieldInfo
			for _, name := range names {
				if fi, ok := mi.fields.GetByAny(name); ok && fi.dbcol {
					cols = append(cols, fi.column)
					flds = append(cols, fi.name)
					idx_fis = append(idx_fis, fi)
				} else {
					panic(fmt.Errorf("orm: cannot found column `%s` when parse UNIQUE in `%s.TableUnique`", name, mi.fullName))
				}
			}

			idx := newIndexInfo().TableBy(mi).ColunmsBy(idx_fis).Unique(true).Constraint(true).Get()

			fullIndexName := idx.getFullIndexName()
			// проверим не существует ли такой же UNIQUE constraint в описании поля модели или не дублируется в списке
			if ide, ok := tableIndexes[fullIndexName]; ok {
				msgIndexRepeated(mi, idx, ide) // индекс описан повторно
				continue
			}
			tableIndexes[fullIndexName] = idx
			mi.uniqFieldIdxs[idx.IndexName] = flds
			mi.uniqColumnIdxs[idx.IndexName] = cols

			// добавляем команду
			column := idx.GetConstraintSQL()
			columns = append(columns, column)
		}

		sql += strings.Join(columns, ",\n")
		sql += "\n)"

		if al.DriverType == DRMySQL {
			var engine string
			if mi.model != nil {
				engine = getTableEngine(mi.addrField)
			}
			if engine == "" {
				engine = al.Engine
			}
			sql += " ENGINE=" + engine
		}

		sql += ";"
		mdTables[fullTableName] = sql

		// ----------------- Comment of table ------------------------------
		if t_desc_enabled {
			if mi.model != nil {
				dbCommentInfo := newDbCommentInfo(db_table_desc, mi)

				for attr, value := range getTableExData(mi.addrField) {
					if _, ok := I[attr]; ok {
						switch attr {
						case DB_table_desc:
							dbCommentInfo.dbComment.Desc = new(string)
							*dbCommentInfo.dbComment.Desc = fmt.Sprintf("%v", value)
						case DB_table_notifier:
							dbCommentInfo.dbComment.Notifier = new(string)
							*dbCommentInfo.dbComment.Notifier = mi.getEventName()
						}
					}
				}
				mdTableComments[fullTableName] = dbCommentInfo
			}
		}

		//---------------------------------------------------------------
		// INDEXES
		//---------------------------------------------------------------
		if t_indx_enabled {

			// Indexes UNIQUE from model.TableUnique() -- обработано при чтении полей модели
			//for _, names := range getTableUnique(mi.addrField) {
			//	idx_fis := make([]*fieldInfo, 0, len(names)) // list of index's fieldInfo
			//	for _, name := range names {
			//		if fi, ok := mi.fields.GetByAny(name); ok && fi.dbcol {
			//			idx_fis = append( idx_fis, fi)
			//		} else {
			//			panic(fmt.Errorf("orm: cannot found column `%s` when parse INDEX in `%s.TableUnique`", name, mi.fullName))
			//		}
			//	}
			//	// construct index data
			//	idx := new_dbIndex(mi, idx_fis)
			//	idx.IsUnique = true
			//	idx.IsConstraint = true
			//
			//	// check if exists in index datas
			//	fullIndexName := idx.getFullIndexName()
			//	if _, ok := tableIndexes[fullIndexName]; !ok {
			//		tableIndexes[fullIndexName] = idx
			//	}
			//}

			// --- NORMAL INDEXES from model.TableIndex()
			for _, names := range getTableIndex(mi.addrField) {
				idx_fis := make([]*fieldInfo, 0, len(names)) // list of index's fieldInfo
				for _, name := range names {
					if fi, ok := mi.fields.GetByAny(name); ok && fi.dbcol {
						idx_fis = append(idx_fis, fi)
					} else {
						panic(fmt.Errorf("orm: cannot found column `%s` when parse INDEX in `%s.TableIndex`", name, mi.fullName))
					}
				}
				// construct index data
				idx := newIndexInfo().TableBy(mi).ColunmsBy(idx_fis).Get()

				fullIndexName := idx.getFullIndexName()
				// проверим не существует ли такой же UNIQUE constraint в описании поля модели или не дублируется в списке
				if ide, ok := tableIndexes[fullIndexName]; ok {
					msgIndexRepeated(mi, idx, ide) // индекс описан повторно
					continue
				}
				tableIndexes[fullIndexName] = idx
			}

			// Indexes DESC/ASC (COMPLEX)
			//if mi.model != nil {
			//	for _, names := range getTableIndexEx(mi.addrField) {
			//		cols := make([]string, 0, len(names))
			//		idx_cols := make([]*fieldInfo, 0, len(names))
			//		for _, name := range names {
			//			if fi, ok := mi.fields.GetByAny(name[0]); ok && fi.dbcol {
			//				cols = append(cols, fi.column + " " + name[1])
			//			} else {
			//				panic(fmt.Errorf("orm: cannot found column `%s` when parse INDEX in `%s.TableIndex`", name, mi.fullName))
			//			}
			//		}
			//		//sqlIndexes = append(sqlIndexes, cols)
			//		sql_Indexes = append(sql_Indexes, idx_cols)
			//	}
			//}
		} //------- END INDEXES --------------------------------------------
		mdTableIndexes[fullTableName] = tableIndexes
	}
	return
}

/* args[0] - it flag table is sync from remote server*/
func (t *syncDb) get_SyncTableStructureSQL(al *alias, models []*modelInfo, Options ...mdType) (sqls []string) {
	sqls = []string{}

	if len(models) == 0 {
		return
	}

	// определение режима репликации
	isReplica := t.repl != nil && al != models[0].al // реплицируемый сервер и сервер моделей разные

	if isReplica {
		// всем моделям переопределяем alias & schema
		for _, mi := range models {

			// схема на реплике для модели
			schema := fmt.Sprintf("%s%s", t.repl_prefix, mi.al.Server.Schema)

			ts := newTableSaver(mi)
			defer ts.Restore() // хранитель данных mi
			mi.al = al         // сервер реплики
			mi.schema = schema // схема на сервере реплики
		}
	}

	//---------------------------------------------------------------------------
	// получение информации о таблицах и индексах указанного алиаса БД из модели
	//---------------------------------------------------------------------------
	mdTables, mdTableComments, mdTableColumnComments, mdTableIndexes := t.get_ModelCreateSQL(al, models /*, isReplica*/)

	//----------------------------------------------------------------------------
	// получение информации о таблицах и индексах указанного алиаса БД из сервера
	//----------------------------------------------------------------------------
	// get supports for database
	dbTbl_desc_enabled := al.DbBaser.Support(db_table_desc)        // can use table desc for the database
	dbCol_desc_enabled := al.DbBaser.Support(db_table_column_desc) // can use column desc for the database
	dbTbl_indx_enabled := al.DbBaser.Support(db_index)             // can use indexes for the database

	if isReplica {
		dbTbl_desc_enabled = false
		dbCol_desc_enabled = false
	}
	Q := al.DbBaser.TableQuote()

	// Alias used schemas( main + inner)
	schemas := t.getAliasInnerSchemas(al)

	db_Tables, err := al.DbBaser.GetTables(al.DB, schemas...)
	if err != nil {
		panic(err)
	}

	// get table descriptions list in database
	db_TableComments := make(map[string]string)
	if dbTbl_desc_enabled {
		db_TableComments, err = al.DbBaser.GetTablesComment(al.DB, schemas...)
		if err != nil {
			panic(err)
		}
	}

	// get table indexes list in database
	db_TableIndexes := make(map[string]idxs)
	if dbTbl_indx_enabled {
		db_TableIndexes, err = al.DbBaser.GetTablesIndexes(al.DB, schemas...)
		if err != nil {
			panic(err)
		}
	}

	flagChangeStructure := false // флаг изменения структуры

	// проверяем модели из списка
	for _, mi := range models {

		fullTableName := mi.getFullTableName() // fullName исходной модели

		// проверяем наличие описания в БД
		ti := t.findTable(db_Tables, mi, isReplica)
		if ti == nil {
			ti = &tableInfo{schema: mi.schema, table: mi.table, exist: false}
			db_Tables[fullTableName] = ti // неописано
		}

		var ts *tableSaver = nil

		// таблица используем внешнюю миграцию
		if getTableIsExtMigration(mi.addrField) {
			sqls = append(sqls, mdTables[fullTableName])
			continue
		}

		// Check if table exists
		if ti.exist {
			if mi.schema != ti.schema {

				// таблица находится не в той схеме
				msgTableLocateIncorrect(ti, mi)

				// переопределяем model
				ts = newTableSaver(mi)
				defer ts.Restore()    // хранитель данных mi
				mi.schema = ti.schema // переопределяем для mi схему для дальнейшей проверки

				flagChangeStructure = true // флаг изменения структуры
			}
			//fullTableName := mi.getFullTableName() // ключ рабочей модели

			// список полей таблицы
			columns, err := al.DbBaser.GetColumns(al.DB, mi.schema, mi.table) // список полей таблицы
			if err != nil {
				panic(err)
			}
			// список комментариев полей таблицы
			c_desc, err := al.DbBaser.GetColumnsDescription(al.DB, mi.schema, mi.table) // список описний полей таблицы
			if err != nil {
				panic(err)
			}

			// список используемых полей таблицы
			colused := make(map[string]bool)
			for col := range columns { // init map
				colused[col] = false
			}

			// --------------------------------------------------
			// -------------Make list of new table columns ------
			// --------------------------------------------------
			var new_columns []*fieldInfo            // list of new columns
			for _, fi := range mi.fields.fieldsDB { // range orm structure
				colused[fi.column] = true // field used in orm

				if _, ok := columns[fi.column]; !ok { // field not present in structure
					new_columns = append(new_columns, fi) // add to map of new columns of table
					continue
				}

				//--------------------------------------------------
				// field not new
				//--------------------------------------------------
				// проверим NULL параметры поля
				col_type := strings.ToUpper(columns[fi.column][1])
				//col_null := true
				//if columns[fi.column][2] == "NO" {
				//	col_null = false
				//}
				col_null := columns[fi.column][2] == "true" // column is_nullable
				if fi.null != col_null {
					if !col_null { // снимаем NOT NULL признак поля
						logger.Warnf("orm: column %q.%s.%s.%s(type:%v) [model.null:%v <-> db.null:%v] was changed. Dropping NOT NULL constraint...", al.Server.DbName, mi.schema, mi.table, fi.column, col_type, fi.null, col_null)
						sqls = append(sqls, fmt.Sprintf("ALTER TABLE %s.%s ALTER COLUMN %s DROP NOT NULL;", mi.schema, mi.table, fi.column))
						flagChangeStructure = true // флаг изменения структуры
					} else {
						// устанавливаем NOT NULL признак поля если возможно
						//if !mi.al.DbBaser.Support(db_script) {
						//	//return "/*NOT SUPPORT SCRIPT - it is not possible to verify the data*/"
						//} else {
						//	if !fi.rel && !fi.reverse {
						//		// получаем def значение для поля
						//		def := strings.Replace(getColumnDefault(fi), " DEFAULT ", "",1)
						//		sql := fmt.Sprintf(
						//			`DO $$ BEGIN
						//					  UPDATE %s%s%s.%s%s%s SET %s%s%s = %s WHERE %s%s%s IS NULL;
						//					  ALTER TABLE %s%s%s.%s%s%s ALTER COLUMN %s%s%s SET NOT NULL;
						//					EXCEPTION WHEN others THEN RAISE; END $$;`,
						//					Q, mi.schema, Q, Q, mi.table, Q, Q, fi.column, Q, def, Q, fi.column, Q,
						//			        Q, mi.schema, Q, Q, mi.table, Q, Q, fi.column, Q )
						//		sqls = append(sqls, sql)
						//	}
						//}
						if !fi.rel && !fi.reverse {
							logger.Warnf("orm: column %q.%s.%s.%s(type:%v) [model.null:%v <-> db.null:%v] was changed. Setting NOT NULL constraint...", al.Server.DbName, mi.schema, mi.table, fi.column, col_type, fi.null, col_null)

							//-------------------------------------
							// проверяем наличие UNIQUE constraint on field
							//-------------------------------------
						checkIndex:
							for inm, idx := range db_TableIndexes[fullTableName] {
								if idx.IsPrimary || !idx.IsConstraint {
									continue
								}
								if utils.SliceContains(idx.Columns, fi.column) {
									// DROP UNIQUE constraint
									sqls = append(sqls, al.DbBaser.ConstraintDropQuery(mi.schema, mi.table, idx.IndexName))
									// обманка
									delete(db_TableIndexes[fullTableName], inm)
									goto checkIndex
								}
							}

							// получаем def значение для поля
							def := strings.Replace(getColumnDefault(fi), " DEFAULT ", "", 1)
							sqls = append(sqls, fmt.Sprintf(`UPDATE %s%s%s.%s%s%s SET %s%s%s = %s WHERE %s%s%s IS NULL;`, Q, mi.schema, Q, Q, mi.table, Q, Q, fi.column, Q, def, Q, fi.column, Q))
							sqls = append(sqls, fmt.Sprintf(`ALTER TABLE %s%s%s.%s%s%s ALTER COLUMN %s%s%s SET NOT NULL;`, Q, mi.schema, Q, Q, mi.table, Q, Q, fi.column, Q))
							flagChangeStructure = true // флаг изменения структуры
						}
					}
				}

				// если support column desc && отсутствует комментарий к полю таблицы и или не совпадает с описанием
				if dbCol_desc_enabled {
					tableColumnComment, ok := mdTableColumnComments[fullTableName][fi.column]

					if !ok || c_desc[fi.column] != tableColumnComment.GetDbComment().AsJson() {
						// скрипт добавления коментария к полю
						if tableColumnComment != nil {
							sqls = append(sqls, tableColumnComment.GetSQL(mi.schema))
						}
					}
				}
			}

			//----------------------------------------------------------------------------------------------------------
			// ------------ create sql of new table columns ------------------------------------------------------------
			//----------------------------------------------------------------------------------------------------------
			for _, fi := range new_columns {
				msgColumnNotFound(fi)
				sqls = append(sqls, t.dbTableColumnAdd(fi))

				if dbCol_desc_enabled {
					if tableColumnComment, ok := mdTableColumnComments[fullTableName][fi.column]; ok {
						sqls = append(sqls, tableColumnComment.GetSQL(mi.schema)) // скрипт добавления коментария к полю
					}
				}
				flagChangeStructure = true // флаг изменения структуры
			}

			//----------------------------------------------------------------------------------------------------------
			// -------- Check unused fields ----------------------------------------------------------------------------
			//----------------------------------------------------------------------------------------------------------
			for column, column_used := range colused {
				if column_used {
					continue
				}

				col_type := strings.ToUpper(columns[column][1])
				col_null := ""
				if columns[column][2] == "NO" {
					col_null = "NOT NULL"
				}

				// if table not syncronized from external server - delete t field
				if utils.SliceContains(Options, mdDelUnused) {
					msgColumnNotUsed(mi, column)
					sqls = append(sqls, t.dbTableColumnDel(mi, column))
					flagChangeStructure = true // флаг изменения структуры
					continue
				}

				// --- снимаем описание поля если есть ---
				if dbCol_desc_enabled {
					if c_desc[column] != "" {
						msgColumnNotUsedDelDesc(mi, column)
						sqls = append(sqls, dbCommentInfo{dbType: db_table_column_desc, schema: mi.schema, table: mi.table, column: column}.GetSQL(mi.schema))
						flagChangeStructure = true // флаг изменения структуры
					}
				}

				// -- снимаем NOT NULL признак поля если есть ---
				if col_null == "NOT NULL" {
					msgColumnNotUsedDelNotNull(mi, column, col_type, col_null)
					logger.Warnf("orm: column %q.%s.%s.%s(%v %v) more not used CRM. Dropping NOT NULL constraint...", al.Server.DbName, mi.schema, mi.table, column, col_type, col_null)
					sqls = append(sqls, t.dbTableColumnDelNotnull(mi, column))
					flagChangeStructure = true // флаг изменения структуры
				}
			}

			//----------------------------------------------------------------------------------------------------------
			// --- Desc of table ---
			//----------------------------------------------------------------------------------------------------------
			if dbTbl_desc_enabled {
				if md_tableComment, ok := mdTableComments[fullTableName]; ok {
					if db_TableComment, ok := db_TableComments[fullTableName]; !ok || db_TableComment != md_tableComment.GetDbComment().AsJson() {
						sqls = append(sqls, md_tableComment.GetSQL(mi.schema))
					}
				}
			}

			//----------------------------------------------------------------------------------------------------------
			// create sql of new indexes columns
			//----------------------------------------------------------------------------------------------------------
			if dbTbl_indx_enabled {
				db_tableIndexes := db_TableIndexes[fullTableName] // db indexes of table

				for _, idx := range mdTableIndexes[fullTableName] {

					// ключ индекса в базе по модели
					fullIndexName := newIndexInfo().CopyBy(idx).TableBy(mi).Get().getFullIndexName()

					// ищем индекс в данных индексов таблицы в базе
					if db_idx, ok := db_tableIndexes[fullIndexName]; ok {
						// test index fields
						if idx.ColumnsCommaText != db_idx.ColumnsCommaText {
							msgIndexNotCorrespond(mi, idx, db_idx)
							// delete && create index
							if db_idx.IsConstraint {
								sqls = append(sqls, al.DbBaser.ConstraintDropQuery(mi.schema, mi.table, db_idx.IndexName))
							} else {
								sqls = append(sqls, al.DbBaser.IndexDropQuery(mi.schema, db_idx.IndexName))
							}
							sqls = append(sqls, idx.GetSQL(mi.schema)...)
							flagChangeStructure = true // флаг изменения структуры
						}
						// delete index from list of db indexes of table
						delete(db_tableIndexes, fullIndexName)
					} else {
						// index not found
						msgIndexNotFound(mi, idx)
						sqls = append(sqls, idx.GetSQL(mi.schema)...)
						flagChangeStructure = true // флаг изменения структуры
					}
				}

				//-------------------------------------
				// process unused db indexes of table
				//-------------------------------------
				for _, idx := range db_tableIndexes {
					if idx.IsPrimary {
						continue
					}

					msgIndexNotUsed(idx)
					if idx.IsConstraint {
						sqls = append(sqls, al.DbBaser.ConstraintDropQuery(mi.schema, mi.table, idx.IndexName))
					} else {
						sqls = append(sqls, al.DbBaser.IndexDropQuery(mi.schema, idx.IndexName))
					}
					flagChangeStructure = true // флаг изменения структуры
				}

			}

			// таблица находится не в той схеме
			if ts != nil && ts.IsDifferentSchema() {
				ts.Restore()
				sqls = append(sqls, t.dbTableMove(ti, mi)...) // формируем команду переноса в нужную схему
			}

			ti.used = true // обработана
			t.alt[al] = append(t.alt[al], fullTableName)
			continue
		}

		flagChangeStructure = true // флаг изменения структуры
		// -------------------------------------------------------------------------------------------------------------
		// Table not exists - create
		// -------------------------------------------------------------------------------------------------------------
		msgTableNotFound(mi)
		sqls = append(sqls, mdTables[fullTableName])

		// sql comment for table
		if dbTbl_desc_enabled {
			if tableComment, ok := mdTableComments[fullTableName]; ok {
				sqls = append(sqls, tableComment.GetSQL())
			}
		}

		// sql comment for columns table
		if dbCol_desc_enabled {
			if tableColumnComments, ok := mdTableColumnComments[fullTableName]; ok {
				for _, tableColumnComment := range tableColumnComments { // Создание индексов
					sqls = append(sqls, tableColumnComment.GetSQL())
				}
			}
		}

		// sql create indexes of table
		if dbTbl_indx_enabled {
			if tableIndexes, ok := mdTableIndexes[fullTableName]; ok {
				for _, idx := range tableIndexes {
					// индексы UNIQUE входят в команду создания таблицы
					if !idx.IsUnique {
						sqls = append(sqls, idx.GetSQL()...)
					}
				}
			}
		}

		// sql add init data (only if table not syncronized from external server)
		if mi.model != nil && !utils.SliceContains(Options, mdNoInit) {
			cmd := getTableDataInit(mi.addrField)
			if len(cmd) > 0 {
				sqls = append(sqls, cmd)
			}
		}
		ti.used = true // обработана
		t.alt[al] = append(t.alt[al], fullTableName)
	}

	// если менялась структура
	if flagChangeStructure && !isReplica {
		// версия базы должна быть выше текущей, иначе предыдущая версия перезапишет данные, что приведет к краху данных
		t.panicIfShemaVersionNotUpdated(al)
	}

	// ----------------------------------------------------------------------
	// Проходимся по списку таблиц существующих в базе и ищем необработанные
	// ----------------------------------------------------------------------
	for _, ti := range db_Tables {
		// таблица уже обработатна
		if ti.used {
			continue
		}

		// не трогаем таблицы из схем репликации
		if t.repl != nil && t.repl == al && strings.HasPrefix(ti.schema, t.repl_prefix) {
			continue
		}

		// если схема неиспользуемой таблицы не соответствует схеме алиасу текущей базы
		if ti.schema != al.Server.Schema {
			// ищем зарегистрированный алиас с параметрами неиспользуемой таблицы
			server := DBServers.findServer(al.Server.Driver, al.Server.DbName, ti.schema)
			if server != nil {
				continue // значит эту таблицу игнорим, с ней разберется алиас-хозяин
			}
		}

		// проверим использование таблицы другими алиасами
		used := false
		fullTableName := ti.getFullTableName()
		for _, tables := range t.alt {
			if used = utils.SliceContains(tables, fullTableName); used {
				break
			}
		}
		if used {
			continue // значит эту таблицу игнорим, с ней разберется алиас-хозяин
		}

		// проверим использование таблицы в реляционной модели
		used = t.relations.findTable(ti, al.Server.server(), []typeLocation{locLocal})
		if used {
			continue // значит эту таблицу игнорим, с ней разберется алиас-хозяин
		}

		// if table not syncronized from external server - delete t table
		if utils.SliceContains(Options, mdDelUnused) {
			msgTableNotUsedDrop(ti)
			sqls = append(sqls, fmt.Sprintf("DROP TABLE IF EXISTS %s%s%s.%s%s%s;", Q, ti.schema, Q, Q, ti.table, Q))
			continue
		}

		upd_flag := false
		// если у таблица поддерживает коментарий и
		// если у таблицы есть коментарий и он не пустой - снимаем описание
		if dbTbl_desc_enabled {
			if tableComment, ok := db_TableComments[ti.getFullTableName()]; ok && len(tableComment) > 0 {
				// формируем пустой комментарий и присваиваем таблице
				sqls = append(sqls, dbCommentInfo{dbType: db_table_desc, schema: ti.schema, table: ti.table}.GetSQL())
				upd_flag = true
			}
		}

		// если у колонки таблицы поддерживают коментарии
		if dbCol_desc_enabled {
			// read description column of table
			c_desc, err := al.DbBaser.GetColumnsDescription(al.DB, ti.schema, ti.table) // список описний полей таблицы
			if err != nil {
				logger.Errorf("orm: %s.%s.%s: %v", al.Server.DbName, ti.schema, ti.table, err)
				break
			}

			// range columns
			for column, desc := range c_desc {
				// если у поля есть коментарий и он не пустой
				if len(desc) > 0 {
					// формируем пустой комментарий и присваиваем колонке таблицы
					sqls = append(sqls, dbCommentInfo{dbType: db_table_column_desc, schema: ti.schema, table: ti.table, column: column}.GetSQL())
					upd_flag = true
				}
			}
		}

		if upd_flag {
			msgTableNotUsed(ti)
		}
	}
	return
}

func (t *syncDb) dbDropUnusedModels() {
	//el := newElapser()	defer el.Show("dbDropUnusedModels()")

	// пройдемся по серверах системы
	// [DBServer][schema]*schemaInfo
	for server, shemasInfos := range t.dbs {

		if len(shemasInfos) == 0 {
			continue
		}

		var al *alias
		// ищем 1-st коннект к серверу
		for _, si := range shemasInfos {
			if !si.skip && si.used {
				al = si.al
				break
			}
		}

		if al == nil {
			logger.Errorf("orm: information of server %q %s:%s.%s not found", server.Alias, server.Driver, server.DbName, server.Schema)
			continue
		}

		if al.DriverType == DRVertica {
			continue
		}

		// получим неиспользуемые схемы
		schemas := t.getUnusedSchemas(server)
		if len(schemas) == 0 {
			continue
		}

		// список таблиц в неиспользуемых схемах
		db_tables, err := al.DbBaser.GetTables(al.DB, schemas...)
		if err != nil {
			panic(err)
		}

		if len(db_tables) == 0 {
			continue
		}

		dbTbl_desc_enabled := al.DbBaser.Support(db_table_desc)        // can use table desc for the database
		dbCol_desc_enabled := al.DbBaser.Support(db_table_column_desc) // can use column desc for the database

		// get table descriptions list in database
		db_tableComments := make(map[string]string)
		if dbTbl_desc_enabled {
			db_tableComments, err = al.DbBaser.GetTablesComment(al.DB, schemas...)
			if err != nil {
				panic(err)
			}
		}

		// ----------------------------------------------------------------------
		// Проходимся по списку таблиц существующих в базе и ищем необработанные
		// ----------------------------------------------------------------------
		sqls := []string{}
		for _, ti := range db_tables {
			upd_flag := false
			// если у таблица поддерживает коментарий и
			// если у таблицы есть коментарий и он не пустой - снимаем описание
			if dbTbl_desc_enabled && len(db_tableComments[ti.getFullTableName()]) > 0 {
				sqls = append(sqls, dbCommentInfo{dbType: db_table_desc, schema: ti.schema, table: ti.table}.GetSQL())
				upd_flag = true
			}

			// если у колонки таблицы поддерживают коментарии
			if dbCol_desc_enabled {
				// read description column of table
				c_desc, err := al.DbBaser.GetColumnsDescription(al.DB, ti.schema, ti.table) // список описний полей таблицы
				if err != nil {
					logger.Errorf("orm: %s.%s.%s: %v", al.Server.DbName, ti.schema, ti.table, err)
					break
				}

				// range columns
				for column, desc := range c_desc {
					// если у поля есть коментарий и он не пустой
					if len(desc) > 0 {
						sqls = append(sqls, dbCommentInfo{dbType: db_table_column_desc, schema: ti.schema, table: ti.table, column: column}.GetSQL())
						upd_flag = true
					}
				}
			}

			if upd_flag {

				logger.Warnf("orm: table %s:%s %s.%s.%q more not used CRM, will be mark as unused",
					al.Server.Driver, al.Server.Host, al.Server.DbName, ti.schema, ti.table)
				upd_flag = false
			}
		}

		// Check version of database
		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
			continue
		}
		// Exec commands
		t.ExecSQL(al, &sqls, fmt.Sprintf("updating unused objects of %s:%s.%s", al.Server.Driver, al.Server.DbName, al.Server.Schema))
	}
	return
}

func (t *syncDb) dbRunStartSQL() {
	//el := newElapser(); defer el.Show("dbRunStartSQL()")

	for _, al := range t.als {
		t.al = al

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}

		// получить START script модификации структуры БД из dbo файла
		cmds, err := t.dbo.find(al.Name, section_db_before_sync)
		if err != nil {
			//logger.Debugf("%v, skipping...", err)
			continue
		}

		if al.DbBaser.Support(db_script) {
			t.ExecSQL(al, &[]string{*cmds}, fmt.Sprintf("running init script for %s", al.info()))
		}
	}
	return
}

// sql add test data
func (t *syncDb) dbRunTestSQL() {

	//el := newElapser(); defer el.Show("dbRunTestSQL()")

	for _, al := range t.als {
		t.al = al

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}

		// --- sql add test data
		sqls := []string{}
		for _, mi := range modelCache.allOrdered(al, false) {
			// проверка данных таблицы ( если есть TableTestData() в структуре )
			cmd := getTableDataTest(mi.addrField)
			if len(cmd) > 0 {
				sqls = append(sqls, cmd)
			}
		}
		// Update structure
		t.ExecSQL(al, &sqls, fmt.Sprintf("running test script for %s", al.info()))
	}
	return
}

func (t *syncDb) dbRunFinishSQL() {
	//el := newElapser(); defer el.Show("dbRunFinishSQL()")

	for _, al := range t.als {
		t.al = al

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}

		// получить FINISH script модификации структуры БД из dbo файла
		cmds, err := t.dbo.find(al.Name, section_db_after_sync)
		if err != nil {
			//logger.Debugf("%v, skipping...", err)
			continue
		}

		if al.DbBaser.Support(db_script) {
			t.ExecSQL(al, &[]string{*cmds}, fmt.Sprintf("running finish script for %s", al.info()))
		}
	}
	return
}

func (t *syncDb) dbUpdateUserSearchPath() {
	//el := newElapser()	defer el.Show("dbUpdateUserSearchPath()")

	for _, al := range t.als {
		t.al = al

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}

		// Update user search-path
		sqls := []string{}

		public_path := al.DbBaser.GetSchemaPublic() // схема public
		db_schemas := t.usr[al].db                  // пути поиска в базе
		md_schemas := t.usr[al].md                  // пути поиска рассчитанные по моделям

		// удаляем схему public из путей, это авто схема
		if utils.SliceContains(md_schemas, public_path) {
			ts := []string{}
			for _, v := range md_schemas {
				if v != public_path {
					ts = append(ts, v)
				}
			}
			md_schemas = ts
		}

		// просматриваем рабочие схемы и сравниваем с теми что в базе
		for _, schema := range md_schemas {
			if !utils.SliceContains(db_schemas, schema) {
				// нужно обновить пути поиска в базе
				cmds := al.DbBaser.SetUserSearchPathQuery(al.Server.User, md_schemas...)
				sqls = append(sqls, cmds...)
				break
			}
		}

		// просматриваем схемы базы и сравниваем с рабочими
		for _, schema := range db_schemas {
			if schema == public_path {
				continue
			}
			switch al.DriverType {
			case DRPostgres:
				if strings.HasPrefix(strings.ToLower(schema), "pg_") {
					continue
				}
			case DRVertica:
				if strings.HasPrefix(strings.ToLower(schema), "v_") {
					continue
				}
			}

			if !utils.SliceContains(md_schemas, schema) {
				// нужно обновить пути поиска в базе
				cmds := al.DbBaser.SetUserSearchPathQuery(al.Server.User, md_schemas...)
				sqls = append(sqls, cmds...)
				break
			}
		}

		msg := fmt.Sprintf("updating %s, set path %s", al.info(), strings.Join(md_schemas, ","))
		t.ExecSQL(al, &sqls, msg)
	}
	return
}

func (t *syncDb) dbUpdateVersion() {

	//el := newElapser()	defer el.Show("dbUpdateVersion()")

	for _, al := range t.als {
		t.al = al

		// Check version of database
		if t.WrongShemaVersion(al, 0) {
			continue
		}

		si := t.getShemaInfo(al)
		// Finalize - SaveChanges version to Database
		if si.db_ver != si.dbo_ver {
			version := strconv.FormatFloat(si.dbo_ver, 'f', 2, 64)
			if err := al.DbBaser.SetShemaDescription(al.DB, al.Server.Schema, version); err != nil {
				panic(err)
			}
		}
	}
	return
}
