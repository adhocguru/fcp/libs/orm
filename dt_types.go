package orm

import (
	"bytes"
	sqlDriver "database/sql/driver"
	"fmt"
	"google.golang.org/protobuf/types/known/timestamppb"
	"regexp"
	"strings"
	"time"
)

const (
	FormatTime            = "15:04"
	FormatTimeFull        = "15:04:05"
	FormatDate            = "2006-01-02"
	FormatDateTime        = "2006-01-02 15:04:05"
	FormatDateTime3       = "2006-01-02 15:04:05.000"
	FormatDateTime6       = "2006-01-02 15:04:05.000000"
	FormatDateTimeZone    = "2006-01-02 15:04:05.000Z07:00"
	FormatDateTimeISOZone = "2006-01-02T15:04:05.000000Z07:00"
	FormatDateTimeISO1    = "2006-01-02T15:04:05-07:00"
	FormatDateTimeISO2    = "2006-01-02T15:04:05Z07:00"
	FormatDateTimeISO3    = "2006-01-02 15:04:05-07:00"
	FormatDateTimeISO4    = "2006-01-02 15:04:05Z07:00"
	FormatJsonDate        = "\"2006-01-02\""
	FormatJsonTime        = "\"15:04:05\""
	FormatJsonDateTime    = "\"2006-01-02 15:04:05\""
	FormatJsonISO         = "\"2006-01-02T15:04:05.000Z07:00\""
	FormatJsonISOZone     = "\"2006-01-02T15:04:05.000000Z07:00\""
)

var TimestampRegexp = regexp.MustCompile("([\"]{0,1}[0-9]{4}-[0-9]{2}-[0-9]{2}([T\\s]{1})[0-9]{2}:[0-9]{2}:[0-9]{2}[.]{0,1})([0-9]{1,12}){0,1}(([+Z]{1})([0-9]{2}[:]{1}[0-9]{2}){0,1}[\"]{0,1})")

// TDate
type TDate struct {
	time.Time
}

func (t *TDate) FieldType() int {
	return TypeDateField
}
func (t *TDate) RawValue() interface{} {
	if t.IsZero() {
		return nil
	}
	return t.Format(FormatDate)
}
func (t *TDate) Set(d time.Time) {
	*t = TDate{d}
}
func (t *TDate) SetRaw(value interface{}) error {
	switch d := value.(type) {
	case time.Time:
		t.Set(d)
	case string:
		if strings.Compare(string(d), "null") == 0 {
			t.Set(time.Time{})
			return nil
		}

		v, err := time.Parse(FormatDate, string(d))
		if err == nil {
			t.Set(v)
		}
		return err
	case nil:
		t.Set(time.Time{})
	default:
		return fmt.Errorf("<DateField.SetRaw> unknown value `%s`", value)
	}
	return nil
}
func (t *TDate) String() string {
	return t.ValueTime().Format(FormatDate) // String()
}
func (t TDate) ValueTime() time.Time {
	return time.Time(t.Time)
}

// Value implements the driver Valuer interface.
func (t TDate) Value() (val sqlDriver.Value, err error) {
	return []byte(t.String()), nil
}

// MarshalJSON implements the json.Marshaler interface.
func (t TDate) MarshalJSON() ([]byte, error) {
	if t.IsZero() {
		return []byte("null"), nil
	}
	return []byte(t.Format(FormatJsonDate)), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (t *TDate) UnmarshalJSON(data []byte) (err error) {
	if strings.Compare(string(data), "null") == 0 {
		*t = TDate{}
		return
	}
	data = bytes.Trim(data, "\"")

	tt, err := time.Parse(FormatDate, string(data))
	if err != nil {
		tt, err = time.Parse(FormatDateTime, string(data))
	}

	*t = TDate{tt}
	return
}

func (t *TDate) ProtoTimestamp() *timestamppb.Timestamp {
	return timestamppb.New(t.Time)
}

// TTime
type TTime struct {
	time.Time
}

func (t *TTime) FieldType() int {
	return TypeTimeField
}

func (t *TTime) RawValue() interface{} {
	if t.IsZero() {
		return nil
	}
	return t.Format(FormatTimeFull)
}
func (t *TTime) Set(d time.Time) {
	*t = TTime{d}
}
func (t *TTime) SetRaw(value interface{}) error {
	switch val := value.(type) {
	case time.Time:
		t.Set(val)
	case string:
		if strings.Compare(string(val), "null") == 0 {
			t.Set(time.Time{})
			return nil
		}

		v, err := time.Parse(FormatTimeFull, string(val))
		if err != nil {
			v, err = time.Parse(FormatTime, string(val))
		}

		if err == nil {
			t.Set(v)
		}
		return err
	case nil:
		t.Set(time.Time{})
	default:
		return fmt.Errorf("<TimeField.SetRaw> unknown value `%s`", value)
	}
	return nil
}
func (t *TTime) String() string {
	return t.ValueTime().String()
}
func (t TTime) ValueTime() time.Time {
	return time.Time(t.Time)
}

// Value implements the driver Valuer interface.
func (t TTime) Value() (val sqlDriver.Value, err error) {
	return []byte(t.String()), nil
}

// MarshalJSON implements the json.Marshaler interface.
func (t TTime) MarshalJSON() ([]byte, error) {
	if t.IsZero() {
		return []byte("null"), nil
	}
	return []byte(t.Format(FormatJsonTime)), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (t *TTime) UnmarshalJSON(data []byte) (err error) {
	if strings.Compare(string(data), "null") == 0 {
		*t = TTime{}
		return
	}
	data = bytes.Trim(data, "\"")

	tt, err := time.Parse(FormatTimeFull, string(data))
	if err != nil {
		tt, err = time.Parse(FormatTime, string(data))
	}

	*t = TTime{tt}
	return
}

func (t *TTime) ProtoTimestamp() *timestamppb.Timestamp {
	return timestamppb.New(t.Time)
}

// TDateTime
type TDateTime struct {
	time.Time `json:"-"`
}

func (t *TDateTime) FieldType() int {
	return TypeDateTimeField
}

func (t *TDateTime) RawValue() interface{} {
	if t.IsZero() {
		return nil
	}
	return t.Format(FormatDateTimeISOZone)
}

func (t *TDateTime) Set(d time.Time) {
	*t = TDateTime{d}
}
func (t *TDateTime) SetRaw(value interface{}) error {
	switch d := value.(type) {
	case int64:
		t.Set(time.Unix(d, 0))
	case time.Time:
		t.Set(d)
	case string:
		if strings.Compare(string(d), "null") == 0 {
			t.Set(time.Time{})
			return nil
		}

		v, err := time.Parse(FormatDateTimeZone, string(d))
		if err != nil {
			v, err = time.Parse(FormatDateTime, string(d))
			if err != nil {
				v, err = time.Parse(FormatDate, string(d))
			}
		}
		if err == nil {
			t.Set(v)
		}
		return err
	case nil:
		t.Set(time.Time{})
	default:
		return fmt.Errorf("<DateTimeField.SetRaw> unknown value `%s`", value)
	}
	return nil
}
func (t *TDateTime) String() string {
	return t.Format(FormatJsonISO)
}

func (t *TDateTime) ValueTime() time.Time {
	return time.Time(t.Time)
}

// Value implements the driver Valuer interface.
func (t *TDateTime) Value() (val sqlDriver.Value, err error) {
	return []byte(t.String()), nil
}

// MarshalJSON implements the json.Marshaler interface.
func (t *TDateTime) MarshalJSON() ([]byte, error) {
	if t.IsZero() {
		return []byte("null"), nil
	}
	return []byte(t.Format(FormatJsonISO)), nil
}

// UnmarshalJSON implements the json.Unmarshaler interface.
// The datetime is expected to be a quoted string in RFC 3339 fmt.
func (t *TDateTime) UnmarshalJSON(data []byte) (err error) {
	if strings.Compare(string(data), "null") == 0 {
		*t = TDateTime{}
		return
	}

	dataStr := ReBuildTime(string(bytes.Trim(data, "\"")))
	//dataStr := ReBuildTime(string(data))

	tt, err := time.Parse(FormatDateTimeISOZone, dataStr)

	if err != nil {
		tt, err = time.Parse(FormatDateTime, dataStr)
	}

	if err != nil {
		tt, err = time.Parse(FormatDate, dataStr)
	}

	*t = TDateTime{tt}
	return
}

func (t TDateTime) ToProtoTimestamp() *timestamppb.Timestamp {
	return timestamppb.New(t.Time)
}

func (t TDateTime) FromProtoTimestamp(ts *timestamppb.Timestamp) TDateTime {
	t.Set(ts.AsTime())
	return t
}

func ReBuildTime(data string) string {
	submatchs := TimestampRegexp.FindStringSubmatch(string(data))
	if len(submatchs) != 7 {
		return data
	}
	replace := ""
	if len(submatchs[3]) > 0 && len(submatchs[3]) < 6 {
		replace = submatchs[3] + strings.Repeat("0", 6-len(submatchs[3]))
	} else if len(submatchs[3]) > 6 {
		replace = string([]rune(submatchs[3])[0:6])
	} else if len(submatchs[3]) > 0 {
		replace = submatchs[3]
	} else {
		replace = "." + strings.Repeat("0", 6)
	}
	if submatchs[2] != "T" {
		submatchs[1] = strings.ReplaceAll(submatchs[1], " ", "T")
	}
	if len(submatchs[6]) == 0 {
		//submatchs[4] = "Z00:00"
		submatchs[4] = "Z"
	}
	//} else if !strings.HasPrefix(submatchs[5], "Z") {
	//	submatchs[4] = "Z" + string([]rune(submatchs[4])[1:len(submatchs[4])])
	//}

	return submatchs[1] + replace + submatchs[4]
}
