// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package orm

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"
)

//customer log func
var LogFunc func(logMap map[string]interface{})

func debugLogQueies(o *orm, alias *alias, operation, query string, t time.Time, err error, args ...interface{}) {
	var logMap = make(map[string]interface{})
	//sub := time.Now().Sub(t) / 1e5
	//duration := float64(int(sub)) / 10.0
	duration := fmt.Sprintf("%.6fs", time.Since(t).Seconds())

	flag := "OK"
	if err != nil {
		flag = "FAIL"
	}

	var cmd string
	if len(args) == 0 || !strings.Contains(query, "$1") {
		//cmd = fmt.Sprintf("[%s] [%.1fms] %s", flag, duration, query)
		cmd = query
		cons := make([]string, 0, len(args))
		for _, arg := range args {
			cons = append(cons, toSQLValue(arg))
		}

		if len(cons) > 0 {
			cmd += fmt.Sprintf(" - `%s`", strings.Join(cons, "`, `"))
		}

	} else {
		for i, arg := range args {
			param := fmt.Sprintf("$%d", i+1)
			value := toSQLValue(arg)
			query = strings.Replace(query, param, value, 1)
		}
		cmd = query
	}
	if err != nil {
		cmd += " : " + err.Error()
	}
	logMap["flag"] = flag
	logMap["error"] = err
	logMap["duration"] = duration
	logMap["db_alias"] = alias.Name
	logMap["db_name"] = alias.Server.DbName
	logMap["operation"] = operation
	logMap["sql"] = cmd

	//cmd = fmt.Sprintf("[%s] [%.1fms] %s", flag, duration, cmd)
	cmd = fmt.Sprintf("[%s] [%s] %s", flag, duration, cmd)

	if o != nil {
		o.result.err = err
		o.result.stmt = cmd
		o.result.operation = operation
	}

	if !o.debug {
		return
	}

	if LogFunc != nil {
		LogFunc(logMap)
	} else {
		logger.Debugf(cmd)
	}
}

// statement query logger struct.
// if dev mode, use stmtQueryLog, or use stmtQuerier.
type stmtQueryLog struct {
	alias *alias
	query string
	stmt  stmtQuerier
	o     *orm
}

var _ stmtQuerier = new(stmtQueryLog)

func (d *stmtQueryLog) Close() error {
	a := time.Now()
	err := d.stmt.Close()
	debugLogQueies(d.o, d.alias, "st.Close", d.query, a, err)

	return err
}

func (d *stmtQueryLog) Exec(args ...interface{}) (sql.Result, error) {
	a := time.Now()
	res, err := d.stmt.Exec(args...)
	debugLogQueies(d.o, d.alias, "st.Exec", d.query, a, err, args...)

	return res, err
}

func (d *stmtQueryLog) Query(args ...interface{}) (*sql.Rows, error) {
	a := time.Now()
	res, err := d.stmt.Query(args...)
	debugLogQueies(d.o, d.alias, "st.Query", d.query, a, err, args...)

	return res, err
}

func (d *stmtQueryLog) QueryRow(args ...interface{}) *sql.Row {
	a := time.Now()
	res := d.stmt.QueryRow(args...)
	debugLogQueies(d.o, d.alias, "st.QueryRow", d.query, a, nil, args...)

	return res
}

func newStmtQueryLog(alias *alias, stmt stmtQuerier, query string, o *orm) stmtQuerier {
	d := new(stmtQueryLog)
	d.stmt = stmt
	d.alias = alias
	d.query = query
	d.o = o

	return d
}

// database query logger struct.
// if dev mode, use dbQueryLog, or use dbQuerier.
type dbQueryLog struct {
	alias *alias
	o     *orm
	db    dbQuerier
	tx    txer
	txe   txEnder
	//log   log.logger
}

var _ dbQuerier = new(dbQueryLog)
var _ txer = new(dbQueryLog)
var _ txEnder = new(dbQueryLog)

func (d *dbQueryLog) Prepare(query string) (*sql.Stmt, error) {
	a := time.Now()
	stmt, err := d.db.Prepare(query)
	debugLogQueies(d.o, d.alias, "db.Prepare", query, a, err)
	return stmt, err
}

func (d *dbQueryLog) PrepareContext(ctx context.Context, query string) (*sql.Stmt, error) {
	a := time.Now()
	stmt, err := d.db.PrepareContext(ctx, query)
	debugLogQueies(d.o, d.alias, "db.Prepare", query, a, err)

	return stmt, err
}

func (d *dbQueryLog) Exec(query string, args ...interface{}) (sql.Result, error) {
	a := time.Now()
	res, err := d.db.Exec(query, args...)
	debugLogQueies(d.o, d.alias, "db.Exec", query, a, err, args...)

	return res, err
}

func (d *dbQueryLog) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	a := time.Now()
	res, err := d.db.ExecContext(ctx, query, args...)
	debugLogQueies(d.o, d.alias, "db.Exec", query, a, err, args...)

	return res, err
}

func (d *dbQueryLog) Query(query string, args ...interface{}) (*sql.Rows, error) {
	a := time.Now()
	res, err := d.db.Query(query, args...)
	debugLogQueies(d.o, d.alias, "db.Query", query, a, err, args...)

	return res, err
}

func (d *dbQueryLog) QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	a := time.Now()
	res, err := d.db.QueryContext(ctx, query, args...)
	debugLogQueies(d.o, d.alias, "db.Query", query, a, err, args...)

	return res, err
}

func (d *dbQueryLog) QueryRow(query string, args ...interface{}) *sql.Row {
	a := time.Now()
	res := d.db.QueryRow(query, args...)
	debugLogQueies(d.o, d.alias, "db.QueryRow", query, a, nil, args...)

	return res
}

func (d *dbQueryLog) QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row {
	a := time.Now()
	res := d.db.QueryRowContext(ctx, query, args...)
	debugLogQueies(d.o, d.alias, "db.QueryRow", query, a, nil, args...)

	return res
}

func (d *dbQueryLog) Begin() (*sql.Tx, error) {
	a := time.Now()
	tx, err := d.db.(txer).Begin()
	debugLogQueies(d.o, d.alias, "db.Begin", "START TRANSACTION", a, err)

	return tx, err
}

func (d *dbQueryLog) BeginTx(ctx context.Context, opts *sql.TxOptions) (*sql.Tx, error) {
	a := time.Now()
	tx, err := d.db.(txer).BeginTx(ctx, opts)
	debugLogQueies(d.o, d.alias, "db.BeginTx", "START TRANSACTION", a, err)

	return tx, err
}

func (d *dbQueryLog) Commit() error {
	a := time.Now()
	err := d.db.(txEnder).Commit()
	debugLogQueies(d.o, d.alias, "tx.Commit", "COMMIT", a, err)

	return err
}

func (d *dbQueryLog) Rollback() error {
	a := time.Now()
	err := d.db.(txEnder).Rollback()
	debugLogQueies(d.o, d.alias, "tx.Rollback", "ROLLBACK", a, err)

	return err
}

func (d *dbQueryLog) SetDB(db dbQuerier) {
	d.db = db
}

func newDbQueryLog(alias *alias, db dbQuerier, o *orm) dbQuerier {
	d := new(dbQueryLog)
	d.alias = alias
	d.db = db
	d.o = o

	return d
}
