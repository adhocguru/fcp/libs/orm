package orm

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"io/ioutil"
	"os"
	"reflect"
	"strings"
)

const column_bm = "bm"

// vertica operators.
var verticaOperators = map[string]string{
	"exact":       "= ?",
	"iexact":      "= UPPER(?)",
	"contains":    "LIKE ?",
	"icontains":   "LIKE UPPER(?)",
	"gt":          "> ?",
	"gte":         ">= ?",
	"lt":          "< ?",
	"lte":         "<= ?",
	"eq":          "= ?",
	"ne":          "!= ?",
	"startswith":  "LIKE ?",
	"endswith":    "LIKE ?",
	"istartswith": "LIKE UPPER(?)",
	"iendswith":   "LIKE UPPER(?)",
}

// vertica column field types.
var verticaTypes = map[string]string{
	"auto":            "IDENTITY(100) NOT NULL PRIMARY KEY ENABLED",
	"pk":              "NOT NULL PRIMARY KEY ENABLED",
	"bool":            "boolean",
	"string":          "varchar(%d)",
	"string-text":     "long varchar",
	"time.Time-clock": "time", // RUM
	"time.Time-date":  "date",
	"time.Time":       "timestamp with time zone",
	"int8":            `smallint CHECK("%COL%" >= -127 AND "%COL%" <= 128)`,
	"int16":           "smallint",
	"int32":           "integer",
	"int64":           "bigint",
	"uint8":           `smallint CHECK("%COL%" >= 0 AND "%COL%" <= 255)`,
	"uint16":          `integer CHECK("%COL%" >= 0)`,
	"uint32":          `bigint CHECK("%COL%" >= 0)`,
	"uint64":          `bigint CHECK("%COL%" >= 0)`,
	"float64":         "double precision",
	"float64-decimal": "numeric(%d, %d)",
}

// vertica dbBaser.
type dbBaseVertica struct {
	dbBase
}

// create new vertica dbBaser.
func newdbBaseVertica() dbBaser {
	b := new(dbBaseVertica)
	b.ins = b
	return b
}

var _ dbBaser = new(dbBaseVertica)

// get vertica operator.
func (d *dbBaseVertica) OperatorSQL(operator string) string {
	return verticaOperators[operator]
}

// get column types of vertica.
func (d *dbBaseVertica) DbTypes() map[string]string {
	return verticaTypes
}

const end_line = " >·<"

func (d *dbBaseVertica) InsertValue(q dbQuerier, mi *modelInfo, isMulti bool,
	names []string, autoFields []string, values []interface{}, ignoreConflict bool) (id interface{}, err error) {
	if len(values) == 0 {
		return
	}

	Q := d.ins.TableQuote()

	narg := len(names)         // count of arg in block(columns)
	blks := len(values) / narg // count blocks(records)

	// ---------------------------------------------------------------------------
	// COPY FROM LOCAL method
	// ---------------------------------------------------------------------------
	if isMulti && blks > 1 {
		columns := strings.Join(names, ",")

		sels := make([]string, blks) // list of select
		for i := 0; i < blks; i++ {
			vals := make([]string, 0, narg)
			for j := i * narg; j < i*narg+narg; j++ {
				var v string
				arg := values[j]
				val := reflect.ValueOf(arg)
				kind := val.Kind()
				if kind == reflect.Ptr {
					val = val.Elem()
					kind = val.Kind()
					arg = val.Interface()
				}

				fiIdx := j - i*narg
				column := names[fiIdx]
				var fi *fieldInfo
				if fi, _ = mi.fields.GetByAny(column); fi != nil {
					column = fi.column
				}

				switch fi.fieldType {
				case TypeTimeField, TypeDateField, TypeDateTimeField:
					//v = fmt.Sprintf("cast( '%v' as timestamp)", arg)
					v = fmt.Sprintf(`%v`, arg)
				default:
					{
						v = fmt.Sprintf(`%v`, arg)
						if v == "<nil>" {
							v = ""
						}
						switch kind {
						case reflect.String:
							v = strings.Replace(v, `~`, `\~`, -1)
							v = fmt.Sprintf(`~%v~`, v)
						}
					}
				}
				vals = append(vals, v)
			}
			sels[i] = strings.Join(vals, ",")
		}

		// for use for speedily process errors
		for {
			file, err := ioutil.TempFile(os.TempDir(), fmt.Sprintf("copy_%s_", mi.table))
			if err != nil {
				logger.Errorf("%v", err)
				break
			}
			fileName := file.Name()
			defer func() {
				if file != nil {
					file.Close()
				}
				os.Remove(fileName)
			}()

			dtsz := int64(len(sels)) // count inserted records
			data := strings.Join(sels, end_line)
			sels = sels[:0] // file data + clear memory

			if _, err = file.WriteString(data); err != nil {
				logger.Errorf("orm: can not write data to file %s, error: %v", file.Name(), err)
				break
			}
			file.Close()

			// for test - add command in 1-st line
			//query := fmt.Sprintf(`COPY %s.%s(%s) FROM LOCAL '%s' STREAM NAME 'copy_%s' DELIMITER ',' ESCAPE '\' ENCLOSED BY '~' SKIP 1 ABORT ON ERROR;`, mi.schema, mi.table, columns, file.Name(), mi.table)
			//if _, err := file.WriteString(query + "\n"); err != nil {
			//}

			//query := fmt.Sprintf(`COPY %s.%s(%s) FROM LOCAL '%s' STREAM NAME 'copy_%s' DELIMITER ',' ESCAPE '\' ENCLOSED BY '~' ABORT ON ERROR;`, mi.schema, mi.table, columns, file.Name(), mi.table)
			query := fmt.Sprintf(`COPY %s%s%s(%s) FROM LOCAL '%s' STREAM NAME 'copy_%s' DELIMITER ',' RECORD TERMINATOR '%s' ESCAPE '\' ENCLOSED BY '~' ABORT ON ERROR;`,
				Q, mi.table, Q, columns, fileName, mi.table, end_line)

			if _, err = q.Exec(query); err != nil {
				logger.Warnf("orm: %v [%v] [%v], trying Multiselect method....", err, query, data)
				break
			}
			return dtsz, nil
		}
	}

	// ---------------------------------------------------------------------------
	// Multiselect method
	// ---------------------------------------------------------------------------
	err = nil
	if isMulti {
		sep := fmt.Sprintf("%s, %s", Q, Q)
		columns := strings.Join(names, sep)

		sels := make([]string, blks) // list of select
		for i := 0; i < blks; i++ {
			vals := make([]string, 0, narg)
			for j := i * narg; j < i*narg+narg; j++ {
				var v string
				arg := values[j]
				val := reflect.ValueOf(arg)
				kind := val.Kind()
				if kind == reflect.Ptr {
					val = val.Elem()
					kind = val.Kind()
					arg = val.Interface()
				}

				fi_idx := j - i*narg
				column := names[fi_idx]
				var fi *fieldInfo
				if fi, _ = mi.fields.GetByAny(column); fi != nil {
					column = fi.column
				}

				switch fi.fieldType {
				case TypeTimeField, TypeDateField, TypeDateTimeField:
					v = fmt.Sprintf("%v", arg)
					if v == "<nil>" {
						v = "NULL::timestamp"
					} else {
						v = fmt.Sprintf("'%v'::timestamp", v)
					}
				default:
					{
						v = fmt.Sprintf("%v", arg)
						if v == "<nil>" {
							v = "NULL"
						}
						switch kind {
						case reflect.String:
							v = strings.Replace(v, "'", "''", -1)
							v = fmt.Sprintf("'%v'", v)
							if fi.size > 0 {
								v = fmt.Sprintf("%v::varchar(%v)", v, fi.size)
							}
						}
					}
				}
				vals = append(vals, v)
			}
			sels[i] = "SELECT " + strings.Join(vals, ",")
		}
		data := strings.Join(sels, " union all\n")
		sels = sels[:0] // query data + clear memory

		query := fmt.Sprintf("INSERT /*+AUTO*/ INTO %s%s%s (%s%s%s)\n%s", Q, mi.table, Q, Q, columns, Q, data)
		//d.ins.ReplaceMarks(&query)
		res, err := q.Exec(query)
		if err != nil {
			return 0, fmt.Errorf("%v [%s]", err, query)
		}
		return res.RowsAffected()
	}

	// ---------------------------------------------------------------------------
	// -----------------------Insert one record method ---------------------------
	// ---------------------------------------------------------------------------

	/*
		// Generate Id for new record( store to field "bm" )
		id, err = d.getIdSequenceNextValue(q, mi, mi.fields.pk)
		if err != nil {
			return 0, err
		}

		var bmExists bool
		for idx, name := range names {	// finfd pk field between inserting names
			if name == column_bm {
				values[idx] = id
				bmExists = true
				break					// pk field present - all ok
			}
		}

		// field bm not present between inserted fields
		if !bmExists {
			names, values = append(names, column_bm), append(values, id)
		}
	*/

	// наличие поля "bm" свидетельствует об наличии sequence для id - фича vertica
	var bmExists bool
	if _, bmExists = mi.fields.columns[column_bm]; bmExists {

		// удаляем первичный ключ/значение из списка если есть
		i := utils.SliceIndex(names, mi.fields.pk.column)
		if i >= 0 {
			utils.SliceDeleteIndex(&names, i)
			utils.SliceDeleteIndex(&values, i)
		}

		// Generate Id for new record( store to column "bm" )
		id, err = d.getIdSequenceNextValue(q, mi, mi.fields.pk)
		if err != nil {
			return 0, err
		}

		// проверяем наличие поля "bm" в списке
		// и присваем ему вычисленный sequence
		i = utils.SliceIndex(names, column_bm)
		if i == -1 {
			names, values = append(names, column_bm), append(values, id)
		} else {
			values[i] = id
		}
	} else {
		// стандартная вставка со всеми полями
		// init return value
		i := utils.SliceIndex(names, mi.fields.pk.column)
		if i >= 0 {
			id = values[i].(int64)
		}
	}

	marks := make([]string, len(names))
	for i := range marks {
		marks[i] = "?"
	}

	sep := fmt.Sprintf("%s, %s", Q, Q)
	qmarks := strings.Join(marks, ", ")
	columns := strings.Join(names, sep)

	query := fmt.Sprintf("INSERT /*+AUTO*/ INTO %s%s%s (%s%s%s) VALUES (%s)", Q, mi.table, Q, Q, columns, Q, qmarks)

	d.ins.ReplaceMarks(&query)

	_, err = q.Exec(query, values...)
	if err != nil {
		return 0, fmt.Errorf("%v [%s]", err, query)
	}

	if bmExists {
		query = fmt.Sprintf("SELECT id from %s%s%s WHERE bm = %d;", Q, mi.table, Q, id)
		row := q.QueryRow(query)
		err = row.Scan(&id)
		return id, err
	}
	return
}

// get next auto key
func (d *dbBaseVertica) getIdSequenceNextValue(db dbQuerier, mi *modelInfo, fi *fieldInfo) (int64, error) {
	query := fmt.Sprintf("SELECT NEXTVAL('%s_%s_seq');", mi.table, fi.column)
	row := db.QueryRow(query)
	var id int64
	err := row.Scan(&id)
	return id, err
}

// vertica unsupports updating joined record.
func (d *dbBaseVertica) SupportUpdateJoin() bool {
	return false
}

func (d *dbBaseVertica) MaxLimit() uint64 {
	return 0
}

// vertica quote is ".
func (d *dbBaseVertica) TableQuote() string {
	return `"`
}

// Support db ddl
func (d *dbBaseVertica) Support(dbe dbElement) bool {
	switch dbe {
	case db_schema, db_schema_desc:
		return true
	case db_table, db_table_desc, db_table_constraint:
		return true
	}
	return false
}

func (d *dbBaseVertica) GetIFExistsSQL() string {
	return "IF EXISTS"
}

func (d *dbBaseVertica) GetIFNotExistsSQL() string {
	return "IF NOT EXISTS"
}

func (d *dbBaseVertica) GetEnabledSQL() string {
	return "ENABLED"
}

// get true if user is a superuser
func (d *dbBaseVertica) UserExists(db dbQuerier, user string) (ok bool, err error) {
	query := fmt.Sprintf(`SELECT count(*) 
								   FROM v_catalog.USERS u
								  WHERE u.user_name = '%s';`, user)

	rows, err := db.Query(query)
	if err != nil {
		err = errors.New("[" + query + "] " + err.Error())
		return
	}

	defer rows.Close()

	var cnt int
	if rows.Next() {
		if err = rows.Scan(&cnt); err != nil {
			return
		}
	}
	ok = cnt != 0
	return
}

// get true if user is a superuser
func (d *dbBaseVertica) UserIsSuperuser(db dbQuerier, user string) (ok bool, err error) {

	query := fmt.Sprintf(`SELECT has_role('pseudosuperuser');`) // for current user connection

	rows, err := db.Query(query)
	if err != nil {
		err = errors.New("[" + query + "] " + err.Error())
		return
	}

	defer rows.Close()

	if rows.Next() {
		err = rows.Scan(&ok)
	}
	return
}

func (d *dbBaseVertica) SetShemaGrantQuery(schema, user string) []string {
	queries := []string{}
	queries = append(queries, fmt.Sprintf("GRANT ALL ON SCHEMA %s to %s,PUBLIC;", schema, user))
	queries = append(queries, fmt.Sprintf("GRANT ALL ON ALL TABLES IN SCHEMA %s TO %s,PUBLIC;", schema, user))
	return queries
}

func (d *dbBaseVertica) SetUserSearchPathQuery(user string, schemas ...string) []string {
	queries := []string{}
	if len(schemas) == 0 {
		return queries
	}

	//for _, schema := range schemas {
	//	queries = append(queries, d.ShemaCreateQuery(schema))
	//	queries = append(queries, fmt.Sprintf("GRANT ALL ON SCHEMA %s to %s,PUBLIC;", schema, user))
	//	queries = append(queries, fmt.Sprintf("GRANT ALL ON ALL TABLES IN SCHEMA %s TO %s,PUBLIC;", schema, user))
	//}
	queries = append(queries, fmt.Sprintf("ALTER USER %s SEARCH_PATH %s,public;", user, strings.Join(schemas, ",")))
	queries = append(queries, fmt.Sprintf("SET SEARCH_PATH TO %s,public;", strings.Join(schemas, ",")))
	return queries
}

// show shemas sql for vertica
func (d *dbBaseVertica) ShowSchemasQuery() string {
	return `SELECT DISTINCT schema_name FROM v_catalog.schemata`
}

func (d *dbBaseVertica) GetShemasInfo(db dbQuerier) ([]*schemaInfo, error) {
	sis := []*schemaInfo{}

	query :=
		`SELECT current_database as dbname
       , s.schema_name    as schema
       , c.comment        as desc
    FROM v_catalog.schemata s
         LEFT JOIN v_catalog.comments c ON c.object_name = schema_name 
                                       AND c.object_type = 'SCHEMA'
   WHERE s.schema_name NOT LIKE 'v_%'`

	rows, err := db.Query(query)
	if err != nil {
		return sis, fmt.Errorf("%v [%s]", err, query)
	}
	defer rows.Close()

	for rows.Next() {
		si := &schemaInfo{}
		var desc sql.NullString
		err := rows.Scan(&si.dbname, &si.schema, &desc)
		if err != nil {
			panic(err.Error())
		}

		if desc.Valid {
			si.desc = desc.String
		}

		sis = append(sis, si)
	}
	return sis, nil
}

func (d *dbBaseVertica) SchemaExists(db dbQuerier, schema string) (bool, error) {
	query := fmt.Sprintf(`SELECT count(*) 
								   FROM v_catalog.schemata 
								  WHERE schema_name = '%s'`, schema)
	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err == sql.ErrNoRows {
		err = nil
	}
	return cnt > 0, err
}

// get schema description
func (d *dbBaseVertica) GetShemaDescription(db dbQuerier, schema string) (desc string, err error) {
	query := fmt.Sprintf(`SELECT comment FROM v_catalog.comments WHERE object_type = 'SCHEMA' AND object_name = '%s';`, schema)

	rows, err := db.Query(query)
	if err != nil {
		err = errors.New("[" + query + "] " + err.Error())
		return
	}
	defer rows.Close()

	var val sql.NullString
	if rows.Next() {
		if err = rows.Scan(&val); err != nil {
			return
		}
		desc = val.String
	}
	return
}

func (d *dbBaseVertica) SetShemaDescription(db dbQuerier, schema string, version string) error {
	query := fmt.Sprintf("COMMENT ON SCHEMA %s IS '%s';", schema, version)
	_, err := db.Exec(query)
	if err != nil {
		err = errors.New("[" + query + "] " + err.Error())
		return err
	}
	return nil
}

func (d *dbBaseVertica) GetUserSearchPath(db dbQuerier, user string) (string, error) {

	query := fmt.Sprintf(`SELECT search_path FROM v_catalog.USERS WHERE user_name = '%s';`, user)

	row := db.QueryRow(query)
	var res sql.NullString
	err := row.Scan(&res)
	if err == sql.ErrNoRows {
		err = nil
	}
	return res.String, err
}

// show shema sql for vertica.
func (d *dbBaseVertica) ShemaCreateQuery(schema string) string {
	return fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS %s;", schema)
}

// show table sql for vertica.
func (d *dbBaseVertica) ShowTablesQuery(schemas ...string) string {
	query := `SELECT table_schema, table_name FROM v_catalog.tables`
	if len(schemas) > 0 {
		query += fmt.Sprintf(` WHERE table_schema IN('%s')`, strings.Join(schemas, "','"))
	}
	return query
}

// get all tables comment.
func (d *dbBaseVertica) GetTablesComment(db dbQuerier, schemas ...string) (map[string]string, error) {
	comments := make(map[string]string)

	query := `SELECT object_schema, object_name, comment
			    FROM v_catalog.comments
			   WHERE object_type = 'TABLE'`

	if len(schemas) > 0 {
		query += fmt.Sprintf(` AND object_schema IN('%s')`, strings.Join(schemas, "','"))
	}

	rows, err := db.Query(query)
	if err != nil {
		return comments, errors.New("[" + query + "] " + err.Error())
	}
	defer rows.Close()

	for rows.Next() {
		var schema string
		var table string
		var desc sql.NullString
		err := rows.Scan(&schema, &table, &desc)
		if err != nil {
			return comments, err
		}
		comments[fmt.Sprintf("%s.%s", schema, table)] = desc.String
	}
	return comments, nil
}

func (d *dbBaseVertica) TableExists(db dbQuerier, schema string, table string) (bool, error) {
	query := fmt.Sprintf(`SELECT object_name, comment
							   	   FROM v_catalog.comments
								  WHERE object_type = 'TABLE'
								 AND object_name = %s`, table)
	if schema != "" {
		query += fmt.Sprintf(` AND object_schema = '%s'`, schema)
	}

	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err == sql.ErrNoRows {
		err = nil
	}
	return cnt > 0, err
}

// show table columns sql for vertica.
func (d *dbBaseVertica) ShowColumnsQuery(schema string, table string) string {
	return fmt.Sprintf("SELECT column_name, data_type, is_nullable from v_catalog.columns where table_name = '%s'", table)
}

// check index exist in vertica.
func (d *dbBaseVertica) IndexExists(db dbQuerier, schema string, table string, name string) (bool, error) {
	return false, nil
}

func (d *dbBaseVertica) IndexDropQuery(schema string, index string) string {
	return ""
}

// get all tables Indexes.
func (d *dbBaseVertica) GetTablesIndexes(db dbQuerier, schema ...string) (map[string]idxs, error) {
	indexes := make(map[string]idxs)
	// TODO Constraint
	return indexes, nil
}

func (d *dbBaseVertica) ConstraintExistQuery(schema string, table string, constraint string) string {
	return fmt.Sprintf(`SELECT 1 
                                 FROM v_catalog.constraint_columns 
                                WHERE table_schema = '%s' 
                                  AND table_name = '%s' 
                                  AND constraint_name = '%s'`, schema, table, constraint)
}

func (d *dbBaseVertica) ConstraintExist(db dbQuerier, schema string, table string, constraint string) (bool, error) {
	query := fmt.Sprintf(`SELECT COUNT(*) FROM ( %s ) d`, d.ConstraintExistQuery(schema, table, constraint))

	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err == sql.ErrNoRows {
		err = nil
	}
	return cnt > 0, err
}

func (d *dbBaseVertica) ConstraintDropQuery(schema string, table string, constraint string) string {
	return fmt.Sprintf(`ALTER TABLE "%s"."%s" DROP CONSTRAINT "%s";`, schema, table, constraint)
}
