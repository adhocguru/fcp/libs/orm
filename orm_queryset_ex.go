package orm

import (
	"fmt"
	"strings"
)

// query all data and map to containers.
// cols means the columns when querying.
func (o *querySet) Select(container interface{}, cols ...string) (int64, error) {
	return o.orm.alias.DbBaser.ReadSelect(o.orm.db, o, o.mi, o.cond, container, o.orm.alias.TZ, cols)
}

// returns processed model
func (o querySet) GetModel() *modelInfo {
	return o.mi
}

type sqlElements struct {
	From    string
	Where   string
	OrderBy string
	Limit   string
}

func (o *querySet) GetSQLStructure() *sqlElements {
	element := &sqlElements{}

	tables := newDbTables(o.mi, o.orm.alias.DbBaser)
	tables.parseRelated(o.related, o.relDepth)

	Q := o.mi.DbBaser().TableQuote()

	where, args := tables.getCondSQL(o.cond, false, o.orm.alias.TZ)
	order := tables.getOrderSQL(o.orders)
	limit := tables.getLimitSQL(o.mi, o.offset, o.limit)
	from := fmt.Sprintf("FROM %s%s%s T0 %s", Q, o.mi.table, Q, tables.getJoinSQL())

	for _, arg := range args {
		val := ToStr(arg)
		switch arg.(type) {
		case string:
			val = fmt.Sprintf("'%s'", val)
		default:
			//val = val
		}
		where = strings.Replace(where, "?", val, 1)
	}

	element.From = from
	element.Where = where
	element.OrderBy = order
	element.Limit = limit

	return element
}

//----------------------------------------------------------------------------------------------------------------------
// Agregate functions
//----------------------------------------------------------------------------------------------------------------------
// SUM of column
func (o *querySet) Sum(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Sum, column)
}

// MIN of column
func (o *querySet) Min(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Min, column)
}

// MAX of column
func (o *querySet) Max(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Max, column)
}

// AVG of column
func (o *querySet) Avg(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Avg, column)
}

// COUNT all values of records
func (o *querySet) CountAll(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_All, column)
}

// COUNT ALL UNIQUE values of column
func (o *querySet) CountUnique(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Unique, column)
}

// COUNT ALL EMPTY values of column
func (o *querySet) CountFilled(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Filled, column)
}

// COUNT ALL NOT EMPTY values of column
func (o *querySet) CountEmpty(column string) (string, error) {
	return o.orm.alias.DbBaser.agregateColumn(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Empty, column)
}

//----------------------------------------------------------------------------------------------------------------------
// Agregate functions with group by field of structure
//----------------------------------------------------------------------------------------------------------------------
// SUM of column group by field of model
func (o *querySet) SumGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Sum, column)
}

// MIN of column group by field of model
func (o *querySet) MinGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Min, column)
}

// MAX of column group by field of model
func (o *querySet) MaxGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Max, column)
}

// AVG of column group by field of model
func (o *querySet) AvgGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Avg, column)
}

// COUNT all values of records group by field of model
func (o *querySet) CountAllGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_All, column)
}

// COUNT ALL UNIQUE values of column group by field of model
func (o *querySet) CountUniqueGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Unique, column)
}

// COUNT ALL EMPTY values of column group by field of model
func (o *querySet) CountFilledGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Filled, column)
}

// COUNT ALL NOT EMPTY values of column group by field of model
func (o *querySet) CountEmptyGroup(column string) (*GroupAggregate, error) {
	return o.orm.alias.DbBaser.agregateColumnGroup(o.orm.db, o, o.mi, o.cond, o.orm.alias.TZ, Agregate_Count_Empty, column)
}
