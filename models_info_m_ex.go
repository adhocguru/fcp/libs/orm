package orm

import (
	"database/sql"
	"fmt"
	"reflect"
	"time"
)

func (mi *modelInfo) getFullTableName() string {
	return fmt.Sprintf("%s.%s", mi.schema, mi.table)
}

func (mi *modelInfo) getEventName() string {
	return mi.getFullTableName()
}

func (mi *modelInfo) getEventFuncName() string {
	return "fn_tr_event"
}

func (mi *modelInfo) getReplicaFuncName() string {
	return "fn_tr_replica"
}

func (mi *modelInfo) getReplicaTableName() string {
	return "sys_replica"
}

func (mi *modelInfo) getEventTriggerName() string {
	return fmt.Sprintf("tr_event_%s", mi.table)
}

func (mi *modelInfo) getReplicaTriggerName() string {
	return fmt.Sprintf("tr_replica_%s", mi.table)
}

func (mi *modelInfo) getSyncroFuncName() string {
	return "fn_tr_sync_2_link"
}

func (mi *modelInfo) getSyncroTriggerName(link_schema string) string {
	return fmt.Sprintf("tr_sync_%s_%s", mi.table, link_schema)
}

func (mi *modelInfo) Name() string {
	return mi.name
}

func (mi *modelInfo) FullName() string {
	return mi.fullName
}

func (mi *modelInfo) Typ() reflect.Type {
	return mi.addrField.Type()
}

func (mi *modelInfo) DbAlias() string {
	return mi.al.Name
}

func (mi *modelInfo) DbDriver() string {
	return mi.al.Server.Driver
}

func (mi *modelInfo) DbName() string {
	return mi.al.Server.DbName
}

func (mi *modelInfo) TimeZone() *time.Location {
	return mi.al.TZ
}

func (mi *modelInfo) DbBaser() dbBaser {
	return mi.al.DbBaser
}

func (mi *modelInfo) Schema() string {
	return mi.schema
}

func (mi *modelInfo) TableName() string {
	return mi.table
}

func (mi *modelInfo) IsVirtual() bool {
	return mi.virtual
}

func (mi *modelInfo) Fields() *fields {
	fs := *mi.fields
	return &fs
}

func (mi *modelInfo) GetObject() interface{} {
	return reflect.New(mi.addrField.Elem().Type()).Interface()
}

func (mi *modelInfo) GetSlice() interface{} {
	//return reflect.MakeSlice(reflect.SliceOf(mi.addrField.Type()), 0,0).Interface()
	return reflect.New(reflect.MakeSlice(reflect.SliceOf(mi.addrField.Type()), 0, 0).Type()).Interface()
}

//----------------------------------------------------------------------------------------------------------------------
// DB functions
//----------------------------------------------------------------------------------------------------------------------
func (mi *modelInfo) Orm(alias ...string) Ormer {
	o := newOrmWithAlias(mi.al)
	if len(alias) > 0 {
		o.UsingAlias(alias[0])
	}
	return o
}

func (mi *modelInfo) QuerySeter(alias ...string) QuerySeter {
	return mi.Orm(alias...).SetDebug(true).QueryTable(mi.table)
}

func (mi *modelInfo) QueryBuilder() QueryBuilder {
	return NewQueryBuilder(mi.DbDriver())
}

func (mi *modelInfo) DB() *sql.DB {
	return mi.al.DB
}
