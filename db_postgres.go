// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package orm

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
)

// postgresql operators.
var postgresOperators = map[string]string{
	"exact":       "= ?",
	"iexact":      "= UPPER(?)",
	"contains":    "LIKE ?",
	"icontains":   "LIKE UPPER(?)",
	"gt":          "> ?",
	"gte":         ">= ?",
	"lt":          "< ?",
	"lte":         "<= ?",
	"eq":          "= ?",
	"ne":          "!= ?",
	"startswith":  "LIKE ?",
	"endswith":    "LIKE ?",
	"istartswith": "LIKE UPPER(?)",
	"iendswith":   "LIKE UPPER(?)",
}

// postgresql column field types.
var postgresTypes = map[string]string{
	"auto":            "bigserial NOT NULL PRIMARY KEY",
	"pk":              "NOT NULL PRIMARY KEY",
	"bool":            "bool",
	"string":          "varchar(%d)",
	"string-text":     "text",
	"time.Time-clock": "time", // RUM
	"time.Time-date":  "date",
	"time.Time":       "timestamp with time zone",
	"int8":            `smallint CHECK("%COL%" >= -127 AND "%COL%" <= 128)`,
	"int16":           "smallint",
	"int32":           "integer",
	"int64":           "bigint",
	"uint8":           `smallint CHECK("%COL%" >= 0 AND "%COL%" <= 255)`,
	"uint16":          `integer CHECK("%COL%" >= 0)`,
	"uint32":          `bigint CHECK("%COL%" >= 0)`,
	"uint64":          `bigint CHECK("%COL%" >= 0)`,
	"float64":         "double precision",
	"float64-decimal": "numeric(%d, %d)",
	"json":            "json",
	"jsonb":           "jsonb",
}

// postgres dbBaser.
type dbBasePostgres struct {
	dbBase
}

var _ dbBaser = new(dbBasePostgres)

// get postgres operator.
func (d *dbBasePostgres) OperatorSQL(operator string) string {
	return postgresOperators[operator]
}

// generate functioned sql string, such as contains(text).
func (d *dbBasePostgres) GenerateOperatorLeftCol(fi *fieldInfo, operator string, leftCol *string) {
	switch operator {
	case "contains", "startswith", "endswith":
		*leftCol = fmt.Sprintf("%s::text", *leftCol)
	case "iexact", "icontains", "istartswith", "iendswith":
		*leftCol = fmt.Sprintf("UPPER(%s::text)", *leftCol)
	}
}

// postgres unsupports updating joined record.
func (d *dbBasePostgres) SupportUpdateJoin() bool {
	return false
}

func (d *dbBasePostgres) MaxLimit() uint64 {
	return 0
}

// postgres quote is ".
func (d *dbBasePostgres) TableQuote() string {
	return `"`
}

// get column types of postgres.
func (d *dbBasePostgres) DbTypes() map[string]string {
	return postgresTypes
}

// postgres value placeholder is $n.
// replace default ? to $n.
func (d *dbBasePostgres) ReplaceMarks(query *string) {
	q := *query
	num := 0
	for _, c := range q {
		if c == '?' {
			num++
		}
	}
	if num == 0 {
		return
	}
	data := make([]byte, 0, len(q)+num)
	num = 1
	for i := 0; i < len(q); i++ {
		c := q[i]
		if c == '?' {
			data = append(data, '$')
			data = append(data, []byte(strconv.Itoa(num))...)
			num++
		} else {
			data = append(data, c)
		}
	}
	*query = string(data)
}

// make returning sql support for postgres.
func (d *dbBasePostgres) HasReturningID(mi *modelInfo, query *string) bool {
	fi := mi.fields.pk
	if fi.fieldType&IsPositiveIntegerField == 0 &&
		fi.fieldType&IsIntegerField == 0 &&
		fi.fieldType != TypeVarCharField &&
		fi.fieldType != TypeCharField &&
		fi.fieldType != TypeTextField {
		return false
	}

	if query != nil {
		*query = fmt.Sprintf(`%s RETURNING "%s"`, *query, fi.column)
	}
	return true
}

// sync auto key
func (d *dbBasePostgres) setIdSequenceValue(db dbQuerier, mi *modelInfo, autoFields []string) error {
	if len(autoFields) == 0 {
		return nil
	}

	Q := d.ins.TableQuote()
	for _, name := range autoFields {
		query := fmt.Sprintf("SELECT setval(pg_get_serial_sequence('%s', '%s'), (SELECT MAX(%s%s%s) FROM %s%s%s));",
			mi.table, name,
			Q, name, Q,
			Q, mi.table, Q)
		if _, err := db.Exec(query); err != nil {
			return err
		}
	}
	return nil
}

// Support db ddl
func (d *dbBasePostgres) Support(dbElement) bool {
	return true
}

func (d *dbBasePostgres) GetIFExistsSQL() string {
	return "IF EXISTS"
}

func (d *dbBasePostgres) GetIFNotExistsSQL() string {
	return "IF NOT EXISTS"
}

// get true if user is a superuser
func (d *dbBasePostgres) UserExists(db dbQuerier, user string) (ok bool, err error) {
	query := fmt.Sprintf(`SELECT COUNT(*) FROM pg_user WHERE usename = '%s';`, user)

	rows, err := db.Query(query)
	if err != nil {
		err = fmt.Errorf("%v [%s]", err, query)
		return
	}

	defer rows.Close()

	var cnt int
	if rows.Next() {
		if err = rows.Scan(&cnt); err != nil {
			return
		}
	}
	ok = cnt != 0
	return
}

// get true if user is a superuser
func (d *dbBasePostgres) UserIsSuperuser(db dbQuerier, user string) (ok bool, err error) {
	//query := fmt.Sprintf(`SELECT COUNT(*) FROM pg_user WHERE usename = '%s' AND usecreatedb = true;`, user)
	//
	//rows, err := db.Query(query)
	//if err != nil {
	//	err = fmt.Errorf("%v [%s]", err, query)
	//	return
	//}
	//
	//defer rows.Close()
	//
	//var cnt int
	//if rows.Next() {
	//	if err = rows.Scan(&cnt); err != nil {
	//		return
	//	}
	//}
	//ok = cnt != 0
	//return

	// TODO roles superuser && usecreatedb are unaccesible
	return true, nil
}

// show shema sql for postgres.
func (d *dbBasePostgres) ShowSchemasQuery() string {
	return "SELECT nspname FROM pg_namespace"
}

func (d *dbBasePostgres) SchemaExists(db dbQuerier, schema string) (bool, error) {
	query := fmt.Sprintf(`SELECT count(*) 
								   FROM pg_namespace 
								  WHERE nspname = '%s'`, schema)
	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return cnt > 0, err
}

func (d *dbBasePostgres) GetShemasInfo(db dbQuerier) ([]*schemaInfo, error) {
	sis := []*schemaInfo{}

	query := `SELECT current_database()   as dbname
				   , n.nspname            as schema
				   , obj_description(oid) as desc
				FROM pg_namespace n
			   WHERE n.nspname not in('pg_catalog','information_schema')
				 AND n.nspname not like 'pg_%';`

	rows, err := db.Query(query)
	if err != nil {
		return sis, fmt.Errorf("%v [%s]", err, query)
	}
	defer rows.Close()

	for rows.Next() {
		si := &schemaInfo{}
		var desc sql.NullString
		err := rows.Scan(&si.dbname, &si.schema, &desc)
		if err != nil {
			panic(err.Error())
		}

		if desc.Valid {
			si.desc = desc.String
		}

		sis = append(sis, si)
	}
	return sis, nil
}

// get schema description
func (d *dbBasePostgres) GetShemaDescription(db dbQuerier, schema string) (desc string, err error) {
	query := fmt.Sprintf(`SELECT pg_catalog.obj_description(oid) FROM pg_namespace WHERE nspname = '%s';`, schema)

	rows, err := db.Query(query)
	if err != nil {
		err = fmt.Errorf("%v [%s]", err, query)
		return
	}
	defer rows.Close()

	var val sql.NullString
	if rows.Next() {
		if err = rows.Scan(&val); err != nil {
			return
		}
		desc = val.String
	}
	return
}

func (d *dbBasePostgres) SetShemaDescription(db dbQuerier, schema string, version string) error {
	query := fmt.Sprintf("COMMENT ON SCHEMA %s IS '%s'", schema, version)
	_, err := db.Exec(query)
	if err != nil {
		err = fmt.Errorf("%v [%s]", err, query)
		return err
	}
	return nil
}

func (d *dbBasePostgres) SetUserSearchPathQuery(user string, schemas ...string) []string {
	queries := []string{}
	if len(schemas) == 0 {
		return queries
	}
	queries = append(queries, fmt.Sprintf("ALTER ROLE %q SET search_path TO public,%s;", user, strings.Join(schemas, ",")))
	queries = append(queries, fmt.Sprintf("SET search_path TO public,%s;", strings.Join(schemas, ",")))
	return queries
}

func (d *dbBasePostgres) GetUserSearchPath(db dbQuerier, user string) (string, error) {
	query := `SHOW search_path`

	row := db.QueryRow(query)
	var res sql.NullString
	err := row.Scan(&res)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return res.String, err
}

// show shema sql for postgres.
func (d *dbBasePostgres) ShemaCreateQuery(schema string) string {
	return fmt.Sprintf(`CREATE SCHEMA IF NOT EXISTS %q;`, schema)
}

// show table sql for postgres.
func (d *dbBasePostgres) ShowTablesQuery(schemas ...string) string {
	query := `SELECT table_schema, table_name 
              FROM information_schema.tables 
             WHERE table_schema NOT IN ('pg_catalog', 'information_schema')`

	if len(schemas) > 0 {
		query += fmt.Sprintf(` AND table_schema IN('%s')`, strings.Join(schemas, "','"))
	}
	return query
}

// get all tables comment.
func (d *dbBasePostgres) GetTablesComment(db dbQuerier, schemas ...string) (map[string]string, error) {
	comments := make(map[string]string)

	query := `
      SELECT n.nspname      as table_schema
		   , c.relname      as table_name
		   , d.description as table_comment                    
	  FROM pg_class c
		   INNER JOIN pg_namespace n ON n.oid = c.relnamespace
		   LEFT  JOIN pg_description d ON (d.objoid = c.oid AND d.objsubid = 0)                         
	WHERE n.nspname NOT IN ('pg_catalog', 'information_schema')
	  AND c.relkind = 'r'`

	if len(schemas) > 0 {
		query += fmt.Sprintf(` AND n.nspname IN('%s')`, strings.Join(schemas, "','"))
	}

	rows, err := db.Query(query)
	if err != nil {
		return comments, fmt.Errorf("%v [%s]", err, query)
	}
	defer rows.Close()

	for rows.Next() {
		var schema string
		var table string
		var desc sql.NullString
		err := rows.Scan(&schema, &table, &desc)
		if err != nil {
			return comments, err
		}
		comments[fmt.Sprintf("%s.%s", schema, table)] = desc.String
	}
	return comments, nil
}

func (d *dbBasePostgres) TableExists(db dbQuerier, schema string, table string) (bool, error) {
	query := fmt.Sprintf(`SELECT count(*) 
								   FROM information_schema.tables 
								  WHERE table_schema  = '%s'
								    AND table_name    = '%s'`, schema, table)
	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return cnt > 0, err
}

// show table columns sql for postgres
func (d *dbBasePostgres) ShowColumnsQuery(schema string, table string) string {
	return fmt.Sprintf(`SELECT column_name, data_type, is_nullable = 'YES' as is_nullable 
                                 FROM information_schema.columns 
                                WHERE table_schema = '%s' 
                                  AND table_name = '%s'`, schema, table)
}

// not implement.
func (d *dbBasePostgres) ShowColumnsDescriptionQuery(schema string, table string) string {
	return fmt.Sprintf(
		`SELECT a.attname     as column_name
			 , d.description as column_comment
		  FROM pg_attribute a
			   INNER JOIN pg_class     c ON c.oid = a.attrelid
			   INNER JOIN pg_type      t ON t.oid = a.atttypid
			   INNER JOIN pg_namespace n ON n.oid = c.relnamespace
			   LEFT  JOIN pg_description d ON (d.objoid = c.oid AND d.objsubid = a.attnum)
		WHERE a.attnum > 0
		  AND n.nspname = '%s'
		  AND c.relname = '%s'`, schema, table)
}

func (d *dbBasePostgres) GetColumnsList(db dbQuerier, schema string, table string) (string, error) {
	query := fmt.Sprintf(`SELECT array_to_string(array_agg(a.attname), ',')
								   FROM pg_attribute a
									    INNER JOIN pg_class     c ON c.oid = a.attrelid
										INNER JOIN pg_type      t ON t.oid = a.atttypid
										INNER JOIN pg_namespace n ON n.oid = c.relnamespace
								  WHERE a.attnum > 0
                                    AND n.nspname = '%s'
									AND c.relname = '%s'`, schema, table)

	row := db.QueryRow(query)
	var res sql.NullString
	err := row.Scan(&res)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return res.String, err
}

// get all cloumns description in table.
func (d *dbBasePostgres) GetColumnsDescription(db dbQuerier, schema string, table string) (map[string]string, error) {
	columns := make(map[string]string)
	query := d.ins.ShowColumnsDescriptionQuery(schema, table)
	rows, err := db.Query(query)
	if err != nil {
		return columns, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			name string
			desc sql.NullString
		)
		err := rows.Scan(&name, &desc)
		if err != nil {
			return columns, err
		}
		columns[name] = desc.String
	}
	return columns, nil
}

func (d *dbBasePostgres) ConstraintExistQuery(schema string, table string, constraint string) string {
	return fmt.Sprintf(`SELECT 1 
                                 FROM information_schema.table_constraints 
                                WHERE table_schema = '%s' 
                                  AND table_name = '%s' 
                                  AND constraint_name = '%s'`, schema, table, constraint)
}

func (d *dbBasePostgres) ConstraintExist(db dbQuerier, schema string, table string, constraint string) (bool, error) {
	query := fmt.Sprintf(`SELECT COUNT(*) FROM ( %s ) d`, d.ConstraintExistQuery(schema, table, constraint))

	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return cnt > 0, err
}

func (d *dbBasePostgres) ConstraintDropQuery(schema string, table string, constraint string) string {
	return fmt.Sprintf(`ALTER TABLE IF EXISTS "%s"."%s" DROP CONSTRAINT IF EXISTS "%s";`, schema, table, constraint)
}

func (d *dbBasePostgres) IndexDropQuery(schema string, index string) string {
	return fmt.Sprintf(`DROP INDEX IF EXISTS %s.%s;`, schema, index)
}

// check index exist in postgres.
func (d *dbBasePostgres) IndexExists(db dbQuerier, schema string, table string, index string) (bool, error) {
	query := fmt.Sprintf(`SELECT COUNT(*)
								   FROM pg_indexes
								  WHERE schemaname = '%s'
	                                AND tablename  = '%s'
	                                AND indexname  = '%s'`, schema, table, index)

	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return cnt > 0, err
}

// get all tables Indexes.
func (d *dbBasePostgres) GetTablesIndexes(db dbQuerier, schemas ...string) (map[string]idxs, error) {
	indexes := make(map[string]idxs)

	query := fmt.Sprintf(
		`SELECT table_schema
             , table_name
             , index_name
             , array_to_string(array_agg(column_name), ',') as columns
             , is_primary
             , is_unique  
             , is_constraint
      FROM ( SELECT n.nspname as table_schema
                  , c.relname as table_name
                  , t.relname as index_name
                  , a.attname as column_name
                  , i.indisunique  AS is_unique
                  , i.indisprimary AS is_primary
                  , k.constraint_type IS NOT NULL as is_constraint
                  , unnest(i.indkey) AS unn
                  , a.attnum
             FROM pg_class c
                  INNER JOIN pg_namespace n ON n.oid = c.relnamespace
                  INNER JOIN pg_index     i ON i.indrelid = c.oid
                  INNER JOIN pg_class     t ON t.oid = i.indexrelid
                  INNER JOIN pg_attribute a ON a.attrelid = c.oid
                  LEFT  JOIN information_schema.table_constraints k ON k.table_schema = n.nspname
                                                                   AND k.table_name = c.relname
                                                                   AND k.constraint_name = t.relname
            WHERE c.relkind = 'r'
              AND a.attnum = ANY(i.indkey)
              AND n.nspname IN('%s')
           ORDER BY n.nspname, c.relname, t.relname, generate_subscripts(i.indkey, 1)
         ) d
    WHERE d.unn = d.attnum
    GROUP BY table_schema, table_name, index_name, is_primary, is_unique, is_constraint 
    ORDER BY table_schema, table_name, index_name;`, strings.Join(schemas, "','"))

	rows, err := db.Query(query)
	if err != nil {
		return indexes, fmt.Errorf("%v [%s]", err, query)
	}
	defer rows.Close()

	for rows.Next() {
		idx := new(indexInfo)
		err := rows.Scan(&idx.Schema, &idx.Table, &idx.IndexName, &idx.ColumnsCommaText, &idx.IsPrimary, &idx.IsUnique, &idx.IsConstraint)
		if err != nil {
			return indexes, err
		}
		idx.Columns = strings.Split(idx.ColumnsCommaText, ",")

		table := idx.getFullTableName()
		index := idx.getFullIndexName()

		if _, ok := indexes[table]; !ok {
			indexes[table] = make(map[string]*indexInfo)
		}
		indexes[table][index] = idx
	}
	return indexes, nil
}

// Function Exists
func (d *dbBasePostgres) FuncExists(db dbQuerier, schema string, fn_name string) (bool, error) {
	query := fmt.Sprintf(`SELECT count(*)
								   FROM pg_catalog.pg_proc p
									    left join pg_catalog.pg_namespace n on n.oid = p.pronamespace
								  WHERE n.nspname = '%s' 
								    AND p.proname = '%s'`, schema, fn_name)
	row := db.QueryRow(query)
	var cnt int
	err := row.Scan(&cnt)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return cnt > 0, err
}

// get function source
func (d *dbBasePostgres) GetFuncSource(db dbQuerier, schema string, fn_name string) (string, error) {
	query := fmt.Sprintf(`SELECT p.prosrc
	                               FROM pg_catalog.pg_proc p
	                                    INNER JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace
	                              WHERE n.nspname = '%s'
	                                AND p.proname = '%s';`, schema, fn_name)
	row := db.QueryRow(query)
	var res sql.NullString
	err := row.Scan(&res)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return res.String, err
}

func (d *dbBasePostgres) GetFuncHeaders(db dbQuerier, schema string, fn_name string) (params []string, err error) {
	query := `SELECT format('%I.%I(%s)', n.nspname, p.proname, pg_get_function_arguments(p.oid))
 			    FROM pg_proc p
				     INNER JOIN pg_namespace n ON n.oid = p.pronamespace
			   WHERE n.nspname = '` + schema + `' AND p.proname = '` + fn_name + `'`

	rows, err := db.Query(query)
	if err != nil {
		return
	}
	defer rows.Close()

	var fn_param *sql.NullString

	for rows.Next() {
		err = rows.Scan(&fn_param)
		if err != nil {
			return
		}
		params = append(params, fn_param.String)
	}

	if err = rows.Err(); err != nil {
		return
	}
	return
}

func (d *dbBasePostgres) GetFuncDescription(db dbQuerier, schema string, fn_name string) (string, error) {
	query := fmt.Sprintf(`SELECT d.description
								   FROM pg_proc p
										INNER JOIN pg_namespace  n  on n.oid = p.pronamespace
										INNER JOIN pg_description d on d.objoid = p.oid
								  WHERE d.objsubid = 0
									AND n.nspname = '%s'
									AND p.proname = '%s';`, schema, fn_name)
	row := db.QueryRow(query)
	var res sql.NullString
	err := row.Scan(&res)
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
		} else {
			err = fmt.Errorf("%v, [%s]", err, query)
		}
	}
	return res.String, err
}

func (d *dbBasePostgres) SetFuncDescription(db dbQuerier, schema string, fn_name string, desc string) error {
	params, err := d.GetFuncHeaders(db, schema, fn_name)
	if err != nil {
		return err
	}

	for _, fn_header := range params {
		query := fmt.Sprintf(`COMMENT ON FUNCTION %s IS '%s';`, fn_header, desc)
		if _, err := db.Exec(query); err != nil {
			err = fmt.Errorf("%v, [%s]", err, query)
			return err
		}
	}
	return nil
}

// check if trigger active
func (d *dbBasePostgres) IsTriggerActive(db dbQuerier, schema string, tr_name string) bool {
	query := fmt.Sprintf(`SELECT t.tgenabled = 'O'
								   FROM pg_catalog.pg_trigger t
										INNER JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
										INNER JOIN pg_catalog.pg_namespace n on n.oid = c.relnamespace
								  WHERE n.nspname = '%s'
									AND tgname = '%s'`, schema, tr_name)
	row := db.QueryRow(query)
	var res sql.NullBool
	row.Scan(&res)
	return res.Bool
}

// create new postgres dbBaser.
func newdbBasePostgres() dbBaser {
	b := new(dbBasePostgres)
	b.ins = b
	return b
}
