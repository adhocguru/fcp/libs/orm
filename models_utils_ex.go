package orm

import (
	"encoding/json"
	"reflect"
	"strconv"
	"strings"
)

// get table ExData from method.
func getTableExData(val reflect.Value) map[DBTableExDataType]interface{} {
	fun := val.MethodByName("TableExData")
	if fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		if len(vals) > 0 && vals[0].CanInterface() {
			if d, ok := vals[0].Interface().(map[DBTableExDataType]interface{}); ok {
				return d
			}
		}
	}
	return make(map[DBTableExDataType]interface{})
}

// getTableName get struct table name.
// If the struct implement the TableName, then get the result as tablename
// else use the struct name which will apply snakeString.
func getTableSchema(val reflect.Value) string {
	// Find in TableInfo
	ti := getTableExData(val)
	if value, ok := ti[DB_table_schema]; ok {
		if s, ok := value.(string); ok {
			return strings.ToLower(s)
		}
	}
	return "public"
}

// get table ExData from method.
func getTableIsVirtual(val reflect.Value) bool {
	extData := getTableExData(val)
	if val, ok := extData[DB_table_virtual]; ok {
		return val.(bool)
	}
	return false
}

// get table ExData from method.
func getTableIsExtMigration(val reflect.Value) bool {
	extData := getTableExData(val)
	if val, ok := extData[DB_table_ext_migration]; ok {
		return val.(bool)
	}
	return false
}

// get table index from method.
func getTableMigration(val reflect.Value) string {
	if fun := val.MethodByName("TableMigration"); fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		// has return and the first val is string
		if len(vals) > 0 && vals[0].Kind() == reflect.String {
			return vals[0].String()
		}
	}
	return ""
}

// get table index from method.
func getTableDataInit(val reflect.Value) string {
	if fun := val.MethodByName("TableDataInit"); fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		// has return and the first val is string
		if len(vals) > 0 && vals[0].Kind() == reflect.String {
			return vals[0].String()
		}
	}
	return ""
}

// get table index from method.
func getTableDataTest(val reflect.Value) string {
	if fun := val.MethodByName("TableDataTest"); fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		// has return and the first val is string
		if len(vals) > 0 && vals[0].Kind() == reflect.String {
			return vals[0].String()
		}
	}
	return ""
}

// get table unique from method
func getTableFunction(val reflect.Value) []string {
	fun := val.MethodByName("TableFunction")
	if fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		if len(vals) > 0 && vals[0].CanInterface() {
			if d, ok := vals[0].Interface().([]string); ok {
				return d
			}
		}
	}
	return nil
}

type DBTrigger struct {
	Name string
	Desc string
}

func getTableTrigger(val reflect.Value) []DBTrigger {
	fun := val.MethodByName("TableTrigger")
	if fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		if len(vals) > 0 && vals[0].CanInterface() {
			if d, ok := vals[0].Interface().([]DBTrigger); ok {
				return d
			}
		}
	}
	return nil
}

func getTableIndexEx(val reflect.Value) [][][]string {
	fun := val.MethodByName("TableIndexEx")
	if fun.IsValid() {
		vals := fun.Call([]reflect.Value{})
		if len(vals) > 0 && vals[0].CanInterface() {
			if d, ok := vals[0].Interface().([][][]string); ok {
				return d
			}
		}
	}
	return nil
}

// rum
// parse json tag string
func parseJsonTag(fieldName string, jsonData string) (jsonName string) {
	for _, v := range strings.Split(jsonData, jsonStructTagDelim) {
		v = strings.TrimSpace(v)
		if v != "" {
			jsonName = strings.TrimSpace(v)
			break
		}
	}
	// if json: is missing set front name as field of structure Name
	if len(jsonName) == 0 {
		jsonName = fieldName
	}
	return
}

// parse logged tag string
func parseLoggedTag(Data string) bool {
	if res, err := strconv.ParseBool(Data); err == nil {
		return res
	}
	return true
}

// parse logged tag string
func parseDescriptionTag(Data string) string {
	comment := strings.TrimSpace(Data)
	if len(comment) > 0 {
		return comment
	}
	return ""
}

func cloneObject(obj interface{}) interface{} {
	original := reflect.ValueOf(obj)
	theCopy := reflect.New(original.Type()).Elem()
	cloneRecursive(theCopy, original)
	return theCopy.Interface()
}

func cloneRecursive(copy, original reflect.Value) {

	if copy.CanSet() {
		copy.Set(original)
	}
	switch original.Kind() {
	case reflect.Ptr:
		originalValue := original.Elem()
		if !originalValue.IsValid() {
			return
		}
		if copy.CanSet() {
			copy.Set(reflect.New(originalValue.Type()))
		}
		cloneRecursive(copy.Elem(), originalValue)

	case reflect.Interface:
		originalValue := original.Elem()
		copyValue := reflect.New(originalValue.Type()).Elem()
		cloneRecursive(copyValue, originalValue)
		if copy.CanSet() {
			copy.Set(copyValue)
		}

	case reflect.Struct:
		for i := 0; i < original.NumField(); i += 1 {
			cloneRecursive(copy.Field(i), original.Field(i))
		}

	case reflect.Slice:
		if copy.CanSet() {
			copy.Set(reflect.MakeSlice(original.Type(), original.Len(), original.Cap()))
		}
		for i := 0; i < original.Len(); i += 1 {
			cloneRecursive(copy.Index(i), original.Index(i))
		}

	case reflect.Map:
		if copy.CanSet() {
			copy.Set(reflect.MakeMap(original.Type()))
		}
		for _, key := range original.MapKeys() {
			originalValue := original.MapIndex(key)
			// New gives us a pointer, but again we want the value
			copyValue := reflect.New(originalValue.Type()).Elem()
			cloneRecursive(copyValue, originalValue)
			copy.SetMapIndex(key, copyValue)
		}
	}

}

func js(x interface{}) string {
	js, _ := json.Marshal(x)
	return string(js)
}
