module gitlab.com/adhocguru/fcp/libs/orm

go 1.16

require (
	github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.7.0
	gitlab.com/adhocguru/fcp/libs/utils v0.0.0-20211007062010-e282adeae12d
	go.uber.org/zap v1.19.1
	google.golang.org/protobuf v1.27.1
)
