package orm

import (
	"database/sql"
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"reflect"
	"strings"
	"time"
)

const (
	Agregate_Sum          = "sum"          // сумма по колонке
	Agregate_Min          = "min"          // минимальное значение по колонке
	Agregate_Max          = "max"          // максимальное значение по колонке
	Agregate_Avg          = "avg"          // среднее по колонке
	Agregate_Count_All    = "count_all"    // кол-во всех записей
	Agregate_Count_Unique = "count_unique" // кол-во уникальных записей
	Agregate_Count_Filled = "count_filled" // кол-во заполненных записей
	Agregate_Count_Empty  = "count_empty"  // кол-во пустых записей
)

//----------------------------------------------------------------------------------------------------------------------
type paramsInfo struct {
	tbls      []*dbTable
	fis       []*fieldInfo
	misCols   map[*modelInfo][]string // models info columns
	misFields map[*modelInfo][]string // models info columns
	mis       []*modelInfo            // models info
	mns       []string                // models name

	names   []string
	columns []string
}

func new_paramsInfo() *paramsInfo {
	t := &paramsInfo{
		tbls:      []*dbTable{},
		fis:       []*fieldInfo{},
		misCols:   make(map[*modelInfo][]string),
		misFields: make(map[*modelInfo][]string),
		mns:       []string{},
		names:     []string{},
		columns:   []string{},
	}
	return t
}

func (t *paramsInfo) Add(tbl *dbTable, fi *fieldInfo) {
	t.tbls = append(t.tbls, tbl)
	t.fis = append(t.fis, fi)

	model_tree := fi.mi.name
	if tbl != nil {
		model_tree = tbl.name
	}

	if _, ok := t.misCols[fi.mi]; !ok {
		t.misCols[fi.mi] = []string{}
		t.mns = append(t.mns, model_tree)
		t.mis = append(t.mis, fi.mi)
	}
	t.misCols[fi.mi] = append(t.misCols[fi.mi], fi.column)
	t.misFields[fi.mi] = append(t.misCols[fi.mi], fi.name)

	t.names = append(t.names, fmt.Sprintf("%s%s%s", model_tree, ExprSep, fi.name))
	t.columns = append(t.columns, fi.column)
}

func (t *paramsInfo) Contains(model_tree string) bool {
	return utils.SliceContains(t.mns, model_tree)
}

func (t *paramsInfo) getModelColumns(mi *modelInfo) []string {
	list := []string{}
	if cols, ok := t.misCols[mi]; ok {
		return cols
	}
	return list
}

func (d *dbBase) ReadSelect(q dbQuerier, qs *querySet, mi *modelInfo, cond *Condition, container interface{}, tz *time.Location, cols []string) (int64, error) {

	if len(cols) == 0 {
		return d.ReadBatch(q, qs, mi, cond, container, tz, cols)
	}

	val := reflect.ValueOf(container)
	ind := reflect.Indirect(val)
	errTyp := true
	oneRec := true
	isPtr := true

	if val.Kind() == reflect.Ptr {
		fn := ""
		if ind.Kind() == reflect.Slice {
			oneRec = false
			typ := ind.Type().Elem()
			switch typ.Kind() {
			case reflect.Ptr:
				fn = getFullName(typ.Elem())
			case reflect.Struct:
				isPtr = false
				fn = getFullName(typ)
			}
		} else {
			fn = getFullName(ind.Type())
		}
		errTyp = fn != mi.fullName
	}

	if errTyp {
		if oneRec {
			panic(fmt.Errorf("wrong object type `%s` for rows scan, need *%s", val.Type(), mi.fullName))
		} else {
			panic(fmt.Errorf("wrong object type `%s` for rows scan, need *[]*%s or *[]%s", val.Type(), mi.fullName, mi.fullName))
		}
	}

	// вычисляем дерево данных для запроса
	tables := newDbTables(mi, d.ins)
	for _, column := range cols {
		if _, _, _, ok := tables.parseExprs(mi, strings.Split(column, ExprSep), true); !ok {
			return 0, fmt.Errorf("orm: unknown field/column name `%s`", column)
		}
	}

	// установим все таблицы как выбыраемые
	for _, tbl := range tables.tables {
		tbl.sel = true
	}

	rlimit := qs.limit
	offset := qs.offset
	Q := d.ins.TableQuote()

	// сначала зачитаем условия т.к. они тоже вносятт в структуру запроса свои связи
	where, args := tables.getCondSQL(cond, false, tz)
	if len(where) > 0 {
		where = fmt.Sprintf("\n%s", where)
	}
	groupBy := tables.getGroupSQL(qs.groups)
	if len(groupBy) > 0 {
		groupBy = fmt.Sprintf("\n%s", groupBy)
	}
	orderBy := tables.getOrderSQL(qs.orders)
	if len(orderBy) > 0 {
		orderBy = fmt.Sprintf("\n%s", orderBy)
	}
	limit := tables.getLimitSQL(mi, offset, rlimit)
	if len(limit) > 0 {
		limit = fmt.Sprintf("\n%s", limit)
	}
	join := tables.getJoinSQL() // after all condition

	// make field list of mi(T0)
	//tCols := tables.getSelectColumns(mi.name) // columns of main model mi(T0)
	//sep   := fmt.Sprintf("%s, T0.%s", Q, Q)
	//sels  := fmt.Sprintf("\n T0.%s%s%s", Q, strings.Join(tCols, sep), Q)

	mCols := append([]string{}, tables.getSelectColumns(mi.name)...) // columns of main model mi(T0)
	tCols := append([]string{}, mCols...)
	for i, val := range mCols {
		mCols[i] = fmt.Sprintf("T0.%s%s%s as %s_%s", Q, val, Q, strings.ToLower(mi.name), strings.ToLower(val))
	}
	sels := fmt.Sprintf("\n  %s", strings.Join(mCols, CommaSpace))

	for _, tbl := range tables.tables {
		if tbl.sel {
			mCols := append([]string{}, tables.getSelectColumns(tbl.name)...)
			if len(mCols) > 0 {
				tCols = append(tCols, mCols...)

				//sep := fmt.Sprintf("%s, %s.%s", Q, tbl.index, Q)
				//sels += fmt.Sprintf("\n, %s.%s%s%s", tbl.index, Q, strings.Join(mCols, sep), Q)
				for i, val := range mCols {
					mCols[i] = fmt.Sprintf("%s.%s%s%s as %s_%s", tbl.index, Q, val, Q, strings.ToLower(tbl.name), strings.ToLower(val))
				}
				sels += fmt.Sprintf("\n, %s", strings.Join(mCols, CommaSpace))
			}
		}
	}

	sqlSelect := "SELECT"
	if qs.distinct {
		sqlSelect += " DISTINCT"
	}
	query := fmt.Sprintf("%s %s\nFROM %s%s%s T0 %s%s%s%s%s", sqlSelect, sels, Q, mi.table, Q, join, where, groupBy, orderBy, limit)

	d.ins.ReplaceMarks(&query)

	var rs *sql.Rows
	r, err := q.Query(query, args...)
	if err != nil {
		return 0, err
	}
	rs = r

	tSize := len(tCols)

	refs := make([]interface{}, tSize)
	for i := range refs {
		var ref interface{}
		refs[i] = &ref
	}

	defer rs.Close()

	slice := ind

	var cnt int64
	for rs.Next() {
		if oneRec && cnt == 0 || !oneRec {
			if err := rs.Scan(refs...); err != nil {
				return 0, err
			}

			elm := reflect.New(mi.addrField.Elem().Type())
			mind := reflect.Indirect(elm)

			cacheV := make(map[string]*reflect.Value)
			cacheM := make(map[string]*modelInfo)
			trefs := refs

			tCols = tables.tablesC[mi.name]
			d.setColsValues(mi, &mind, tCols, refs[:len(tCols)], tz)
			trefs = refs[len(tCols):]

			for _, tbl := range tables.tables {
				// loop selected tables
				if tbl.sel {
					mmi := mi
					last := mind
					model := ""
					// loop cascade models
					for _, name := range tbl.names {
						model += name

						if val, ok := cacheV[model]; ok {
							last = *val
							mmi = cacheM[model]
						} else {
							tCols := tables.tablesC[model] // data for model

							fi := mmi.fields.GetByName(name)
							lastm := mmi
							mmi = fi.relModelInfo
							field := last
							if last.Kind() != reflect.Invalid {
								field = reflect.Indirect(last.FieldByIndex(fi.fieldIndex))
								if field.IsValid() {
									// set data to model
									d.setColsValues(mmi, &field, tCols, trefs[:len(tCols)], tz)
									for _, fi := range mmi.fields.fieldsReverse {
										if fi.inModel && fi.reverseFieldInfo.mi == lastm {
											if fi.reverseFieldInfo != nil {
												f := field.FieldByIndex(fi.fieldIndex)
												if f.Kind() == reflect.Ptr {
													f.Set(last.Addr())
												}
											}
										}
									}
									last = field
								}
							}
							cacheV[model] = &field
							cacheM[model] = mmi
							trefs = trefs[len(tCols):] // truncate used data
						}
						model += ExprSep
					}
				}
			}

			if oneRec {
				ind.Set(mind)
			} else {
				if cnt == 0 {
					// you can use a empty & caped container list
					// orm will not replace it
					if ind.Len() != 0 {
						// if container is not empty
						// create a new one
						slice = reflect.New(ind.Type()).Elem()
					}
				}

				if isPtr {
					slice = reflect.Append(slice, mind.Addr())
				} else {
					slice = reflect.Append(slice, mind)
				}
			}
		}
		cnt++
	}

	if !oneRec {
		if cnt > 0 {
			ind.Set(slice)
		} else {
			// when a result is empty and container is nil
			// to set a empty container
			if ind.IsNil() {
				ind.Set(reflect.MakeSlice(ind.Type(), 0, 0))
			}
		}
	}

	return cnt, nil
}
