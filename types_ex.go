package orm

import (
	"fmt"
	"strings"
)

// schemas info structure
type schemaInfo struct {
	al     *alias // schema alias
	dbname string // schema db name
	schema string // schema name
	desc   string // schema description

	dbo_ver float64 // version in dbo-file
	db_ver  float64 // version in database
	skip    bool    // used in system
	used    bool    // skip for test structure
}

// tables info structure
type tableInfo struct {
	schema string // schema name
	table  string // table name
	exist  bool   // exist in system
	used   bool   // skip for test structure
}

func (ti *tableInfo) getFullTableName() string {
	return fmt.Sprintf("%s.%s", ti.schema, ti.table)
}

// indexInfo structure
const maxIndexNameSize = 63

type indexInfo struct {
	Schema           string
	Table            string
	IndexName        string
	Columns          []string
	ColumnsCommaText string
	IsUnique         bool
	IsPrimary        bool
	IsConstraint     bool
}
type indexSetter struct {
	data *indexInfo
}

func newIndexInfo() *indexSetter {
	this := &indexSetter{}
	this.data = &indexInfo{}
	return this
}

func (this *indexInfo) getFullTableName() string {
	return fmt.Sprintf("%s.%s", this.Schema, this.Table)
}

func (this *indexInfo) getFullIndexName() string {
	return fmt.Sprintf("%s.%s.%s", this.Schema, this.Table, this.IndexName)
}

func (this *indexInfo) getUniqueWord() string {
	if !this.IsUnique {
		return ""
	}
	return "UNIQUE"
}

func (this *indexInfo) GetSQL(newSchema ...string) []string {
	schema := append(newSchema, this.Schema)[0]
	//if schema != "" {
	//	schema = fmt.Sprintf("%s", schema)
	//}

	mi := GetModel(this.Table)

	q := mi.al.DbBaser.TableQuote()
	sep := fmt.Sprintf("%s%s%s", q, CommaSpace, q)

	idx_cols := fmt.Sprintf("%s%s%s", q, strings.Join(this.Columns, sep), q)
	idx_table := fmt.Sprintf("%s%s%s.%s%s%s", q, schema, q, q, mi.table, q)
	idx_name := fmt.Sprintf("%s%s%s", q, this.IndexName, q)

	if !this.IsUnique {
		NotExistSQL := mi.al.DbBaser.GetIFNotExistsSQL() // IF NOT EXISTS support ddl
		sql := fmt.Sprintf("CREATE INDEX %s %s ON %s(%s);", NotExistSQL, idx_name, idx_table, idx_cols)
		return []string{sql}
	}

	sqls := []string{}

	// UNIQUE - generate CONSTRAINT SQL
	enabled := mi.al.DbBaser.GetEnabledSQL() // Vertica feature( enabled )

	// make query for test and delete double data
	sqls = append(sqls, fmt.Sprintf(
		`DELETE FROM %s WHERE id in (SELECT id FROM %s c, (SELECT min(id) as min_id, %s FROM %s WHERE %s GROUP BY %s HAVING COUNT(*) > 1) d WHERE c.id > d.min_id AND %s )`,
		idx_table, idx_table, idx_cols, idx_table,
		fmt.Sprintf("%s%s", strings.Join(strings.Split(idx_cols, CommaSpace), " IS NOT NULL AND "), " IS NOT NULL "),
		idx_cols,
		strings.Join(func() []string { // --- AND condition ---
			and_conds := []string{}
			//for _, col := range this.Columns {
			//	col = fmt.Sprintf("%s%s%s", q, col, q)
			//	and_conds = append(and_conds, fmt.Sprintf("(c.%s = d.%s OR (c.%s IS NULL AND d.%s IS NULL))", col, col, col, col))
			//}
			for _, col := range strings.Split(idx_cols, CommaSpace) {
				and_conds = append(and_conds, fmt.Sprintf("c.%s = d.%s", col, col))
			}
			return and_conds
		}(), " AND ")))

	// make query create constraint
	sqls = append(sqls, mi.al.DbBaser.ConstraintDropQuery(schema, mi.table, this.IndexName))
	sqls = append(sqls, fmt.Sprintf("ALTER TABLE %s ADD CONSTRAINT %s UNIQUE (%s) %s;", idx_table, idx_name, idx_cols, enabled))
	return sqls
}

func (this *indexInfo) GetConstraintSQL() string {
	if !this.IsUnique {
		return "/*NOT UNIQUE*/"
	}

	mi := GetModel(this.Table)
	q := mi.al.DbBaser.TableQuote()
	sep := fmt.Sprintf("%s, %s", q, q)

	idx_cols := fmt.Sprintf("%s%s%s", q, strings.Join(this.Columns, sep), q)
	idx_name := fmt.Sprintf("%s%s%s", q, this.IndexName, q)

	// UNIQUE - generate CONSTRAINT SQL
	enabled := mi.al.DbBaser.GetEnabledSQL()
	sql := fmt.Sprintf("CONSTRAINT %s UNIQUE (%s) %s", idx_name, idx_cols, enabled)
	return sql
}

func (this *indexSetter) CopyBy(idx *indexInfo) *indexSetter {
	this.data.Schema = idx.Schema
	this.data.Table = idx.Table
	this.data.IndexName = idx.IndexName
	this.data.Columns = idx.Columns
	this.data.ColumnsCommaText = idx.ColumnsCommaText
	this.data.IsUnique = idx.IsUnique
	this.data.IsPrimary = idx.IsPrimary
	this.data.IsConstraint = idx.IsConstraint
	return this
}

func (this *indexSetter) TableBy(mi *modelInfo) *indexSetter {
	this.data.Schema = mi.schema
	this.data.Table = mi.table
	return this
}

func (this *indexSetter) ColunmsBy(fields []*fieldInfo) *indexSetter {
	this.data.Columns = make([]string, 0, len(fields))
	for _, fi := range fields {
		this.data.Columns = append(this.data.Columns, fi.column)
	}
	this.data.ColumnsCommaText = strings.Join(this.data.Columns, ",")
	return this
}

func (this *indexSetter) Unique(v bool) *indexSetter {
	this.data.IsUnique = v
	return this
}

func (this *indexSetter) Constraint(v bool) *indexSetter {
	this.data.IsConstraint = v
	return this
}

func (this *indexSetter) Get() *indexInfo {
	this.data.IndexName = fmt.Sprintf("%s_%s", this.data.Table, strings.Join(this.data.Columns, "_"))
	this.data.IndexName = this.data.IndexName[:min(len(this.data.IndexName), maxIndexNameSize)]
	if this.data.IsUnique {
		this.data.IndexName = this.data.IndexName[:min(len(this.data.IndexName), maxIndexNameSize-4)] + "_key"
	}
	return this.data
}

type idxs map[string]*indexInfo // [index_name]*indexInfo
//type idxs map[string]map[string]*indexInfo // [schema][table]*indexInfo
