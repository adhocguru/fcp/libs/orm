package orm

import (
	"database/sql"
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"strings"
)

func (t *syncDb) dbCreateProgrammObjects() {

	//el := newElapser()	defer el.Show("dbCreateProgrammObjects()")
	logger.Infof("orm: testing programm objects...")

	for _, al := range t.als {
		t.al = al

		prg_sqls := make(map[dbElement][]string)
		//--------------------------------------------------------------------------------------------------
		//--------------------- Функции и триггера ---------------------------------------------------------
		//--------------------------------------------------------------------------------------------------
		// Зачитаем обьекты функции/триггера из описания модели
		d_func_enabled := al.DbBaser.Support(db_function) // can use function for the database
		d_trig_enabled := al.DbBaser.Support(db_trigger)  // can use triegger for the database

		// получим список программных обьектов для моделей алиаса БД
		for mi, db_objects := range t.getDBProgramObjects() {

			// и обработаем их
			for _, db_object := range db_objects {

				// исходный код обькта в dbo файле
				db_object_src, err := t.dbo.find(al.Name, db_object.Name)
				if err != nil {
					//continue
					panic(err)
				}

				switch db_object.Type {
				case db_function:
					if d_func_enabled {
						// Исходник функции в dbo
						pr := &procInfo{
							al:     al,
							dbname: al.Server.DbName,
							schema: mi.schema,
							name:   db_object.Name,
							header: GetFunctionHeader(*db_object_src),
							body:   GetFunctionBody(*db_object_src),
							source: *db_object_src}

						// ищем функцию в списке функций базы
						pit := t.findFunction(pr)

						// формируем скрипт создания/обновления
						prg_sqls[db_object.Type] = append(prg_sqls[db_object.Type], t.dbFuncSqlMaker(pr, pit)...)

						// удаляем из списка фнкций обработанный
						if pit != nil {
							t.prs[pit.pi.al.Server.server()][pit.pi.schema][pit.pi.name].used = true
						}

					}
				case db_trigger:
					if d_trig_enabled {
						// исходный код триггера в dbo
						dbo_source, _ := replaceDoubleSpace(*db_object_src) // remove double space
						dbo_source = strings.Trim(dbo_source, " ")
						dbo_source = strings.TrimRight(dbo_source, ";")

						// описание триггера для таблицы
						tr := &triggerInfo{
							al:      mi.al,
							dbname:  mi.al.Server.DbName,
							schema:  mi.schema,
							table:   mi.table,
							name:    db_object.Name,
							enabled: true,
							source:  dbo_source, // dbo source
							desc:    db_object.Desc,
						}

						// проверяем существует ли триггер в базе
						ti := t.findTrigger(tr)

						// сравниваем описание триггера с триггером в базе
						if ok, msg := t.dbTriggerCompare(tr, ti); !ok {
							logger.Warnf(msg)
							// команды обновления триггера
							prg_sqls[db_object.Type] = append(prg_sqls[db_object.Type],
								t.dbTriggerDrop(tr),             // DROP TRIGGER
								tr.source,                       // CREATE TRIGGER ( ROW )
								t.dbTriggerComment(tr, tr.desc)) // COMMENT ON TRIGGER
						}

						// удаляем из списка триггеров обработанный
						if ti != nil {
							delete(t.trs[tr.al.Server.server()][tr.schema][tr.table], tr.name)
						}
					}
				}
			}
		}

		// Update structure
		sqls := []string{}
		sqls = append(sqls, prg_sqls[db_function]...)
		sqls = append(sqls, prg_sqls[db_trigger]...)

		// Check version of database
		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
			continue
		}

		// если менялась структура
		//if len(sqls) > 0 {
		//	// версия базы должна быть выше текущей, иначе предыдущая версия перезапишет данные, что приведет к краху данных
		//	t.panicIfShemaVersionNotUpdated(al)
		//}

		// Exec commands
		t.ExecSQL(al, &sqls, fmt.Sprintf("updating programming objects of %s:%s.%s", al.Server.Driver, al.Server.DbName, al.Server.Schema))
	}

	return
}

func (t *syncDb) dbCreateProgrammNotify() {

	//el := newElapser()	defer el.Show("dbCreateProgrammNotify()")
	logger.Infof("orm: testing notify objects...")

	for _, al := range t.als {
		t.al = al

		//d_func_enabled := al.DbBaser.Support(db_function)									// can use function for the database
		d_trig_enabled := al.DbBaser.Support(db_trigger) // can use triegger for the database

		prg_sqls := make(map[dbElement][]string)

		//--------------------------------------------------------------------------------------------------
		//--------------------- Функции и триггера for notify event ----------------------------------------
		//--------------------------------------------------------------------------------------------------
		if d_trig_enabled {
			models := []*modelInfo{}

			// список моделей имеющих нотификацию
			for _, mi := range modelCache.allOrdered(al, false) {
				if has, ok := getTableExData(mi.addrField)[DB_table_notifier]; ok && has.(bool) {
					models = append(models, mi)
				}
			}

			if len(models) > 0 {
				switch al.DriverType {
				case DRPostgres:
					{
						schema_public := al.DbBaser.GetSchemaPublic()

						// -------- trigger function -------------------------
						fn_name := models[0].getEventFuncName()

						// Исходник функции в dbo
						pr := &procInfo{
							al:     al,
							dbname: al.Server.DbName,
							schema: schema_public,
							name:   fn_name,
							header: GetFunctionHeader(pgFnTrEvent),
							body:   GetFunctionBody(pgFnTrEvent),
							source: pgFnTrEvent}

						//// проверяем существует ли функция в базе
						//pit, _ := t.prs[pr.dbname][pr.schema][pr.name]
						//
						//// сравниваем описание функции с функцией в базе
						//if ok, msg := t.sql_func_compare(pr, pit); !ok {
						//	// команды обновления триггера
						//	prg_sqls[db_function] = append(prg_sqls[db_function], pr.source)
						//}

						// ищем функцию в списке функций базы
						pit := t.findFunction(pr)
						// формируем скрипт создания/обновления
						prg_sqls[db_function] = append(prg_sqls[db_function], t.dbFuncSqlMaker(pr, pit)...)

						// помечаем в списке фнкций как обработанный
						if pit != nil {
							//delete(t.prs[pi.dbname][pi.schema], pi.name);
							t.prs[pit.pi.al.Server.server()][pit.pi.schema][pit.pi.name].used = true
						}

						// -------- triggers for notify objects -------------------------
						//
						for _, mi := range models {
							// описание триггера для таблицы
							tr := &triggerInfo{
								al:        mi.al,
								dbname:    mi.al.Server.DbName,
								schema:    mi.schema,
								table:     mi.table,
								name:      mi.getEventTriggerName(),
								fn_schema: schema_public,
								fn_name:   fn_name,
								enabled:   true,
								desc:      "event notification trigger"}
							tr.source = t.dbTriggerCreate_AROW_IUD(tr) // CREATE TRIGGER ( ROW )

							ti := t.findTrigger(tr)

							// сравниваем описание триггера с триггером в базе
							if ok, msg := t.dbTriggerCompare(tr, ti); !ok {
								logger.Warnf(msg)
								// команды обновления триггера
								prg_sqls[db_trigger] = append(prg_sqls[db_trigger],
									t.dbTriggerDrop(tr),             // DROP TRIGGER
									tr.source,                       // CREATE TRIGGER ( ROW )
									t.dbTriggerComment(tr, tr.desc)) // COMMENT ON TRIGGER
							}

							// удаляем из списка триггеров обработанный
							if ti != nil {
								delete(t.trs[tr.al.Server.server()][tr.schema][tr.table], tr.name)
							}
						}
					}
				}
			}
		}

		// Update structure
		sqls := []string{}
		sqls = append(sqls, prg_sqls[db_function]...)
		sqls = append(sqls, prg_sqls[db_trigger]...)

		// Check version of database
		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
			continue
		}

		// Exec commands
		t.ExecSQL(al, &sqls, fmt.Sprintf("updating programming replica objects of %s:%s.%s", al.Server.Driver, al.Server.DbName, al.Server.Schema))
	}

	return
}

//func (t *syncDb) dbCreateProgrammReplica() {
//
//	//el := newElapser()	defer el.Show("dbCreateProgrammReplica()")
//	// logger.Infof("orm: testing replicated objects...")
//
//	for _, al := range t.als {
//		t.al = al
//
//		//d_func_enabled := al.DbBaser.Support(db_function)									// can use function for the database
//		d_trig_enabled := al.DbBaser.Support(db_trigger) // can use triegger for the database
//
//		prg_sqls := make(map[dbElement][]string)
//
//		//--------------------------------------------------------------------------------------------------
//		//--------------------- Функции и триггера for replica ---------------------------------------------
//		//--------------------------------------------------------------------------------------------------
//		if d_trig_enabled {
//			models := []*modelInfo{}
//
//			// список моделей имеющих репликацию
//			for _, mi := range modelCache.allOrdered(al, false) {
//				used := true
//				//if val, ok := getTableExData(mi.addrField)[DB_table_replica]; ok {
//				//	used = val.(bool)
//				//}
//
//				if used {
//					models = append(models, mi)
//				}
//			}
//
//			if len(models) == 0 {
//				continue
//			}
//			switch al.DriverType {
//			case DRPostgres:
//				{
//					schema_public := al.DbBaser.GetSchemaPublic()
//
//					// -------- trigger function -------------------------
//					fn_name := models[0].getReplicaFuncName()  // function for replica
//					tb_name := models[0].getReplicaTableName() // table for accumulate replica
//
//					if mr, ok := modelCache.get(tb_name); !ok {
//						panic(fmt.Sprintf("orm: table %q, must be register for replica to bigdata", tb_name))
//					} else {
//						if ok, err := al.DbBaser.TableExists(al.DB, mr.schema, mr.table); err != nil {
//							panic(fmt.Errorf("orm: %v", err))
//						} else if !ok {
//							panic(fmt.Errorf("orm: table %s.%s not found, must exists for replica to bigdata", mr.schema, mr.table))
//						}
//					}
//
//					// Исходник функции в dbo
//					pr := &procInfo{
//						al:     al,
//						dbname: al.Server.DbName,
//						schema: schema_public,
//						name:   fn_name,
//						header: GetFunctionHeader(pg_fn_tr_replica),
//						body:   GetFunctionBody(pg_fn_tr_replica),
//						source: pg_fn_tr_replica}
//
//					// ищем функцию в списке функций базы
//					pit := t.findFunction(pr)
//					// формируем скрипт создания/обновления
//					prg_sqls[db_function] = append(prg_sqls[db_function], t.dbFuncSqlMaker(pr, pit)...)
//
//					// помечаем в списке фнкций как обработанный
//					if pit != nil {
//						//delete(t.prs[pi.dbname][pi.schema], pi.name);
//						t.prs[pit.pi.al.Server.server()][pit.pi.schema][pit.pi.name].used = true
//					}
//
//					// -------- triggers for notify objects -------------------------
//					//
//					for _, mi := range models {
//						// описание триггера для таблицы
//						tr := &triggerInfo{
//							al:        mi.al,
//							dbname:    mi.al.Server.DbName,
//							schema:    mi.schema,
//							table:     mi.table,
//							name:      mi.getReplicaTriggerName(),
//							fn_schema: schema_public,
//							fn_name:   fn_name,
//							enabled:   true,
//							desc:      "replication trigger"}
//						tr.source = t.dbTriggerCreate_AROW_IUD(tr) // CREATE TRIGGER ( ROW )
//
//						ti := t.findTrigger(tr)
//
//						// сравниваем описание триггера с триггером в базе
//						if ok, msg := t.dbTriggerCompare(tr, ti); !ok {
//							logger.Warnf(msg)
//							// команды обновления триггера
//							prg_sqls[db_trigger] = append(prg_sqls[db_trigger],
//								t.dbTriggerDrop(tr), // DROP TRIGGER
//								tr.source,           // CREATE TRIGGER ( ROW )
//								t.dbTriggerComment(tr, tr.desc)) // COMMENT ON TRIGGER
//						}
//
//						// удаляем из списка триггеров обработанный
//						if ti != nil {
//							delete(t.trs[tr.al.Server.server()][tr.schema][tr.table], tr.name)
//						}
//					}
//				}
//			}
//		}
//		// Update structure
//		sqls := []string{}
//		sqls = append(sqls, prg_sqls[db_function]...)
//		sqls = append(sqls, prg_sqls[db_trigger]...)
//
//		// Check version of database
//		if len(sqls) > 0 && t.WrongShemaVersion(al, 0) {
//			continue
//		}
//
//		// Exec commands
//		t.ExecSQL(al, &sqls, fmt.Sprintf("updating programming replica objects of %s:%s.%s", al.Server.Driver, al.Server.DbName, al.Server.Schema))
//	}
//
//	return
//}

func (t *syncDb) getDBProgramObjects() map[*modelInfo][]dbObject {
	modelObjects := make(map[*modelInfo][]dbObject)

	if len(modelCache.cache) == 0 {
		logger.Errorf("no Model found, need register your model")
		return modelObjects
	}

	// Формируем список программных обьектов ДБ
	for _, mi := range modelCache.allOrdered(t.al, false) {
		if mi.model == nil {
			continue
		}

		// functions
		for _, fn_name := range getTableFunction(mi.addrField) {
			object := dbObject{}
			object.Type = db_function
			object.Name = fn_name
			modelObjects[mi] = append(modelObjects[mi], object)
		}

		// triggers
		for _, tri := range getTableTrigger(mi.addrField) {
			object := dbObject{}
			object.Type = db_trigger
			object.Name = tri.Name
			object.Desc = tri.Desc
			modelObjects[mi] = append(modelObjects[mi], object)
		}
	}
	return modelObjects
}

func (t *syncDb) findFunction(pr *procInfo) *pit {
	server := pr.al.Server.server()
	if pit, ok := t.prs[server][pr.schema][pr.name]; ok {
		return pit
	}

	schemas := t.getAliasInnerSchemas(pr.al) // Alias inner schemas( main + inner)
	// если нет, ищем во внкутренних схемах
	for _, schema := range schemas {
		if funcInfos, ok := t.prs[server][schema]; ok {
			if pit, ok := funcInfos[pr.name]; ok {
				return pit
			}
		}
	}

	return nil
}

func (t *syncDb) DropFunctionSQL(pr *procInfo, excludeSchemas ...string) []string {
	sqls := []string{}
	server := pr.al.Server.server()
	schemas := t.getAliasInnerSchemas(pr.al) // Alias inner schemas( main + inner)
	// если нет, ищем во внкутренних схемах
	for _, schema := range schemas {
		if utils.SliceContains(excludeSchemas, schema) {
			continue
		}

		if funcInfos, ok := t.prs[server][schema]; ok {
			if pit, ok := funcInfos[pr.name]; ok {
				sqls = append(sqls, t.dbFuncDrop(pit.pi))
			}
		}
	}
	return sqls
}

// определение лишних функций
func (t *syncDb) dropUnusedFunctions() {

	for _, shemas := range t.prs {
		for _, funcs := range shemas {
			for _, pit := range funcs {
				if !pit.used {
					if si, ok := t.dbs[pit.pi.al.Server.server()][pit.pi.schema]; ok {
						if si.used {
							msgFunctionNotUsed(pit.pi)
						}
					}

					//if si, ok := t.dbs[pit.pi.dbname][pit.pi.schema]; !ok {
					//	msgFunctionIsAlien(pit.pi)
					//} else if !si.skip {
					//	msgFunctionNotUsed(pit.pi)
					//}
				}
			}
		}
	}
}

func (t *syncDb) PrintFunctions() {
	//map[db]map[schema]map[func]*procInfo
	logger.Debugf("------------------------ FUNCTIONS --------------------------------------------")
	i := 0
	for _, shemas := range t.prs {
		for _, funcs := range shemas {
			for _, pit := range funcs {
				i = i + 1
				logger.Debugf("%v: used:%v db:%s.%s func:%s", i, pit.used, pit.pi.dbname, pit.pi.schema, pit.pi.header)
			}
		}
	}
	logger.Debugf("-------------------------------------------------------------------------------")
}

//----------------------------------------------------------------------------------------------------------------------
// FUNCTIONS core
//----------------------------------------------------------------------------------------------------------------------
type procInfo struct {
	al     *alias
	dbname string
	schema string
	name   string
	header string
	body   string
	source string
	desc   string
}

func (t *procInfo) prepareSourceBySchema() {
	// проверяем описание функции
	t.header = GetFunctionHeader(t.source)
	hd := strings.Split(t.header, ".")
	if len(hd) == 1 {
		hd = append(hd, hd[0])
		hd[0] = t.schema
	} else if len(hd) == 2 {
		hd[0] = t.schema
	}
	header := strings.Join(hd, ".")
	t.source = strings.Replace(t.source, t.header, header, 1)
	t.header = GetFunctionHeader(t.source)
}

type pit struct {
	pi   *procInfo
	used bool
}

// зачитка всех функций всех баз
func (t *syncDb) dbReadAllProcs() {
	//el := newElapser()	defer el.Show("dbReadAllProcs()")
	for _, al := range t.als {

		// check if db support triggers
		if !al.DbBaser.Support(db_function) {
			continue
		}

		server := al.Server.server()

		if _, ok := t.prs[server]; ok {
			continue
		}
		t.prs[server] = make(map[string]map[string]*pit)

		query := `SELECT current_database()  as func_db
						 , n.nspname           as func_schema
						 , p.proname           as func_name
						 , format('%I(%s)', p.proname, pg_get_function_arguments(p.oid)) as func_header     
						 , p.prosrc            as func_source
						 , d.description       as func_desc
					  FROM pg_proc p
						   INNER JOIN pg_namespace n ON n.oid = p.pronamespace
						   LEFT  JOIN pg_description d on d.objoid = p.oid
					 WHERE n.nspname not in('pg_catalog','information_schema')
					   AND p.probin is null;`

		rows, err := al.DB.Query(query)
		if err != nil {
			panic(fmt.Errorf("%v [%s]", err, query))
		}
		defer rows.Close()

		for rows.Next() {
			pi := &procInfo{al: al}
			var desc sql.NullString

			err := rows.Scan(&pi.dbname, &pi.schema, &pi.name, &pi.header, &pi.body, &desc)
			if err != nil {
				panic(err.Error())
			}

			if desc.Valid {
				pi.desc = desc.String
			}

			if _, ok := t.prs[server][pi.schema]; !ok {
				t.prs[server][pi.schema] = make(map[string]*pit)
			}

			t.prs[server][pi.schema][pi.name] = &pit{pi: pi}
		}
	}
	return
}

func (t *syncDb) dbFuncSqlMaker(pr *procInfo, pit *pit) (sqls []string) {
	sqls = []string{}

	if pr == nil {
		return
	}
	pr.prepareSourceBySchema()

	// если  нет в базе
	if pit == nil || pit.pi == nil {
		logger.Warnf("orm: function %s.%s.%q not found, will be created....", pr.dbname, pr.schema, pr.name)
		sqls = append(sqls, pr.source)
		return
	}

	// исходный код не совпадает
	if pit.pi.body != pr.body {
		logger.Warnf("orm: function %s.%s.%q souгce is incorrect, will be fixed", pr.dbname, pr.schema, pr.name)

		// обновляем исходник
		sqls = append(sqls, pr.source)

		// удаляем дубликаты из других схем
		sqls = append(sqls, t.DropFunctionSQL(pr, pr.schema)...)
		return
	}

	//  перенесем в нужную схему
	if pit.pi.schema != pr.schema {

		pit.pi.source = pr.source
		pit.pi.prepareSourceBySchema()

		logger.Warnf("orm: schema of function %s.%s.%q is incorrect, will move to schema %s.%s ", pit.pi.dbname, pit.pi.schema, pit.pi.name, pr.dbname, pr.schema)
		sqls = append(sqls, t.dbFuncMove(pit.pi, pr))
	}
	return
}

func (t *syncDb) dbFuncMove(ps *procInfo, pd *procInfo) (sql string) {
	if ps == nil {
		panic(fmt.Errorf("orm: source function info is null"))
	}

	if pd == nil {
		panic(fmt.Errorf("orm: destination function info is null"))
	}

	if t.al.DriverType == DRPostgres {
		return fmt.Sprintf(
			`DO $$
					BEGIN
					  ALTER FUNCTION %s SET SCHEMA %s;
					EXCEPTION
					WHEN duplicate_function THEN
					  RAISE NOTICE '%% %%', SQLSTATE, SQLERRM;
					WHEN others THEN
					  RAISE;
					END $$;`, ps.header, pd.schema)
	}
	return
}

func (t *syncDb) dbFuncDrop(pi *procInfo) (sql string) {
	if pi == nil {
		panic(fmt.Errorf("orm: function info is null"))
	}
	return fmt.Sprintf("DROP FUNCTION IF EXISTS %s.%s;", pi.schema, pi.header)
}

//----------------------------------------------------------------------------------------------------------------------
//-- TRIGGERS
//----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// TRIGGER core
// ---------------------------------------------------------------------------------------------------------------------
// trigger info
type triggerInfo struct {
	al        *alias
	dbname    string
	schema    string
	table     string
	name      string
	fn_schema string
	fn_name   string
	enabled   bool
	source    string
	desc      string
}

// зачитка всех триггеров всех баз
func (t *syncDb) dbReadAllTriggers() {
	//el := newElapser()	defer el.Show("dbReadAllTriggers()")

	for _, t.al = range t.als {
		si := t.getShemaInfo(t.al)
		if si.skip {
			continue
		}

		// check if db support triggers
		if !t.al.DbBaser.Support(db_trigger) {
			continue
		}

		server := t.al.Server.server()
		if _, ok := t.trs[server]; !ok {
			t.trs[server] = make(map[string]map[string]map[string]*triggerInfo)
		}

		schemas := t.getAliasInnerSchemas(t.al) // Alias inner schemas( main + inner)

		query := fmt.Sprintf(
			`SELECT current_database()       as trigger_dbname
						, n.nspname                as trigger_schema
						, c.relname                AS trigger_table
						, t.tgname                 as trigger_name
						, t.tgenabled = 'O'        as trigger_enabled
						, pg_get_triggerdef(t.oid) as trigger_source
						, d.description            as trigger_desc
					 FROM pg_trigger t
						  INNER JOIN pg_class c ON c.oid = t.tgrelid 
						  INNER JOIN pg_namespace n ON n.oid = c.relnamespace
						  LEFT  JOIN pg_description d on d.objoid = t.oid
					WHERE t.tgisinternal = false
					  AND (t.tgname NOT LIKE 'tr_log_%%' OR 
						   t.tgname = 'tr_log_table_au' OR 
						   t.tgname = 'tr_log_detail_biud')         
					  AND n.nspname IN('%s')`, strings.Join(schemas, "','"))

		rows, err := t.al.DB.Query(query)
		if err != nil {
			panic(fmt.Errorf("%v [%s]", err, query))
		}
		defer rows.Close()

		for rows.Next() {
			ti := &triggerInfo{al: t.al}
			var desc sql.NullString

			err := rows.Scan(&ti.dbname, &ti.schema, &ti.table, &ti.name, &ti.enabled, &ti.source, &desc)
			if err != nil {
				panic(err.Error())
			}

			if desc.Valid {
				ti.desc = desc.String
			}

			if _, ok := t.trs[server][ti.schema]; !ok {
				t.trs[server][ti.schema] = make(map[string]map[string]*triggerInfo)
			}

			if _, ok := t.trs[server][ti.schema][ti.table]; !ok {
				t.trs[server][ti.schema][ti.table] = make(map[string]*triggerInfo)
			}

			t.trs[server][ti.schema][ti.table][ti.name] = ti
		}
	}
	return
}

func (t *syncDb) findTrigger(tr *triggerInfo) *triggerInfo {
	server := tr.al.Server.server()

	if ti, ok := t.trs[server][tr.schema][tr.table][tr.name]; ok {
		return ti
	}

	schemas := t.getAliasInnerSchemas(tr.al) // Alias inner schemas( main + inner)
	// если нет, ищем во внкутренних схемах
	for _, schema := range schemas {
		if ti, ok := t.trs[server][schema][tr.table][tr.name]; ok {
			return ti
		}
	}

	return nil
}

// sql trigger drop
func (t *syncDb) dbTriggerDrop(ti *triggerInfo) string {
	return fmt.Sprintf(`DROP TRIGGER IF EXISTS %s ON "%s";`, ti.name, ti.table)
}

// sql trigger AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE...FOR EACH STATEMENT
func (t *syncDb) dbTriggerCreate_ASTM_IUDT(ti *triggerInfo, tr_func_args ...string) string {
	var args string
	if len(tr_func_args) > 0 {
		args = strings.Join(tr_func_args, ",")
	}
	return fmt.Sprintf(`CREATE TRIGGER %s AFTER INSERT OR DELETE OR UPDATE OR TRUNCATE ON %s FOR EACH STATEMENT EXECUTE FUNCTION %s(%s)`,
		ti.name, ti.table, ti.fn_name, args)
}

// sql trigger AFTER INSERT OR UPDATE OR DELETE...FOR EACH ROW
func (t *syncDb) dbTriggerCreate_AROW_IUD(ti *triggerInfo, trigger_args ...string) string {
	var args string
	if len(trigger_args) > 0 {
		args = strings.Join(trigger_args, ",")
	}

	return fmt.Sprintf(`CREATE TRIGGER %s AFTER INSERT OR DELETE OR UPDATE ON %s.%s FOR EACH ROW EXECUTE FUNCTION %s(%s)`,
		ti.name, ti.schema, ti.table, ti.fn_name, args)
}

// sql trigger comment
func (t *syncDb) dbTriggerComment(ti *triggerInfo, comment ...string) string {
	//return fmt.Sprintf("COMMENT ON TRIGGER %s ON %s.%s IS '%s';", ti.name, ti.schema, ti.table, append(comment,"model dbo implementation")[0])
	return fmt.Sprintf(`COMMENT ON TRIGGER %s ON "%s" IS '%s';`, ti.name, ti.table, append(comment, "model dbo implementation")[0])
}

func (t *syncDb) dbTriggerMakerSql(tr, ti *triggerInfo) (sqls []string) {
	sqls = []string{}
	return
}

func unQuoteTriggerOwnerInSource(tr *triggerInfo) string {
	owner := tr.table
	// replace qouted table name in trigger source
	return strings.Replace(tr.source, fmt.Sprintf(` "%s" `, owner), fmt.Sprintf(` %s `, owner), 1)
}

// rigger compare tr-dbo ti-db
func (t *syncDb) dbTriggerCompare(tr, ti *triggerInfo) (ok bool, msg string) {

	// в нет
	if ti == nil {
		return false, fmt.Sprintf("orm: table %s.%s.%s, trigger %s not found", tr.dbname, tr.schema, tr.table, tr.name)
	}

	// в бд - не активный
	if !ti.enabled {
		return false, fmt.Sprintf("orm: table %s.%s.%s, trigger %s is inactive, will be fixed", tr.dbname, tr.schema, tr.table, tr.name)
	}

	// исходный код не совпадает
	if ti.source != tr.source {
		tr_src := unQuoteTriggerOwnerInSource(tr) // снимаем кавычки с имени таблицы
		if ti.source != tr_src {
			logger.Debugf("\ntr_source:%v\ndb_source:%v", tr.source, ti.source)

			msg = fmt.Sprintf("orm: table %s.%s.%s, trigger %s souгce is incorrect, will be fixed", tr.dbname, tr.schema, tr.table, tr.name)
			return false, msg
		}
	}

	// описание не соответствует
	if ti.desc != tr.desc {
		return false, fmt.Sprintf("orm: %s.%s.%s, trigger %s description is incorrect, will be fixed", tr.dbname, tr.schema, tr.table, tr.name)
	}
	return true, ""
}

// определение лишних триггеров
func (t *syncDb) dropUnusedTriggersSQL() {

	//el := newElapser()	defer el.Show("dropUnusedTriggersSQL()")
	// [DBServer][schema][table][trigger]*triggerInfo
	for server, schemaTables := range t.trs {
		for schema, tables := range schemaTables {

			si, ok := t.dbs[server][schema]
			if !ok || !si.used {
				continue
			}

			sqls := []string{}

			for _, triggerInfos := range tables {
				for _, ti := range triggerInfos {
					//if st, ok := t.dbs[ti.al.Server.server()][ti.schema]; ok && st.used {

					mi, ok := modelCache.get(ti.table)
					// чужая таблица
					if !ok {
						continue
					}
					// таблица имеет ручной скрипт миграции - за структуру отвечает скрипт миграции
					if getTableIsExtMigration(mi.addrField) {
						continue
					}

					msgTriggerNotUsed(ti)
					sqls = append(sqls, t.dbTriggerDrop(ti)) // DROP TRIGGER
					//}
				}
			}

			if len(sqls) == 0 {
				continue
			}

			// Check version of database
			if len(sqls) > 0 && t.WrongShemaVersion(si.al, 0) {
				continue
			}

			// Exec commands
			t.ExecSQL(si.al, &sqls, fmt.Sprintf("dropping unused triggers of %s:%s.%s", si.al.Server.Driver, si.al.Server.DbName, si.al.Server.Schema))

		}
	}
}

func (t *syncDb) PrintTriggers() {
	// [db][schema][table][trigger]*procInfo
	logger.Debugf("--------------------- TRIGGERS ------------------------------------------------")
	for _, dbs := range t.trs {
		for _, schemas := range dbs {
			for _, triggers := range schemas {
				for _, ti := range triggers {
					logger.Debugf("* db:%s.%s.%s.%q", ti.dbname, ti.schema, ti.table, ti.name)
				}
			}
		}
	}
	logger.Debugf("-------------------------------------------------------------------------------")
}
