package orm

import (
	"fmt"
)

var SyncDB = &syncDB{}

type syncDB struct {
	dbo           string
	externalLinks []*DBLink
	replicaServer *DBServer
}

func (t *syncDB) WithDboData(value string) *syncDB {
	t.dbo = value
	return t
}

func (t *syncDB) WithExternalLinks(value []*DBLink) *syncDB {
	t.externalLinks = value
	return t
}

func (t *syncDB) WithReplica(value *DBServer) *syncDB {
	t.replicaServer = value
	return t
}

// Sync app modules...
//func (t *syncDB) SyncAppModelsWithDB(dbo_file string, externalLinks []*DBLink, replicaServer *DBServer) (err error) {
func (t *syncDB) Do() (err error) {
	defer recoverPanic(t.Do, &err)
	BootStrap() // fix models' structure

	logger.Infof("orm: --- testing structure of used databases ---")

	this := newSyncDb()
	this.dbo = newDbo(&t.dbo) // load dbo metadata file

	// replica server
	if t.replicaServer != nil {
		this.repl, _ = dataBaseCache.get(t.replicaServer.Alias) // replica alias
		this.repl_prefix = fmt.Sprintf("%s_", this.repl.Server.Schema)
	}

	//------------------------------------------------------------------------------------------------------------------
	// DO syncro
	//------------------------------------------------------------------------------------------------------------------
	el := newElapser() //defer el.Show("SyncDB()")

	this.initSyncInfo() // Load registered db server's aliases && make list of working aliases

	//this.PrintShemas()
	//this.PrintRelations()
	//------------------------
	this.dbRunStartSQL() // testing the structure of databases - выполняются стартовые скрипты

	this.dbCreateStructureByModels() // testing the structure of databases - создаются таблицы + индексы схемы используемых баз
	//this.dbCreateReplicaByModels()   //
	//this.dbCreateExternalLinks(t.externalLinks...) // Links - создаются связи между базами, указанные в external_db.ini
	//this.dbCreateDynamicLinks()                    // создаются схемы и линки на таблицы внешних баз
	this.dbDropUnusedModels() // очищаются параметры таблиц в неиспользуемых схемах

	//this.PrintShemas()
	//-------------------------
	this.dbReadAllProcs()    // зачитка всех функций всех баз
	this.dbReadAllTriggers() // зачитка всех триггеров всех баз
	//this.PrintFunctions()
	//this.PrintTriggers()

	this.dbCreateProgrammObjects() // создаются програмные объекты базы данных на основе моделей
	this.dbCreateProgrammNotify()  // создаются програмные объекты базы данных на основе моделей
	//this.dbCreateProgrammReplica() // создаются програмные объекты базы данных на основе моделей

	this.dropUnusedTriggersSQL() // удаление лишних триггеров
	//this.dropUnusedFunctions()					// удаление лишних функций
	//------------------------
	this.dbRunTestSQL()           // выполняются тестовые скрипты для данных на основе моделей
	this.dbRunFinishSQL()         // testing the structure of databases - выполняются заключительный скрипт
	this.dbUpdateUserSearchPath() // обновление путей поиска пользователя
	this.dbUpdateVersion()        // обновление версий баз данных
	//------------------------------------------------------------------------------------------------------------------

	logger.Infof("orm: --- end test structure of used databases(%v) --", el.Duration())
	return nil
}

//------------------------------
// Выполнить запросы в базу.
//------------------------------

func (t *syncDb) ExecSQL(al *alias, sqls *[]string, msg string, onlyShow ...bool) {
	size := len(*sqls)
	if size == 0 {
		return
	}
	logger.Infof("orm: %v %d command(s)...", msg, size)
	show := append(onlyShow, false)[0]
	for _, sql := range *sqls {
		if show {
			logger.Debugf(sql)
		} else { // ignore
			if _, err := al.DB.Exec(sql); err != nil {
				err = fmt.Errorf("orm: %s:%s <DB.Exec()> ERROR:%v [%v]", al.Server.Driver, al.Server.DbName, err, sql)
				getDbSchemaSearchPathSQL(al)
				logger.Errorf("%s", err.Error())
				panic(err.Error())
			}
		}
	}

}

//------------------------------
// Проверка существования моделей по имени структуры
//------------------------------

func TestGetModel(modelNameLists ...[]string) (err error) {
	defer recoverPanic(TestGetModel, &err)

	for _, modelNameList := range modelNameLists {
		for _, modelName := range modelNameList {
			logger.Infof("Testing '%s' model...", modelName)
			GetModel(modelName)
		}
	}
	return nil
}
