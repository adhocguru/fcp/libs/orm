package orm

import (
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
// table core
// ---------------------------------------------------------------------------------------------------------------------
func (t *syncDb) findTable(dbTables map[string]*tableInfo, mi *modelInfo, isReplica bool) *tableInfo {

	if ti, ok := dbTables[mi.getFullTableName()]; ok {
		return ti
	}

	// если нет, ищем во внутренних схемах
	schemas := t.getAliasInnerSchemas(mi.al) // Alias inner schemas( main + inner)
	// реплика не использует "public" схему
	if isReplica {
		publicPath := mi.al.DbBaser.GetSchemaPublic() // схема public
		utils.SliceDelete(&schemas, publicPath)
	}

	for _, schema := range schemas {
		fullName := fmt.Sprintf("%s.%s", schema, mi.table)
		if ti, ok := dbTables[fullName]; ok {
			return ti
		}
	}
	return nil
}

func (t *syncDb) dbTableMove(ti *tableInfo, md *modelInfo) []string {

	if ti == nil {
		panic(fmt.Errorf("orm: source model is null"))
	}

	if md == nil {
		panic(fmt.Errorf("orm: destination model is null"))
	}

	var sqls []string
	switch md.al.DriverType {
	case DRPostgres, DRVertica:
		sqls = append(sqls, fmt.Sprintf("DROP TABLE IF EXISTS %s.%s;", md.schema, md.table))
		sqls = append(sqls, fmt.Sprintf("ALTER TABLE %s.%s SET SCHEMA %s;", ti.schema, ti.table, md.schema))
	}
	return sqls
}

func (t *syncDb) dbTableColumnAdd(fi *fieldInfo) string {
	Q := t.al.DbBaser.TableQuote()
	S := fmt.Sprintf("%s%s%s.", Q, fi.mi.schema, Q)
	if fi.mi.schema == "" {
		S = ""
	}
	typ := getColumnTyp(fi.mi.al, fi)

	if !fi.null {
		typ += " " + "NOT NULL"
	}

	sql := fmt.Sprintf("ALTER TABLE %s%s%s%s ADD COLUMN %s%s%s %s %s",
		S, Q, fi.mi.table, Q,
		Q, fi.column, Q,
		typ,
		getColumnDefault(fi))

	if strings.Contains(sql, "%COL%") {
		sql = strings.Replace(sql, "%COL%", fi.column, -1)
	}
	return sql
}

func (t *syncDb) dbTableColumnDel(mi *modelInfo, column string) string {
	Q := mi.al.DbBaser.TableQuote()
	S := fmt.Sprintf("%s%s%s.", Q, mi.schema, Q)
	if mi.schema == "" {
		S = ""
	}

	return fmt.Sprintf("ALTER TABLE %s%s%s%s DROP COLUMN IF EXISTS %s%s%s;",
		S, Q, mi.table, Q,
		Q, column, Q)
}

func (t *syncDb) dbTableColumnDelNotnull(mi *modelInfo, column string) string {
	Q := mi.al.DbBaser.TableQuote()
	return fmt.Sprintf("ALTER TABLE %s.%s ALTER COLUMN %s%s%s DROP NOT NULL;", mi.schema, mi.table, Q, column, Q)
}
