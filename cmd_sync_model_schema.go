package orm

import (
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"strconv"
	"strings"
)

func (t *syncDb) getShemaInfo(al *alias) *schemaInfo {
	if si, ok := t.dbs[al.Server.server()][al.Server.Schema]; ok {
		return si
	}
	panic(fmt.Errorf("orm: schema information %s:%s.%s not found", al.Server.Driver, al.Server.DbName, al.Server.Schema))
}

// зачитка всех схем всех баз [db][schema]*schemaInfo
func (t *syncDb) getSchemas(al *alias) []string {

	sqls := []string{}
	t.getDBSchemas() // собирем информацию о схемах в БД

	if _, ok := t.usr[al]; !ok {
		t.usr[al] = &userPath{db: []string{}, md: []string{}, wk: []string{}}
	}

	// читаем схемы из моделей и создаем если нужно
	schemas, cmds := t.getMDSchemas(al)
	sqls = append(sqls, cmds...)

	t.usr[al].md = schemas // t.usr[*alias].md - пути поиска рассчитанные по моделям

	// получим текущий путь поиска
	if db_user_path, err := al.DbBaser.GetUserSearchPath(al.DB, al.Server.User); err != nil {
		panic(err)
	} else {
		t.usr[al].db = strings.Split(db_user_path, ",") // t.usr[*alias].db - пути поиска в базе
		// убираем пробелы из схем
		for i, path := range t.usr[al].db {
			t.usr[al].db[i] = strings.TrimSpace(path)
		}
	}

	// просматриваем текущие схемы поска в базе и добавляем в рабочие отсутствующие
	db_user_pats := t.usr[al].db // пути поиска в базе
	//public_path  := al.DbBaser.GetSchemaPublic()
	for _, path := range db_user_pats {
		if path == "" {
			continue
		}

		//if path == public_path {
		//	continue
		//}

		if al.DriverType == DRVertica && strings.HasPrefix(strings.ToLower(path), "v_") {
			continue
		}

		if !utils.SliceContains(schemas, path) {
			schemas = append(schemas, path)
		}
	}
	t.usr[al].wk = schemas // t.usr[*alias].wk - пути поиска при синхронизации

	// просматриваем рабочие схемы и сравниваем с теми что в базе
	for _, schema := range schemas {
		if !utils.SliceContains(db_user_pats, schema) {
			// нужно обновить пути поиска в базе
			cmds = al.DbBaser.SetUserSearchPathQuery(al.Server.User, schemas...)
			sqls = append(sqls, cmds...)
			break
		}
	}

	return sqls
}

func (t *syncDb) getDBSchemas() {
	server := t.al.Server.server() // сервер на хосте

	// проверка на обработанный
	if _, ok := t.dbs[server]; ok {
		return
	}

	// инициализируем хранилище
	t.dbs[server] = make(map[string]*schemaInfo)

	// читам схемы из базы
	sis, err := t.al.DbBaser.GetShemasInfo(t.al.DB)
	if err != nil {
		panic(err)
	}

	schema_public := t.al.DbBaser.GetSchemaPublic()

	for _, si := range sis {
		si.used = si.schema == schema_public // schema public используется в системе
		si.skip = true
		si.al = t.al // owner alias

		fal := t.al.findServer(si.al.Server.Driver, si.al.Server.DbName, si.schema)
		if fal != nil {
			si.al = fal
			si.used = true // schema ипользуется в системе
			//----------------------------------------------------------------------------
			// заполняем информацию о схеме
			//----------------------------------------------------------------------------
			// Читаем локальную версию
			ver, err := t.dbo.find(fal.Name, tagVer)
			if err != nil {
				panic(err)
			}
			si.dbo_ver = Str2Version(strings.TrimSpace(*ver)) // версия dbo
			si.db_ver = Str2Version(si.desc)                  // версия БД
			si.skip = si.dbo_ver < si.db_ver                  // schema пропускается для тестирования
			//----------------------------------------------------------------------------
		}

		server = si.al.Server.server()
		if _, ok := t.dbs[server]; !ok {
			t.dbs[server] = make(map[string]*schemaInfo)
		}
		t.dbs[server][si.schema] = si

	}
	return
}

// test schemas by Alias && make create SQL if need
func (t *syncDb) getMDSchemas(al *alias) ([]string, []string) {

	schemas := []string{}
	sqls := []string{}

	// current schema
	schemas = append(schemas, al.Server.Schema) // main schema
	t.testShema(al, al.Server.Schema, &sqls)    // test && cmd create if need

	// add relations schemas
	locAliases := t.relations[al.Server]
	for _, aliasSchemas := range locAliases {
		for al, schemaModels := range aliasSchemas {
			for schema := range schemaModels {
				schemas = append(schemas, schema) // relation schema
				t.testShema(al, schema, &sqls)    // test && cmd create if need
			}
		}
	}

	// replica shemas
	// если есть реплика и текущий алиас как раз он и есть
	if t.repl != nil && t.repl == al {
		// просматриваем все алиасы
		for _, alr := range t.als {
			// самого себя пропускаем
			if alr == t.repl {
				continue
			}
			// RUM{ по моделям нельзя, т.к. длина search_path MAX 128 }
			schema := fmt.Sprintf("%s%s", t.repl_prefix, alr.Server.Schema)
			// Добавляем схему в список схем
			if !utils.SliceContains(schemas, schema) {
				schemas = append(schemas, schema) // replica schema
				t.testShema(alr, schema, &sqls)   // test && cmd create if need
			}

		}
	}
	return schemas, sqls
}

func (t *syncDb) testShema(rel_al *alias, schema string, sqls *[]string) {

	server := t.al.Server.server() // сервер на хосте
	if _, ok := t.dbs[server]; !ok {
		t.dbs[server] = make(map[string]*schemaInfo)
	}

	if si, ok := t.dbs[server][schema]; ok {
		if !si.used {
			si.used = true
			si.al = rel_al
		}
		return
	}

	//----------------------------------------------------------------------------
	// заполняем информацию о схеме
	//----------------------------------------------------------------------------
	si := &schemaInfo{}
	si.al = t.al
	si.dbname = t.al.Server.DbName
	si.schema = schema
	si.used = true
	si.skip = true

	//if si.dbname == rel_al.Server.DbName {
	if server == rel_al.Server.server() {
		// Читаем локальную версию
		ver, err := t.dbo.find(rel_al.Name, tagVer)
		if err != nil {
			panic(err)
		}
		si.dbo_ver = Str2Version(strings.TrimSpace(*ver)) // версия dbo
		si.db_ver = Str2Version(si.desc)                  // версия БД
		si.skip = si.dbo_ver < si.db_ver                  // schema пропускается
	}
	t.dbs[server][si.schema] = si
	*sqls = append(*sqls, si.al.DbBaser.ShemaCreateQuery(schema))
	*sqls = append(*sqls, si.al.DbBaser.SetShemaGrantQuery(schema, t.al.Server.User)...)
	return
}

// возвращает список внутренних схем алиаса
func (t *syncDb) getAliasInnerSchemas(al *alias) []string {
	schemas := t.usr[al].wk // [2] пути поиска при синхронизации
	return schemas
}

// возвращает список внутренних схем алиаса
func (t *syncDb) getUnusedSchemas(server DBServer) []string {
	schemas := []string{}
	if schemaInfos, ok := t.dbs[server]; ok {
		for _, si := range schemaInfos {
			if !si.used {
				schemas = append(schemas, si.schema) // relation schema
			}
		}
	}
	return schemas
}

func (t *syncDb) panicIfShemaVersionNotUpdated(al *alias) {
	si := t.getShemaInfo(al)
	if si.dbo_ver <= si.db_ver {
		panic(fmt.Errorf("orm: value of version [%s.version](%s) in `dbo-file` data must be greater than in the database(%s) when change the structure",
			al.Name,
			strconv.FormatFloat(si.dbo_ver, 'f', 2, 64),
			strconv.FormatFloat(si.db_ver, 'f', 2, 64)))
	}
}

func (t *syncDb) WrongShemaVersion(al *alias, _ /*level*/ int) bool {

	si := t.getShemaInfo(al)
	if si.skip {
		msg := fmt.Sprintf("orm: version of %s.%s[%v] is more than in dbo-file [%s.version]%v, skipped",
			al.info(), al.Server.Schema, strconv.FormatFloat(si.db_ver, 'f', 2, 64),
			al.Name, strconv.FormatFloat(si.dbo_ver, 'f', 2, 64))
		logger.Errorf("%s", msg)
		return true
	}
	return false
}

func (t *syncDb) PrintShemas(ids ...int) {
	id := append(ids, 0)[0]

	logger.Debugf("-------------------- SCHEMAS %d--------------------------------------------------", id)
	i := 0
	for _, schemas := range t.dbs {
		if i > 0 {
			logger.Debugf("")
		}
		for _, si := range schemas {
			i = i + 1
			logger.Debugf("%2d) %-20s %s.%-25s %-8.0f %-8.0f used:%-6v skip:%-6v %v alias:%q", i, si.al.Server.Driver, si.dbname, si.schema, si.dbo_ver, si.db_ver, si.used, si.skip, si.desc, si.al.Name)
		}
	}

	logger.Debugf("............. MAIN SHEMAS ..................................................................")
	i = 0
	for _, t.al = range t.als {
		i = i + 1
		si := t.getShemaInfo(t.al)
		logger.Debugf("%2d) %-20s %s.%-25s %-8.0f %-8.0f used:%-6v skip:%-6v %v alias:%q", i, si.al.Server.Driver, si.dbname, si.schema, si.dbo_ver, si.db_ver, si.used, si.skip, si.desc, si.al.Name)
	}

	logger.Debugf("............. USED SHEMAS ..................................................................")
	i = 0
	for _, schemas := range t.dbs {
		if i > 0 {
			logger.Debugf("")
		}
		for _, si := range schemas {
			if si.used {
				i = i + 1
				logger.Debugf("%2d) %-20s %s.%-25s %-8.0f %-8.0f used:%-6v skip:%-6v %v alias:%q", i, si.al.Server.Driver, si.dbname, si.schema, si.dbo_ver, si.db_ver, si.used, si.skip, si.desc, si.al.Name)
			}
		}
	}

	logger.Debugf("............. NOT USED SHEMAS ..................................................................")
	i = 0
	for _, schemas := range t.dbs {
		if i > 0 {
			logger.Debugf("")
		}
		for _, si := range schemas {
			if !si.used {
				i = i + 1
				logger.Debugf("%2d) %-20s %s.%-25s %-8.0f %-8.0f used:%-6v skip:%-6v %v alias:%q", i, si.al.Server.Driver, si.dbname, si.schema, si.dbo_ver, si.db_ver, si.used, si.skip, si.desc, si.al.Name)
			}
		}
	}

	logger.Debugf("-------------------------------------------------------------------------------")
}
