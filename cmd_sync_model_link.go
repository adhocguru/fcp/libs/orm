package orm

//func (t *syncDb) dbCreateExternalLinks(extLinks ...*DBLink) {
//	if len(extLinks) == 0 {
//		return
//	}
//	//el := newElapser() defer el.Show("dbCreateExternalLinks()")
//	logger.Infof("orm: testing static links between used databases...")
//	for _, extLink := range extLinks {
//
//		err := RegisterDBLink(extLink)
//		if err != nil {
//			panic(fmt.Errorf("orm: %v", err))
//		}
//
//		server := extLink.srvOwner.server() // сервер на хосте
//
//		if _, ok := t.dbs[server]; !ok {
//			t.dbs[server] = make(map[string]*schemaInfo)
//		}
//
//		if si, ok := t.dbs[server][extLink.Alias]; !ok {
//			//----------------------------------------------------------------------------
//			// заполняем информацию о схеме
//			//----------------------------------------------------------------------------
//			si = &schemaInfo{}
//			si.al = getDbAlias(extLink.srvOwner.Alias)
//			si.dbname = server.DbName
//			si.schema = extLink.Alias
//			si.used = true
//			si.skip = true
//
//			t.dbs[server][si.schema] = si
//		} else {
//			si.used = true // отмечаем как используемый в системе
//		}
//	}
//	return
//}

//func (t *syncDb) dbCreateDynamicLinks() {
//	//el := newElapser() defer el.Show("dbCreateDynamicLinks()")
//	//logger.Infof("orm: testing dynamic links between used databases...")
//	var err error
//
//	for _, al := range t.als {
//
//		t.al = al
//
//		// Check version of database
//		if t.WrongShemaVersion(al, 0) {
//			continue
//		}
//
//		// TODO: multiple registry procedure? slice of DB? don't known
//		// читаем ссылки на внешние базы и формируем линки
//		if aliasShemas, ok := t.relations[t.al.Server][locRemote]; ok {
//			for al, schemaModels := range aliasShemas { // range relations
//				for schema, models := range schemaModels {
//
//					relTables := []string{}
//					// список external таблиц алиаса которые линкуются
//					for _, mi := range models {
//						relTables = append(relTables, mi.table)
//					}
//
//					logger.Infof("orm: testing dynamic link %s:%s.%s --> %s:%s.%s(%s)",
//						t.al.Server.Driver, t.al.Server.DbName, t.al.Server.Schema,
//						al.Server.Driver, al.Server.DbName, schema /*al.Server.Schema*/, strings.Join(relTables, ","))
//
//					// Crete owner server info
//					ownerServer := *t.al.Server
//					ownerServer.Alias = fmt.Sprintf("%s.%s", ownerServer.DbName, schema /*al.Server.Schema*/)
//					ownerServer.Schema = schema /*al.Server.Schema*/
//					// Register remote server && databaase
//					DBServers.Add(&ownerServer)
//					defer DBServers.Delete(&ownerServer)
//					// Register remote database
//					RegisterDatabase(&ownerServer, 1, 1)
//					defer UnRegisterDataBase(ownerServer.Alias)
//
//					// Crete remote server info
//					remoteServer := *al.Server
//					remoteServer.Alias = fmt.Sprintf("%s.%s", remoteServer.DbName, schema /*al.Server.Schema*/)
//					remoteServer.Schema = schema /*al.Server.Schema*/
//					// Register remote server && databaase
//					DBServers.Add(&remoteServer)
//					defer DBServers.Delete(&remoteServer)
//					// Register remote database
//					RegisterDatabase(&remoteServer, 1, 1)
//					defer UnRegisterDataBase(remoteServer.Alias)
//
//					// Crete db link to remote database
//					dbLink := &DBLink{
//						Alias:   fmt.Sprintf("%s", schema /*al.Server.Schema*/),
//						Owner:   ownerServer.Alias,
//						Remote:  remoteServer.Alias,
//						Objects: strings.Join(relTables, ","),
//					}
//
//					err = RegisterDBLink(dbLink)
//					defer UnRegisterDBLink(dbLink)
//
//					if err != nil {
//						panic(err.Error())
//					}
//				}
//			}
//		}
//	}
//	return
//}
