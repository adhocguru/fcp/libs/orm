package orm

import (
	"database/sql"
	"errors"
	"fmt"
	"gitlab.com/adhocguru/fcp/libs/utils"
	"reflect"
	"strconv"
	"strings"
	"time"
)

func (d *dbBase) agregateColumn(q dbQuerier, qs *querySet, mi *modelInfo, cond *Condition, tz *time.Location, agr_method string, agr_column string) (res string, err error) {

	if len(agr_column) == 0 {
		return "", errors.New("no aggregate field/column specified.")
	}

	var cmd string
	var cmd_distinct string

	//defer func(){
	//	if err == nil {
	//     res = &num
	//	}
	//}()

	qs.distinct = false

	Q := d.ins.TableQuote()

	tables := newDbTables(mi, d.ins)
	tables.parseRelated(qs.related, qs.relDepth)

	// detect field name with query alias
	join_index, fi, ok := tables.getColumnJoinIndex(agr_column)
	if !ok {
		return "", fmt.Errorf("unknown field/column name `%s`", agr_column)
	}

	if agr_method == Agregate_Sum || agr_method == Agregate_Avg {
		switch fi.fieldType {
		case TypeBooleanField, TypeCharField, TypeTextField, TypeTimeField, TypeDateField, TypeDateTimeField, TypeJSONField, TypeJsonbField:
			return "", errors.New(fmt.Sprintf("not supported method `%s` for column `%s`", agr_method, agr_column))

		}
	}

	if agr_method == Agregate_Min || agr_method == Agregate_Max {
		switch fi.fieldType {
		case TypeBooleanField, TypeCharField, TypeTextField, TypeJSONField, TypeJsonbField:
			return "", errors.New(fmt.Sprintf("not supported method `%s` for column `%s`", agr_method, agr_column))

		}
	}

	column_sql := fmt.Sprintf("%s.%s%s%s", join_index, Q, fi.column, Q)
	column_alias := fmt.Sprintf("%s%s%s", Q, fi.column, Q)

	join := tables.getJoinSQL()
	//where, args := tables.getCondSQL(cond, false, tz) // not work correctly
	where, args := tables.getCondSQL1(cond, false, tz)
	//groupBy     := tables.getGroupSQL(qs.groups)
	groupBy := ""
	//tables.getOrderSQL(qs.orders)

	exWhereOperator := "AND"
	if cond == nil {
		exWhereOperator = "WHERE"
	}

	switch agr_method {
	case Agregate_Count_All: // кол-во всех записей
		cnt, _ := d.Count(q, qs, mi, cond, tz)
		res = strconv.FormatInt(cnt, 10)
		return
	case Agregate_Count_Unique: // кол-во уникальных записей
		cmd = "COUNT"
		//qs.distinct = true
		cmd_distinct = "DISTINCT"
	case Agregate_Count_Filled: // кол-во заполненных записей
		cmd = "COUNT"
		column_sql = "*"
		where += fmt.Sprintf(" %s %s.%s%s%s IS NOT NULL", exWhereOperator, join_index, Q, fi.column, Q)
	case Agregate_Count_Empty: // кол-во пустых записей
		cmd = "COUNT"
		column_sql = "*"
		where += fmt.Sprintf(" %s %s.%s%s%s IS NULL", exWhereOperator, join_index, Q, fi.column, Q)
	case Agregate_Sum: // сумма по колонке
		cmd = "SUM"
	case Agregate_Min: // минимальное значение по колонке
		cmd = "MIN"
	case Agregate_Max: // максимальное значение по колонке
		cmd = "MAX"
	case Agregate_Avg: // среднее по колонке
		cmd = "AVG"
	default:
		return "", errors.New("Unprocessable agregate method: `" + agr_method + "`")
	}

	query := fmt.Sprintf("SELECT %s(%s %s) as %s FROM %s%s%s T0 %s%s%s", cmd, cmd_distinct, column_sql, column_alias, Q, mi.table, Q, join, where, groupBy)

	if groupBy != "" {
		query = fmt.Sprintf("SELECT %s(%s) as %s FROM (%s) AS %s", cmd, column_sql, column_alias, query, join_index)
	}
	d.ins.ReplaceMarks(&query)

	//row := q.QueryRow(query, args...)
	//err = row.Scan(&res)

	rs, err := q.Query(query, args...)
	if err != nil {
		return "", err
	}
	defer rs.Close()

	refs := []interface{}{new(interface{})}

	for rs.Next() {
		if err := rs.Scan(refs...); err != nil {
			return "", err
		}
	}

	val := reflect.Indirect(reflect.ValueOf(refs[0])).Interface()
	value, err := d.convertValueFromDB(fi, val, tz)

	if err != nil {
		return "", err
	}

	if value == nil {
		switch fi.fieldType {
		case TypeVarCharField, TypeCharField, TypeTextField:
			value = ""
		case TypeDateField, TypeDateTimeField:
			value = time.Time{}
		case TypeTimeField:
			value = "00:00"
		case TypeBitField, TypeSmallIntegerField, TypeIntegerField,
			TypeBigIntegerField, TypePositiveBitField, TypePositiveSmallIntegerField,
			TypePositiveIntegerField, TypePositiveBigIntegerField, TypeFloatField,
			TypeDecimalField:
			value = 0
		case TypeBooleanField:
			value = false
		case TypeJSONField, TypeJsonbField:
			value = "{}"
		case RelForeignKey, RelOneToOne, RelManyToMany, RelReverseOne, RelReverseMany:
			value = 0
		}
	}

	return fmt.Sprintf("%v", value), err
}

type groupAggregateItem struct {
	Group interface{} `json:"group"`
	Value interface{} `json:"value"`
}

type GroupAggregate struct {
	GroupName string                `json:"group"`     // Group name
	GroupType string                `json:"groupType"` // Group data type
	ValueName string                `json:"value"`     // Value name
	ValueType string                `json:"valueType"` // Value data type
	Count     int                   `json:"count"`     // Datas count
	Data      []*groupAggregateItem `json:"data"`      // Datas
}

var casts = []string{"date"}

func (d *dbBase) agregateColumnGroup(q dbQuerier, qs *querySet, mi *modelInfo, cond *Condition, tz *time.Location, agr_method, column_agreg string) (inter *GroupAggregate, err error) {
	inter = nil

	if len(column_agreg) == 0 {
		err = errors.New("no aggregate column specified.")
		return
	}

	if len(qs.groups) == 0 {
		err = errors.New("no GROUP BY column specified.")
		return
	}

	// Using only 1-st group parameter
	groups_arg := qs.groups[0]
	groups_arg, err = replaceDoubleSpace(groups_arg) // remove double space
	group_args := strings.Split(groups_arg, " ")
	column_cast := ""
	if len(group_args) > 1 {
		if len(group_args) < 3 {
			err = errors.New(fmt.Sprintf("GROUP BY parameters %q contains less than 3 words", groups_arg))
		}
		if strings.ToLower(group_args[1]) != "as" {
			err = errors.New(fmt.Sprintf("parameter %q is invalid.", group_args[1]))
			return
		}
		column_cast = group_args[2]
		if !utils.SliceContains(casts, strings.ToLower(column_cast)) {
			err = errors.New(fmt.Sprintf("GROUP BY parameters %q contains unknown tag %q", groups_arg, column_cast))
			return
		}
		column_cast = strings.ToLower(column_cast)
	}
	column_group := group_args[0]

	// Make related model
	models := make(map[string]bool)
	// ...from group field...
	model := strings.Split(column_group, ExprSep)
	model = model[:len(model)-1]
	models[strings.Join(model, ExprSep)] = true
	// ...from aggregate field...
	model = strings.Split(column_agreg, ExprSep)
	model = model[:len(model)-1]
	models[strings.Join(model, ExprSep)] = true

	for model := range models {
		if model == "" {
			continue
		}
		if !utils.SliceContains(qs.related, model) {
			qs.related = append(qs.related, model)
		}
	}

	tables := newDbTables(mi, d.ins)
	tables.parseRelated(qs.related, qs.relDepth)

	// detect field name with query alias
	fi_alias, fi, ok := tables.getColumnJoinIndex(column_agreg)
	if !ok {
		err = fmt.Errorf("unknown column name `%s`", column_agreg)
		return
	}

	//detect group field name with query alias
	fg_alias, fg, ok := tables.getColumnJoinIndex(column_group)
	if !ok {
		err = fmt.Errorf("unknown column name `%s`", column_group)
		return
	}

	if agr_method == Agregate_Sum || agr_method == Agregate_Avg {
		switch fi.fieldType {
		case TypeBooleanField, TypeCharField, TypeTextField, TypeTimeField, TypeDateField, TypeDateTimeField, TypeJSONField, TypeJsonbField:
			err = errors.New(fmt.Sprintf("not supported method `%s` for column `%s`", agr_method, column_agreg))
			return
		}
	}

	if agr_method == Agregate_Min || agr_method == Agregate_Max {
		switch fi.fieldType {
		case TypeBooleanField, TypeCharField, TypeTextField, TypeJSONField, TypeJsonbField:
			err = errors.New(fmt.Sprintf("not supported method `%s` for column `%s`", agr_method, column_agreg))
			return
		}
	}

	qs.distinct = false
	Q := "" //d.ins.TableQuote()

	// Make group column
	col_group_al := fmt.Sprintf("%s%s%s", Q, fg.column, Q)
	col_group := fmt.Sprintf("%s.%s%s%s", fg_alias, Q, fg.column, Q)
	if column_cast != "" {
		switch fg.fieldType {
		case TypeDateField, TypeDateTimeField:
			switch column_cast {
			case "date":
				col_group = fmt.Sprintf("cast(%s as date)", col_group)
			}
		default:

		}
	}

	// Make aggregate column
	col_agregate_al := fmt.Sprintf("%s%s%s", Q, fi.column, Q)
	col_agregate := fmt.Sprintf("%s.%s%s%s", fi_alias, Q, fi.column, Q)

	from := fmt.Sprintf("%s%s%s as T0", Q, mi.table, Q)
	join := tables.getJoinSQL()
	where, args := tables.getCondSQL(cond, false, tz) // not work correctly
	groupBy := fmt.Sprintf("GROUP BY %s", col_group)
	orderBy := fmt.Sprintf("ORDER BY %s", col_group) //tables.getOrderSQL(qs.orders)

	exWhereOperator := "AND"
	if cond == nil {
		exWhereOperator = "WHERE"
	}

	var fmt_agregate string
	//var cmd_distinct string

	switch agr_method {
	case Agregate_Count_All: // кол-во всех записей
		fmt_agregate = "COUNT(%s)"
		col_agregate = "*"
	case Agregate_Count_Unique: // кол-во уникальных записей
		fmt_agregate = "COUNT(DISTINCT %s)"
	case Agregate_Count_Filled: // кол-во заполненных записей
		where += fmt.Sprintf(" %s %s IS NOT NULL", exWhereOperator, col_agregate)
		fmt_agregate = "COUNT(%s)"
		col_agregate = "*"
	case Agregate_Count_Empty: // кол-во пустых записей
		where += fmt.Sprintf(" %s %s IS NULL", exWhereOperator, col_agregate)
		fmt_agregate = "COUNT(%s)"
		col_agregate = "*"
	case Agregate_Sum: // сумма по колонке
		fmt_agregate = "SUM(%s)"
	case Agregate_Min: // минимальное значение по колонке
		fmt_agregate = "MIN(%s)"
	case Agregate_Max: // максимальное значение по колонке
		fmt_agregate = "MAX(%s)"
	case Agregate_Avg: // среднее по колонке
		fmt_agregate = "AVG(%s)"
	default:
		err = errors.New("Unprocessable agregate method: `" + agr_method + "`")
		return
	}
	column_agreg_sql := fmt.Sprintf(fmt_agregate, col_agregate)

	query := fmt.Sprintf("SELECT %s as %s, %s as %s FROM %s %s %s %s %s", col_group, col_group_al, column_agreg_sql, col_agregate_al, from, join, where, groupBy, orderBy)

	d.ins.ReplaceMarks(&query)

	var rows *sql.Rows
	rows, err = q.Query(query, args...)
	if err != nil {
		return
	}
	defer rows.Close()

	// collect rows types
	rec_types, err := rows.ColumnTypes()
	if err != nil {
		err = fmt.Errorf("ColumnTypes(): %v", err)
		return
	}

	inter = &GroupAggregate{
		GroupName: rec_types[0].Name(),
		ValueName: rec_types[1].Name(),
		Count:     0,
		Data:      make([]*groupAggregateItem, 0),
	}

	// collect data
	for rows.Next() {
		item := &groupAggregateItem{}
		if err := rows.Scan(&item.Group, &item.Value); err != nil {
			return nil, err
		}
		inter.Data = append(inter.Data, item)
		inter.Count++

		// determine types of data
		if inter.GroupType == "" && item.Group != nil {
			inter.GroupType = reflect.TypeOf(item.Group).Name()
		}
		if inter.ValueType == "" && item.Value != nil {
			inter.ValueType = reflect.TypeOf(item.Value).Name()
		}
	}
	return inter, nil
}

// excute count sql and return count result int64.
//func (d *dbBase) agregate(q dbQuerier, qs *querySet, mi *modelInfo, cond *Condition, tz *time.Location, column string) (res int64, err error) {
//
//	if len(column) == 0 {
//		panic("no aggregate field/column specified")
//	}
//
//	Q := d.ins.TableQuote()
//
//	tables := newDbTables(mi, d.ins)
//	tables.parseRelated(qs.related, qs.relDepth)
//
//    // detect field name with query alias
//	join_index, _, fi, ok := tables.parseExprs(mi, strings.Split(column, ExprSep))
//	if !ok {
//		panic(fmt.Errorf("unknown field/column name `%s`", column))
//	}
//
//	//col := fmt.Sprintf("%s.%s%s%s", join_index, Q, fi.column, Q)
//	//fmt.Println( col )
//
//	where, args := tables.getCondSQL(cond, false, tz)
//	groupBy := tables.getGroupSQL(qs.groups)
//	tables.getOrderSQL(qs.orders)
//	join := tables.getJoinSQL()
//
//	query := fmt.Sprintf("SELECT MIN(%s.%s%s%s) as %s%s%s FROM %s%s%s T0 %s%s%s", join_index, Q, fi.column, Q, Q, fi.column, Q, Q, mi.table, Q, join, where, groupBy)
//
//	if groupBy != "" {
//		query = fmt.Sprintf("SELECT MIN(%s.%s%s%s) as %s%s%s FROM (%s) AS %s", join_index, Q, fi.column, Q, Q, fi.column, Q, query, join_index)
//	}
//
//	d.ins.ReplaceMarks(&query)
//
//	row := q.QueryRow(query, args...)
//
//	err = row.Scan(&res)
//
//	//fmt.Println( "res:", res )
//	return
//}
