// Copyright 2014 beego Author. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package orm

import (
	"database/sql"
	"fmt"
	"time"
)

const wait_keepalive = 10 * time.Minute

type alias struct {
	Name         string
	Server       *DBServer
	DriverName   string
	DriverType   DriverType
	MaxIdleConns int
	MaxOpenConns int
	DB           *sql.DB
	DbBaser      dbBaser
	TZ           *time.Location
	Engine       string
	active       bool
}

func newAlias() *alias {
	return &alias{}
}

func (al *alias) findServer(driver, dbname, schema string) *alias {
	if server := DBServers.findServer(driver, dbname, schema); server != nil {
		return getDbAlias(server.Alias)
	}
	return nil
}

func (al *alias) info() string {
	return fmt.Sprintf("%q %s{%s:%d %s(%q)}", al.Name, al.Server.Driver, al.Server.Host, al.Server.Port, al.Server.DbName, al.Server.User)
}

func (al *alias) openDB() {
	al.connect()
	al.active = true
	go al.waitKeepAlive()
}

func (al *alias) connect() {
	var err error
	if al.DB != nil {
		return
	}
	logger.Infof("orm: connecting to %s", al.info())

	if al.DB, err = sql.Open(al.Server.GetConnectionDriver(), al.Server.GetConnectionString()); err != nil {
		logger.Errorf("orm: connect to %s error(%v), will try reconnect after %v...", al.info(), err, wait_keepalive)
		return
	}

	if err := al.DB.Ping(); err != nil {
		logger.Errorf("orm: ping %s error(%v), will try reconnect after %v", al.info(), err, wait_keepalive)
		return
	}

	if al.MaxIdleConns > 0 {
		al.DB.SetMaxIdleConns(al.MaxIdleConns)
	}
	if al.MaxOpenConns > 0 {
		al.DB.SetMaxOpenConns(al.MaxOpenConns)
	}
	al.DB.SetConnMaxLifetime(0)

	detectTZ(al) // Detect database's TimeZone
	//logger.Infof("orm: connected to %s success", al.info())
}

func (al *alias) waitKeepAlive() {
	al.active = true
	for al.active {
		select {
		case <-time.After(wait_keepalive):
			//logger.Debugf("orm: keeping alive %s", al.info())
			if al.DB == nil {
				al.connect()
				break
			}

			if _, err := al.DB.Exec("select null where 0=1"); err != nil {
				logger.Errorf("orm: ping %s, error(%v)...trying connect", al.info(), err)
				al.connect()
			}
		}
	}
}

func detectTZ(al *alias) {
	// orm timezone system match database
	// default use Local
	//al.TZ = time.Local

	// default use UTC
	al.TZ = time.UTC

	if al.Server.Driver == "sphinx" {
		return
	}

	switch al.DriverType {
	case DRMySQL:
		row := al.DB.QueryRow("SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP)")
		var tz string
		_ = row.Scan(&tz)
		if len(tz) >= 8 {
			if tz[0] != '-' {
				tz = "+" + tz
			}
			t, err := time.Parse("-07:00:00", tz)
			if err == nil {
				al.TZ = t.Location()
			} else {
				logger.Debugf("Detect DB timezone: %s %s\n", tz, err.Error())
			}
		}

		// get default engine from current database
		row = al.DB.QueryRow("SELECT ENGINE, TRANSACTIONS FROM information_schema.engines WHERE SUPPORT = 'DEFAULT'")
		var engine string
		var tx bool
		_ = row.Scan(&engine, &tx)

		if engine != "" {
			al.Engine = engine
		} else {
			al.Engine = "INNODB"
		}

	case DRSqlite, DROracle:
		al.TZ = time.UTC

	case DRPostgres:
		row := al.DB.QueryRow("SELECT current_setting('TIMEZONE')")
		var tz string
		if err := row.Scan(&tz); err != nil {
			logger.Errorf("select DB timezone: %v\n", err)
			break
		}

		if loc, err := time.LoadLocation(tz); err != nil {
			logger.Errorf("detect DB timezone: %s %s\n", tz, err.Error())
		} else {
			al.TZ = loc
		}

	case DRVertica:
		row := al.DB.QueryRow("SHOW TIMEZONE")
		var nm string
		var tz sql.NullString
		if err := row.Scan(&nm, &tz); err != nil {
			logger.Errorf("select DB timezone: %v\n", err)
			break
		}

		if loc, err := time.LoadLocation(tz.String); err != nil {
			logger.Debugf("detect DB timezone: %v %s\n", tz, err.Error())
		} else {
			al.TZ = loc
		}
	}
}
