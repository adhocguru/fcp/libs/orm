package orm

import (
	"fmt"
	"reflect"
)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// filedInfo Getters
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
func (fs *fields) Count() int {
	return len(fs.dbcols)
}

func (fs *fields) DBColumns() []string {
	return append([]string{}, fs.dbcols...)
}

func (fs *fields) MDColumns() []string {
	var list []string
	for k := range fs.fields {
		list = append(list, k)
	}
	return list
}

func (fs *fields) Pk() *fieldInfo {
	return fs.pk
}

func (fs *fields) FindFieldsRelatedBy(ptrStructOrTableName interface{}) []string {
	mf := GetModel(ptrStructOrTableName)

	// модель - это само искомое поле
	if fs.pk.mi.name == mf.name {
		return []string{fs.pk.name}
	}

	var fn_recursiveDepth func(depth int, prefix string, fi *fieldInfo, related []string, model *modelInfo) []string
	fn_recursiveDepth = func(depth int, prefix string, fi *fieldInfo, related []string, model *modelInfo) []string {
		if depth < 0 {
			return related
		}

		if fi.fieldType == RelManyToMany {
			return related
		}

		if prefix == "" {
			prefix = fi.name
		} else {
			prefix = prefix + ExprSep + fi.name
		}

		if fi.rel {
			if fi.relModelInfo.name == model.name {
				related = append(related, prefix)
				//return related
			}
		}

		depth--
		for _, fi := range fi.relModelInfo.fields.fieldsRel {
			related = fn_recursiveDepth(depth, prefix, fi, related, model)
		}
		return related
	}

	var related []string
	for depth := 0; len(related) == 0 && depth < 5; depth++ {
		for _, fi := range fs.fieldsRel {
			related = fn_recursiveDepth(depth, "", fi, related, mf)
		}
	}
	return related
}

func (fi *fieldInfo) GetName() string {
	return fi.name
}

func (fi *fieldInfo) GetValue(md interface{}) interface{} {
	val := reflect.ValueOf(md)
	ind := reflect.Indirect(val)
	typ := ind.Type()
	if val.Kind() != reflect.Ptr {
		panic(fmt.Errorf("cannot use non-ptr model struct `%s`", getFullName(typ)))
	}
	value, _ := fi.mi.DbBaser().collectFieldValue(fi.mi, fi, ind, false, fi.mi.al.TZ)
	return value
}

func (fi *fieldInfo) GetName_db() string {
	return fi.column
}

func (fi *fieldInfo) GetName_json() string {
	return fi.json
}

func (fi *fieldInfo) GetIsAuto() bool {
	return fi.auto
}

func (fi *fieldInfo) GetIsPk() bool {
	return fi.pk
}

func (fi *fieldInfo) GetIsNull() bool {
	return fi.null
}

func (fi *fieldInfo) GetIsIndex() bool {
	return fi.index
}

func (fi *fieldInfo) GetIsUnique() bool {
	return fi.unique
}

func (fi *fieldInfo) GetModel() *modelInfo {
	return fi.mi
}

func (fi *fieldInfo) GetIsRel() bool {
	return fi.rel
}

func (fi *fieldInfo) GetRelModel() *modelInfo {
	return fi.relModelInfo
}

func (fi *fieldInfo) IsAutoNow() bool {
	return fi.autoNow
}
